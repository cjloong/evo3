import makeStore from './redux/store';
import startServer from './server';

export const store = makeStore();
startServer(store);

// Init store with default entries
store.dispatch({
  type: 'SET_ENTRIES',
  entries: require('./entries.json')
});
store.dispatch({type: 'NEXT'});
