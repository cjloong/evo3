var webpack = require('webpack');

module.exports = {
    entry: [
        'webpack-dev-server/client?http://localhost:8080',
        'webpack/hot/only-dev-server',
        __dirname + '/src/index.jsx'
    ],
    module: {
        loaders: [{
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: 'react-hot!babel'
            }]
            // loaders: [{
            //      test: /\.jsx?$/
            //      , exclude: /(node_modules|bower_components)/
            //      , loader: 'babel' // 'babel-loader' is also a legal name to reference
            //      , query: {
            //           presets: ['es2015', 'react']
            //           , plugins: ['transform-runtime']
            //      }
            // }]
    },
    resolve: {
        extensions: ['', '.js', '.jsx']
    },
    output: {
        path: __dirname + '/dist',
        publicPath: '/',
        filename: 'bundle.js'
    },
    devServer: {
        contentBase: __dirname + '/dist',
        hot: true
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin()
    ]
};
