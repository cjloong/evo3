import Server from 'socket.io';

export default function startServer(store) {
    console.log("Server starting...");
    const io = new Server().attach(8090);

    console.log("Subscribing to state...");

    // Attaching a callback to state changes within a store
    // Store is pub/sub
    //TODO: We need to push changes, not whole state. something
    // to improve in future
    //TODO: Idea: Maybe send initial state and actions only
    store.subscribe(() => io.emit(state, store.getState().toJS())));

    // Send all server state to client right away on connection
    io.on('connection', (socket) => {
      socket.emit('state', store.getState().toJS());
      socket.on('action', store.dispatch.bind(store)); // Receive votes
    });

    console.log("Server started...");

}
