import { Component, OnInit } from '@angular/core';
import { MdMenuModule } from '@angular/material';

@Component({
  selector: 'eggs-rete-visual',
  template: `
<md-menu>
    <button md-menu-item> Refresh </button>
    <button md-menu-item> Settings </button>
    <button md-menu-item> Help </button>
    <button md-menu-item disabled> Sign Out </button>
</md-menu>
  `,
  styles: []
})
export class ReteVisualComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
