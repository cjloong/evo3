import { browser, element, by } from 'protractor';

export class EggsPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('eggs-root h1')).getText();
  }
}
