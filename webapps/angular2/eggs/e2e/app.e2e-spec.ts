import { EggsPage } from './app.po';

describe('eggs App', function() {
  let page: EggsPage;

  beforeEach(() => {
    page = new EggsPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('eggs works!');
  });
});
