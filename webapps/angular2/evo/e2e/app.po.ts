import { browser, element, by } from 'protractor';

export class EvoPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('evo-root h1')).getText();
  }
}
