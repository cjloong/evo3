import { EvoPage } from './app.po';

describe('evo App', function() {
  let page: EvoPage;

  beforeEach(() => {
    page = new EvoPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('evo works!');
  });
});
