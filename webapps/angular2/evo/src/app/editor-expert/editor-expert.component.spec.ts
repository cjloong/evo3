/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { EditorExpertComponent } from './editor-expert.component';

describe('EditorExpertComponent', () => {
  let component: EditorExpertComponent;
  let fixture: ComponentFixture<EditorExpertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditorExpertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorExpertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
