import F from 'funcunit';
import QUnit from 'steal-qunit';

F.attach(QUnit);

QUnit.module('e-vo functional smoke test', {
  beforeEach() {
    F.open('../development.html');
  }
});

QUnit.test('e-vo main page shows up', function() {
  F('title').text('e-vo', 'Title is set');
});
