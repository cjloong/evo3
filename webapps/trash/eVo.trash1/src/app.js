import Map from "can/map/";
import route from "can/route/";
import 'can/map/define/';
import 'can/route/pushstate/';

const AppViewModel = Map.extend({
  define: {
    title: {
      value: 'eVo',
      serialize: false
    }
    , version: {
      value: '1.0',
      serialize: false
    }
  }
});

route('/:page', { page: 'home' });
route('/:page/:subpage1', { subpage1: null });
route('/:page/:subpage1/:subpage2', { subpage1: null, subpage2: null });

export default AppViewModel;
