import F from 'funcunit';
import QUnit from 'steal-qunit';

F.attach(QUnit);

QUnit.module('evo functional smoke test', {
  beforeEach() {
    F.open('../development.html');
  }
});

QUnit.test('evo main page shows up', function() {
  F('title').text('evo', 'Title is set');
});
