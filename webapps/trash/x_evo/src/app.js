import Map from "can/map/";
import route from "can/route/";
import 'can/map/define/';
import 'can/route/pushstate/';

const AppViewModel = Map.extend({
  define: {
    message: {
      value: 'Hello World!',
      serialize: false
    },
    version: {
      value: '0.0',
      serialize: false
    },
    title: {
      value: 'evo',
      serialize: false
    }
  }
});

export default AppViewModel;
