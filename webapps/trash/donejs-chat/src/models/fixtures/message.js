import fixture from 'can-fixture';

const store = fixture.store([{
  id: 0,
  description: 'First item'
}, {
  id: 1,
  description: 'Second item'
}]);

fixture({
  'GET /message': store.findAll,
  'GET /message/{id}': store.findOne,
  'POST /message': store.create,
  'PUT /message/{id}': store.update,
  'DELETE /message/{id}': store.destroy
});

export default store;
