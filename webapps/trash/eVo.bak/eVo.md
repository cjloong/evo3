# Overview
eVo framework is a high level framework for development of applicatins.
It looks at level of abstractions from the business domain and uses code
generators to generate the actual app. This is the main app that generate
all other applications.

## Concepts
1. Entity Designer
    - This models the entity for the app.
1. State Model Designer
    - This model finite state behaviour.
1. Workflow
    - This model workflow processes.
1. Form Designer
    - Base on state, entities, we create data input screens for user
      data entry.
