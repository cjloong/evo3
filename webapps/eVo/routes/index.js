var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  //TODO: Unhardcode apps and load it from apps table
  res.render('index', { title: 'eVo-Main', apps: [
    {name: "reactPlayground", url: "reactPlayground"}
    , {name: "machineLearning", url: "ml"}
//     , {name: "machineLearning", url: "/ml"}
//     , {name: "math" , url: "/math"}
//     , {name: "generators", url : "/generators"}
//     , {name: "eYe" , url: "/eYe"}
//     , {name: "csp" , url: "/csp"}
//     , {name: "nrp" , url: "/nrp"}
//     , {name: "visual" , url: "/visual"}

  ]});
});

module.exports = router;
