/*
import React from 'react';
import ReactDOM from 'react-dom';
import RaisedButton from 'material-ui/RaisedButton';
import IconButton from 'material-ui/IconButton';
import NavigationClose from 'material-ui/svg-icons/navigation/close';
import FlatButton from 'material-ui/FlatButton';
import injectTapEventPlugin from 'react-tap-event-plugin';


injectTapEventPlugin();


const MyAwesomeReactComponent = () => (
  <RaisedButton label="Default" />
);

const styles = {
  title: {
    cursor: 'pointer',
  },
};

function handleTouchTap() {
  console.log('onTouchTap triggered on the title component');
}
function handleLeftTap() {
  console.log('onTouchTap triggered on the left component');
}

const App = () => (
  <MuiThemeProvider muiTheme={getMuiTheme()}>
       <AppBar
          title={<span style={styles.title}>eVo - Machine Learning</span>}
          onLeftIconButtonTouchTap={handleLeftTap}
          onTitleTouchTap={handleTouchTap}
          iconElementRight={
               <div>
                    <FlatButton label="Save" />
               </div>
          }


        />

  </MuiThemeProvider>
);



ReactDOM.render(
 <App/>,
 document.getElementById('app')
);
*/

import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import React from 'react';
import ReactDOM from 'react-dom';
import AppBar from 'material-ui/AppBar';
import ActionHome from 'material-ui/svg-icons/action/home';
import FileCreateNewFolder  from 'material-ui/svg-icons/file/create-new-folder';
import MenuItem from 'material-ui/MenuItem';
import Divider from 'material-ui/Divider';
import Drawer from 'material-ui/Drawer';
import injectTapEventPlugin from 'react-tap-event-plugin';


injectTapEventPlugin();

// Components fire actions
class Actions {

}

// Pub/Sub done here
class Dispatcher {

}

const EventEmitter = require("events");
// Receive every event and reacts to ones it cares about
class MenuStore extends EventEmitter{
  constructor() {
    super();
    this.menus = [
      { id: 1, text: "Home", icon:"Test" }
      , { id: 2, text: "New", icon:"Test" }
    ]

  }

  createMenu() {
    this.menus.push({
      id:3, text:"Testing",icon:"Test2"
    })
    this.emit("change");

  }
  allMenus() {
    return this.menus;
  }


}

const menuStore = new MenuStore;


class Constants {

}

class TopBar extends React.Component {
  constructor(props) {
    super(props);
    this.handleTouchTap = this.handleTouchTap.bind(this)
    this.state = {open: false};
  }

  componentWillMount() {
    menuStore.on("change", () => {

    })
  }
  handleTouchTap() {
    this.setState({
      open: !this.state.open
    })
  }

  render() {
    const title = this.props.title;

    return(
      <div>
           <AppBar
              title={title}
              onLeftIconButtonTouchTap={this.handleTouchTap}
            />
            <Drawer
              open={this.state.open}
              docked={false}
              onRequestChange={(open) => this.setState({open})}
            >
                <MenuItem  leftIcon={<ActionHome/>}>Home</MenuItem>
                <Divider />
                <MenuItem leftIcon={<FileCreateNewFolder/>}>New</MenuItem>

            </Drawer>
        </div>
    )
  }
}

TopBar.propTypes = {
     title: React.PropTypes.string //.isRequired
     //, limit: React.PropTypes.number //.isRequired
}

TopBar.defaultProps = {
  title: "<Title>"
}


class App extends React.Component {
  render() {
    return (
          <MuiThemeProvider muiTheme={getMuiTheme()}>
            <div>
              <TopBar title="eVo - Machine Learning"></TopBar>

            </div>
          </MuiThemeProvider>
    )
  }
}

ReactDOM.render(
 <App/>,
 document.getElementById('app')
);
