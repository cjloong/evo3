/**
 * This webpack configuration listens to components and apps of the system.
 */
const path = require('path');
const webpack = require("webpack");
const WebpackNotifierPlugin = require("webpack-notifier");

//projects
const project = "./webapps/ml";
module.exports = [{

	context : path.resolve(project)
	, node: {fs: "empty"} //TODO: Check if node apps will be affected. Should only affect this on browser stuffs
	, entry: {
		"index": path.resolve(path.join(project, "react", "index.jsx"))
	}
	//For faster code gen:	devtool : 'eval-source-map'
	, devtool: '#source-map'
	, output : {
		path : path.resolve(project, "public", "bundles")
		, filename : "[name].bundle.js"
		, sourcePrefix: ""
	}
	, plugins: [
        new WebpackNotifierPlugin()
	   , new webpack.ProvidePlugin({
		$: "jquery",
		jQuery: "jquery"
	   })
     ]
	,module: {
		loaders: [
			{ test: /\.json$/, loader: "json" }
			, { test: /\.css$/, loader: "style-loader!css-loader" }
			, {
				test: /\.jsx?$/
				, exclude: /(node_modules|bower_components)/
				, loader: 'babel' // 'babel-loader' is also a legal name to reference
				, query: {
					presets: ['es2015', 'react']
					, plugins: ['transform-runtime']
				}
			}

			, { test: /\.(woff2?|svg)$/, loader: 'url?limit=10000' }
		     , { test: /\.(ttf|eot)$/, loader: 'file' }
			, { test:/bootstrap-sass[\/\\]assets[\/\\]javascripts[\/\\]/, loader: 'imports?jQuery=jquery' }
    		]

	}
}];
