var express = require('express')
var router = express.Router()

var data = [
{
    "id": 1,
    "author": "Pete Hunt",
    "text": "Pete's comment"
},
{
    "id": 2,
    "author": "Jordan Walke",
    "text": "Hello world 2"
}
]

/* GET users listing. */
router
.get('/', function(req, res, next) {
    console.log("Get")
    res.send(JSON.stringify(data))
})
.post('/', function(req, res, next) {
    console.log("post")
    data.push(req.body)
    console.log(req.body)
    res.send(JSON.stringify(data))
})

module.exports = router
