// This is for scratch code

QUnit.module("Scratch", ()=>{

	QUnit.module("ES5 techniques");
	QUnit.test("Inheritance", function(assert){
		///////////////////////////////////////////
		// Animal as base class
		var Animal = function(param){
			console.log("Animal construction:" + param);
		};
		Animal.prototype.eat = function(){
			console.log("Mum mum");
		}
		Animal.prototype.speak = function(){
			console.log("Huh?");
		}
		Animal.prototype.version = "Animal v1.0";

		///////////////////////////////////////////
		// Cat inherit from Animal
		var Cat = function(param){
			Animal.call(param);
			console.log("Cat construction:" + param);
		}

		Cat.prototype = Object.create(Animal.prototype); // sync up the prototypes
		Cat.constructor = Cat; // so will not take from Animal

		Cat.prototype.version = "Cat v1.0";
		// Cat overrides
		Cat.prototype.speak = function(){
			console.log("Meow");
		}


		var animal = new Animal("Animal Construction\n-------------------");
		animal.speak();
		animal.eat();
		console.log(animal.version)

		var cat = new Cat("Cat Construction\n-------------------");
		cat.speak();
		cat.eat();
		console.log(cat.version)
		assert.ok(true, "No asserts yet.");


	});

	QUnit.module("ES6");
	QUnit.test("Arrow function", function(assert){
		assert.ok([1,2,3,4,5,6].filter(e=>e===2)[0]===2);
	});

	QUnit.test("Normal class", function(assert){
		class Animal {
			constructor() {

			}

			speak() {
				return "huh";
			}
		}

		class Dog extends Animal {
			constructor() {
				super();
			}
			speak() {
				return "woof";
			}
		}

		class Cat extends Animal {
			constructor(){
				super();
			}
			speak(){
				return "meow";
			}
		}
		class MuteCat extends Cat {
			speak(){
				return "";
			}
		}

		var a = new Animal();
		assert.ok(a.speak()=="huh", "huh assert.ok");

		var cat = new Cat();
		assert.ok(cat.speak()=="meow", "meow expected");

		var dog = new Dog();
		assert.ok(dog.speak()=="woof", "woof expected");

		var muteCat = new MuteCat();
		assert.ok(muteCat.speak()=="", "nothing expected");

	});



	QUnit.module("Qunit hierarchy tryout", (assert)=>{
		QUnit.module("Module 1");
		QUnit.test("1.1 test", (assert)=>{ assert.ok (true) });
		QUnit.test("1.2 test", (assert)=>{ assert.ok (true) });
		QUnit.module("Module 2");
		QUnit.test("2.1 test", (assert)=>{ assert.ok (true) });
		QUnit.test("2.1 test", (assert)=>{ assert.ok (true) });

	});

});
