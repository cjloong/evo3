var $ = require("jquery");


QUnit.module("UI", (assert)=>{
	QUnit.module("ML UI Warmup", 
		{beforeEach: ()=>{
			$('body').append('<div id="qunit-fixture"><div id="ui"></div></div>');
		}, afterEach: (assert)=>{
		    $('#qunit-fixture').remove();
		}
	});
	QUnit.module("ML UI Warmup");
	QUnit.test("UI Warmup",(assert)=>{
		assert.ok(typeof $ != 'undefined', "JQuery exists!!");

		var $ui = $("#qunit-fixture"); 
		$ui.append("Hello World");
		assert.ok($ui.html()=="Hello World");
		$ui.empty();
		assert.ok($ui.html()=="");


	});


	QUnit.module("_Development:ML UI", 
		{beforeEach: ()=>{
			$('body').append('<div id="qunit-fixture"></div>');
		}, afterEach: (assert)=>{
		    $('#qunit-fixture').remove();
		}
	});
	QUnit.skip("Instantiation of ui for ML framework",(assert)=>{});
});

