var Matrix = require("../math_matrix/math_matrix.js");
/**
 * This houses code for data preprocessing and manipulation for machine learning,
 * cleansing and other purposes.
 */
module.exports = {};
var featureNormalizer = module.exports.featureNormalizer = function(X, y, normParam) {
	var MX = Matrix.matrixify(X);
	var m = MX.rowCount();
	
	if(typeof normParam == 'undefined') {
		var normParam = MX.colMaxMinSum();
		normParam.mean = normParam.sum / m;
	}

	var mean = normParam.mean;
	var S = normParam.max - normParam.min;


	var ret = MX.mapAllInPlace(function(i){
		return (i - mean)/S;
	});

	return normParam;
}


