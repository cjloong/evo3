# Parallel Programming
- power wall - 21 century processor frequency hitting a limit

Harder
- seprating sequential to parallel can be challenging or even impossible
- correctness and hard to debug

Why do it?
- faster / speedup

Paralelism and concurrency are closely related but not same

parallel program - uses parallel hardware to execution faster

Parallel
-divide into subproblems
- optimal use of parllel hardwared

Concurrent - may /or not execute multiple executions at the same time.
- improve modularity, responsiveness or maintainability.

parallel often overlap with concurrent

Parallelism Granularity Levels
- bit level (4 bit to 8 bit to 16 bit to 32 bit ....)
- instruction level parallelsism
    - b = a + c
    - d = k + y
    - b & d can be parallelized.
    
- task level parallelsim
    - executing separate instruction streams in parallel
    - this course is about this.
    
Hardware/Parallel Computers
- multicore processors
- symmetric multiprocessors
- GPU - coprocessor
- field-programmable gate arrays
- computer clusters

course focus on SMP and multicores

## Parallelism on the JVM
- Process and threads
