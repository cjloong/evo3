// This is the implementation class
class PropLogic {
	static OP(){
		return {
			OR:'||'
			, AND:'&&'
		};
	}

	constructor(data={model:{

	}}){
		this._data = data;
	}

	// Connectives
	or(...list) { return (['or'].concat(list)) }
	and(...list) { return (['and'].concat(list)) }
	not(item) { return (['not'].concat(item)) }
	xor(item) {return ['xor'].concat(item)}

	// Evaluators
	eval(expr=[], facts={}) {

		return false;
	}

}

class TruthTable {
	constructor(data={
		conditions: []
		, definitions: []
		, truthTable: [] // Using heap to handle truth tree
	}) {
		this.data = data;
		this.indexes = {};
	}

	//TODO: Create a binary tree for this
	define(defn={}) {
		// validation
		if(Object.keys(defn).length===0) throw "Empty definition";
		if(!defn.hasOwnProperty("result")) throw "result not specified";
		if(Object.keys(defn).length===1) throw "You need to define conditions besides result";

		// OK. Can continue with definition
		// Order conditions in sequence of their indexes
		// As definition are processed, more and more conditions are created with index ascending
		const index = this.data.indexes
		const heap = this.data.truthTableHeap;
		const conditions = Object.keys(defn)
		.filter(cond=>cond!="result")
		.sort( (condA, condB)=>{
			const keyA = this.putCondition(condA);
			const keyB = this.putCondition(condB);
			return  keyB - keyA;
		})
		;
		
		this.data.definitions.push(defn);


// 		Object.keys(defn).filter(key=>key!="result").forEach(cond=>{
// 			console.log("Defn Processing:" + cond);
// 			const index = this.putCondition(cond);
// 			rule[index] = defn[cond];
// 		});

	}

	putCondition(condition) {

		if(this.indexes.hasOwnProperty(condition)) return this.indexes[condition];
		else {
			const index = this.data.conditions.length;
			this.data.conditions[index] = condition;
			this.indexes[condition] = index;

			return index;
		}
		
		// POST ASSERTION
		BugTrap.invalidate("Should not reach here!!");
	}

}

//
//              Debug Traps
//

var BugTrap = DebugLib;

class DebugLib {
	static invalidate(msg="Invalid state reached") {
		throw msg;
	}
}

class ProdLib {
	static invalidate() {}
}



QUnit.module("Logic", (assert)=>{
	// Test objects
	QUnit.module("_Development",(assert)=>{

		QUnit.test("Truth table", (assert)=>{
			const tt = new TruthTable();

			tt.define({
				"banana present": true
				, "there is a monkey": '*'
				, result: "banana not there"
			})
			tt.define({
				"there is monkey":false
				, "banana present": true
				, result: "banana still there"
			})


			console.log(tt.data);

		});



    });

	QUnit.module("Parked aside Test", (assert)=>{
	
		QUnit.test("Prop Logic Warmup",(assert)=>{
			let L = new PropLogic();
			
			let expr1 = L.or("it is sunday", "it is raining");
			let expr2 = L.or("it is monday", "it is sunny");
			let notexpr = L.not("there is a black cat");
			let expr = L.and(expr1, expr2, notexpr);

			console.debug(expr);
			assert.ok(expr.length===4);
			assert.ok(expr[0]=='and');
			assert.ok(expr[1].length===3);
			assert.ok(expr[1].join(",")=="or,it is sunday,it is raining");
			assert.ok(expr[2].length===3);
			assert.ok(expr[2].join(",")=="or,it is monday,it is sunny");
			assert.ok(expr[3].length===2);
			assert.ok(expr[3].join(",")=="not,there is a black cat");

			let facts = {
				"it is sunday":true
				, "it is sunny":true
				, "there is a black cat": false
			}
			let r = L.eval(expr, facts);
			console.log(r);
			assert.ok(r===true);
			
		});


		// 			If it is Sunday we go fishing.
		// 			It is Sunday
		// 			Therefore we go fishing
		QUnit.skip("Test if", (assert)=>{

		});

	});

	QUnit.module("Unit Test", (assert)=>{

		QUnit.skip("Simple UI rule test", (assert) =>{
			
		});
	});

});