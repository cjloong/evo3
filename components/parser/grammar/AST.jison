/* description: AST Parser */
/* lexical grammar */
%lex
%%

\s+                    /* skip whitespace */
"("                    return '('
")"                    return ')'
[a-zA-Z0-9]+           return 'WORD'
<<EOF>>                return 'EOF'


/lex
%{

// Support functions
const logStateON = false;

function logState(lhs, stateName, outputArray) {
    if(!logStateON) return;

    outputArray = outputArray || [];
    console.log(lhs + ">>>" + stateName + ":" + outputArray.join(","));
}

function makeNode(type,child) {
    return {node: type, child: child};
}

function makeChild(a) {
    var accum = [];
    if( Array.isArray(a) ){
        accum = a;
    } else {
        accum.push( a );
    }
    return accum;    
}
function appendChild(a, b) {
    var arrayChild = makeChild(a);
    arrayChild.push(b);
    return arrayChild
}
%}

%start root

%% /* language grammar */
root: ast EOF{
    return $1
};

// Test Cases:
// (home (node1 a b c (n1x1 f a a))(node2 d e f))

ast: 
    '(' WORD astlist ')' {
        logState("ast", "( WORD astlist )", [$1, $2, $3, $4]);
        $$ = {
            node: $2,
            child: makeChild($3)
        }
    }
    | '(' WORD ast ')' {
        logState("ast", "( WORD ast )", [$1, $2, $3, $4]);
        
        $$ = {
            node: $2,
            child: makeChild($3)
        }
    }
    | '(' WORD wordlist ')' {
        logState("ast", "( WORD wordlist )", [$1, $2, $3, $4]);
        
        $$ = {
            node: $2,
            child: makeChild($3)
        }


    }
;

astlist:
    astlist ast {
        logState("astlist", "astlist ast", [$1, $2]);
        $$ = appendChild($1, $2);
    }
    | ast {
        logState("astlist", "ast", [$1]);
        $$ = $1;
    }
;

wordlist: wordlist WORD {
        logState("wordlist", "wordlist WORD", [$1, $2]);
        $$ = appendChild($1, $2);
    }
    | wordlist astlist {
        logState("wordlist", "wordlist astlist", [$1, $2]);
        $$ = appendChild($1, $2);
    }
    | WORD { 
        logState("wordlist", "WORD", [$1]);
        $$=yytext;
    }
;
