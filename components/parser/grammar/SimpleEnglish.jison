%lex
%%

/* description: Parses simplified english */
/* lexical grammar */

\n                    return 'NEWLINE'
\s+                   /* skip whitespace */
\.                    return 'PERIOD'
[a-zA-Z0-9]+           return 'WORD'
<<EOF>>               return 'EOF'


/lex
%{
    const logStateON = false;

    function logState(stateName, outputArray) {
        if(!logStateON) return;

        outputArray = outputArray || [];
        console.log(stateName + ":" + outputArray.join(","));
    }


%}

%start document

%% /* language grammar */

document: paragraphs EOF {
    logState("document",[$1]);
    return "(document " + $1 + ")";
}
;

paragraphs: 
paragraphs paragraph {
    logState("paragraphs paragraph", [$1, $2]);
    $$ = $1 + $2;

}
| paragraph {
    logState("paragraph:" + [$1]);
    $$ = $1;
}
;

paragraph:
paragraph sentences NEWLINE {
    logState("paragraph sentences NEWLINE:", [$1, $2]);
    $$ = $1 + "(paragraph " + $2 + ")";
}
| NEWLINE // Consume all newlines
;

sentences:
sentences sentence PERIOD {
    logState("sentences sentence:", [$1, $2]);
    $$ = $1 + "(sentence2 " + $2 + ")";
}
| sentence PERIOD {
    logState("sentence PERIOD:", [$1]);
    var node = "(sentence1 " + $1 + ' '  + ")"
    $$ = node;

}
;

sentence :
sentence WORD {
    logState("sentence WORD:", [$1, $2]);
    $$ = $1  + " " + $2;
}
| 
WORD { 
    logState("WORD");
    $$=yytext 
}
;
