/* description: AST Parser */
/* lexical grammar */
%lex
%%

\s+                    /* skip whitespace */
"("                    return '('
")"                    return ')'
[a-zA-Z0-9]+           return 'WORD'
<<EOF>>                return 'EOF'


/lex
%{

// Support functions
const logStateON = false;

function logState(lhs, stateName, outputArray) {
    if(!logStateON) return;

    outputArray = outputArray || [];
    console.log(lhs + ">>>" + stateName + ":" + outputArray.join(","));
}


%}

%start root

%% /* language grammar */
root: ast EOF{
    return $1
};

ast: 
    '(' ast ')' {
        logState("ast", "( ast )", [$1, $2]);
        $$ = $2;
    }
    | WORD words {
        logState("ast", "words WORD", [$1, $2]);
        $$ = {
            type: $1
            , param: $2
        }

    }
    | WORD ast {
        logState("ast", "WORD ast", [$1, $2]);
        $$ = {
            type: $1
            , param: $2
        }
        
    }
;

words:
    words WORD {
        logState("words", "words WORD", [$1, $2]);
        $$ = $1 + " " + $2;
    }
    | WORD { 
        logState("words", "WORD", [yytext]);
        $$ = yytext 
    }
;   

