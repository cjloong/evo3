// mygenerator.js
var PEG = require("pegjs");
var Jison = require("jison");
var Parser = Jison.Parser;
var bnf = require('ebnf-parser');

////////////////////////////////////////////////////////////////////////
// Parsers - Sentence, paragraph, document
function makeParser (grammar) {
	try{
		const type = "lalr";
		const cfg = bnf.parse(grammar);
		Jison.print = function () {};
		const parserGenerator = Jison.Generator(cfg, {type: type});
		const parser = parserGenerator.createParser();

		return parser;
	} catch(e) {
		console.error(e);
		return null;
	}

}

function ParagraphParser(){
	var grammar = require("raw!./grammar/SimpleEnglish.jison");
	return makeParser(grammar);
}


QUnit.module("Parser", (assert)=>{
	QUnit.module("Unit Test:Parser");
	QUnit.skip("Warmup test",(assert)=>{
		assert.ok(false, "TODO:Warm up test");
	});

	QUnit.module("PEG:Parsing test");
	QUnit.test("Warmup Parser",(assert)=>{
		var PEG = require("pegjs");
		var parser = PEG.buildParser("start = ('a' / 'b')+");
		parser.parse("abba"); // returns ["a", "b", "b", "a"]
		try{
			parser.parse("abcd"); // throws an exception 
		} catch(e) {
			assert.ok(e.message.startsWith("Expected "));
			return;
		}
		assert.ok(false, "Exception should be happening.");
	});

	QUnit.module("JISON:Parsing test");
	QUnit.test("Jison warmup", (assert)=>{

		var grammar = {
		    "lex": {
		        "rules": [
		           ["\\s+", "/* skip whitespace */"],
		           ["[a-f0-9]+", "return 'HEX';"]
		        ]
		    },

		    "bnf": {
		        "hex_strings" :[ "hex_strings HEX",
		                         "HEX" ]
		    }
		};

		var parser = new Parser(grammar);

		// generate source, ready to be written to disk
		var parserSource = parser.generate();

		// you can also use the parser directly from memory

		parser.parse("adfe34bc e82a");
		assert.ok(true, "Parse assert.ok");

		
		try{
	 		parser.parse("adfe34bc zxg");		
		} catch(e) {
			assert.ok(e.message.startsWith("Lexical error "));
			return;
		}
		assert.ok(false, "Exception should be happening.");

	});

	QUnit.test("Calculator Parser:Text",(assert)=>{
		try{
			var grammar = require("raw!./grammar/Expression.jison");
			var parser2 = makeParser(grammar);
			var result = parser2.parse("1+3");
			assert.ok(result===4, "Parse assert.ok");

		} catch (e) {
			console.error(e);
		}

	});

 	QUnit.test("Calculator Parser-json", (assert)=>{
		var grammar = {
		   "comment": "JSON Math Parser",
		   // JavaScript comments also work

		   "lex": {
			  "rules": [
				 ["\\s+",                    "/* skip whitespace */"]
				 , ["[0-9]+(?:\\.[0-9]+)?\\b", "return 'NUMBER'"]
				 , ["\\*",                     "return '*'"]
				 , ["\\/",                     "return '/'"]
				 , ["-",                       "return '-'"]
				 , ["\\+",                     "return '+'"]
				 , ["\\^",                     "return '^'"]
				 , ["!",                       "return '!'"]
				 , ["%",                       "return '%'"]
				 , ["\\(",                     "return '('"]
				 , ["\\)",                     "return ')'"]
				 , ["PI\\b",                   "return 'PI'"]
				 , ["E\\b",                    "return 'E'"]
				 , ["$",                       "return 'EOF'"]
			  ]
		   },

		   "operators": [
			  ["left", "+", "-"]
			  , ["left", "*", "/"]
			  , ["left", "^"]
			  , ["right", "!"]
			  , ["right", "%"]
			  , ["left", "UMINUS"]
		   ],

		   "bnf": {
			  "expressions": [["e EOF",   "return $1"]],

			  "e" :[
				 ["e + e",    "$$ = $1+$3"]
				 , ["e - e",  "$$ = $1-$3"]
				 , ["e * e",  "$$ = $1*$3"]
				 , ["e / e",  "$$ = $1/$3"]
				 , ["e ^ e",  "$$ = Math.pow($1, $3)"]
				 , ["e !",    "$$ = (function(n) {if(n==0) return 1; return arguments.callee(n-1) * n})($1)"]
				 , ["e %",    "$$ = $1/100"]
				 , ["- e",    "$$ = -$2", {"prec": "UMINUS"}]
				 , ["( e )",  "$$ = $2"]
				 , ["NUMBER", "$$ = Number(yytext)"]
				 , ["E",      "$$ = Math.E"]
				 , ["PI",     "$$ = Math.PI"]
			  ]
		   }
		}

 		
		var parser = new Parser(grammar);
		var parserSource = parser.generate();
		console.log(parser.parse("1+3"));
		assert.ok(true, "Parse assert.ok");
 	});



	QUnit.module("Entity Parser - Statement Extraction");

 	QUnit.test("Entity Parser - Paragraph extraction", (assert)=>{

		try{

			const parser = ParagraphParser();
			const code = `
			All the turkey got loose. They ran out of the zoo. Zoo door was open.
			It was noon day when all these happen.
			Amazing things happen when all animals are loose. Things get messy.
			`;

			var ast = parser.parse(code);
			console.log(ast);
			assert.ok(true, "Parse assert.ok");
		} catch(e) {
			console.error(e);
		}
 	});



	QUnit.module("_Development:Parser");
 	QUnit.skip("New development", (assert)=>{});
 	QUnit.test("Entity Parser - AST to array", (assert)=>{
		try{


			function ASTParser() {
				const grammar = require("raw!./grammar/AST.jison");
				return makeParser(grammar);
			}

			const parser = ASTParser();
			var result = parser.parse(`(home a b c)`);
			console.debug(result);
			assert.ok(result.node==="home", "1");
			assert.ok(result.child.length===3, "2");
			assert.ok(result.child[0]==='a', "3");
			assert.ok(result.child[1]==='b', "4");
			assert.ok(result.child[2]==='c', "5");

 			var result = parser.parse(`(home (node1 a b c))`);
 			console.debug(result);
 			assert.ok(result.node==="home", "6");
 			assert.ok(result.child.length===1, "7");
 			assert.ok(result.child[0].node==='node1', "8");
 			assert.ok(result.child[0].child[0]==='a', "9");
 			assert.ok(result.child[0].child[1]==='b', "10");
 			assert.ok(result.child[0].child[2]==='c', "11");

			
			var result = parser.parse(`(home (node1 a b c )(node2 d e f))`);
			console.debug(result);
 			assert.ok(result.node==="home", "12");
 			assert.ok(result.child.length===2, "13");
 			assert.ok(result.child[0].node==='node1', "14");
 			assert.ok(result.child[0].child[0]==='a', "15");
 			assert.ok(result.child[0].child[1]==='b', "16");
 			assert.ok(result.child[0].child[2]==='c', "17");
 			assert.ok(result.child[1].node==='node2', "18");
 			assert.ok(result.child[1].child[0]==='d', "19");
 			assert.ok(result.child[1].child[1]==='e', "20");
 			assert.ok(result.child[1].child[2]==='f', "21");


			var result = parser.parse(`(home (node1 a b c (n1x1 f a a))(node2 d e f))`);
			console.debug(result);
			// L1
 			assert.ok(result.node==="home", "22");
 			assert.ok(result.child.length===2, "23");
			// L2
 			assert.ok(result.child[0].node==='node1', "24");
 			assert.ok(result.child[0].child[0]==='a', "25");
 			assert.ok(result.child[0].child[1]==='b', "26");
 			assert.ok(result.child[0].child[2]==='c', "27");
 			assert.ok(result.child[1].node==='node2', "28");
 			assert.ok(result.child[1].child[0]==='d', "29");
 			assert.ok(result.child[1].child[1]==='e', "30");
 			assert.ok(result.child[1].child[2]==='f', "31");

 			// L3
 			assert.ok(result.child[0].child[3].node==='n1x1', "33");
 			assert.ok(result.child[0].child[3].child.length===3, "34");
 			assert.ok(result.child[0].child[3].child[0]==='f', "35");
 			assert.ok(result.child[0].child[3].child[1]==='a', "36");
 			assert.ok(result.child[0].child[3].child[2]==='a', "37");

	
		} catch(e) {
			console.error(e);
		}
 	});


});
