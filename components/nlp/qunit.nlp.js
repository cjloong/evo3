// This file bridges to nlp_compromise package. Goal is to have this parse a natural language
// for app components/entities/screens

QUnit.module("NLP", (assert)=>{
	let nlp = require("nlp_compromise"); // or nlp = window.nlp_compromise

	QUnit.module("_Development");

	QUnit.skip("Extract action and nouns", (assert)=>{
		let all = nlp.text("A zoo will contain many animals. All animals feed on food.");
		all.sentences.map((s)=>{

		});
	});
	QUnit.skip("Test1", (assert)=>{

		assert.ok(nlp.noun("dinosaur").pluralize()=="dinosaurs");
		// "dinosaurs"

		nlp.verb("speak").conjugate();
		// { past: 'spoke',
		//   infinitive: 'speak',
		//   gerund: 'speaking',
		//   actor: 'speaker',
		//   present: 'speaks',
		//   future: 'will speak',
		//   perfect: 'have spoken',
		//   pluperfect: 'had spoken',
		//   future_perfect: 'will have spoken'
		// }

		assert.ok(nlp.statement('She sells seashells').negate().text()=="She doesn't sell seashells")
		// She doesn't sell seashells

		assert.ok(nlp.sentence('I fed the dog').replace('the [Noun]', 'the cat').text()=="I fed the cat");
		// I fed the cat

		nlp.text("Tony Hawk did a kickflip").people();
		// [ Person { text: 'Tony Hawk' ..} ]

		assert.ok(nlp.noun("vacuum").article()=="a");
		// "a"

		assert.ok(nlp.person("Tony Hawk").pronoun()=="he");
		// "he"

		assert.ok(nlp.value("five hundred and sixty").number==560);
		// 560

// 		nlp.text(require('nlp-corpus').text.friends()).topics()//11 seasons of friends
		// [ { count: 2523, text: 'ross' },
		//   { count: 1922, text: 'joey' },
		//   { count: 1876, text: 'god' },
		//   { count: 1411, text: 'rachel' },

	});
	QUnit.module("Unit Test");
	QUnit.test("Warmup test", (assert)=>{
		assert.ok(true, "Warmup test ok");
	});
});