let _ = require("lodash");

module.exports = {};
module.exports.WorkingMemory = class {
	constructor(data={
		"_system" : { // holds system informations
			size: 0
		} 
		, _facts : {}
		, "_global" : [] // holds global sentences
	}) {
		this.data = data;
	}

	facts(name=null, value) {
		if(_.isEmpty(name)) {
			return this.data._facts;
		} else if(_.isUndefined(value)) {
			return this.factsGet(name);
		} else {
			name = this.cleanupString(name);
			this.factsAdd(name, value);
			return this;
		}
	}

	system(sentence=null) {
		if(_.isEmpty(sentence)) {
			return this.data._system;
		} else {
			sentence = this.cleanupString(sentence);
			this.data._system.push(sentence);
			this.data._system.size ++;
			return this;
		}
	}

	global(sentence=null) {
		if(_.isEmpty(sentence)) {
			return this.data._global;
		} else {
			sentence = this.cleanupString(sentence);

			if(this.globalExist(sentence)) {
				console.warn(`Sentence[${sentence}] already exist. Not adding`);
			} else {
				this.data._global.push(sentence);
				this.data._system.size ++;
				
			}
			return this;
		}
	}

	makefactsParent(data = "") {
		let param = [];

		if(_.isString(data)) param = data.split(".");
		else if(_.isArray(data)) param = data;
		else throw "makefactsParent can only accept string and array as data";

		let parentSet = param.slice(0, param.length-1);
		let currentNode = this.facts();
		parentSet.forEach((node)=>{
			console.debug(`Processing ${node}`);
			if(currentNode==null) currentNode={};

			if(!currentNode.hasOwnProperty(node)) currentNode[node]={};

			currentNode = currentNode[node];
			console.debug(this.facts());

		});

		return currentNode;
	}

	factsAdd(obj="", value=null) {
		if(_.isEmpty(obj)) throw "obj is mandatory";

		let splitted = obj.trim().split(".");
		let node = splitted[splitted.length - 1];
		const parent = this.makefactsParent(splitted);
		parent[node]=value;
		return this;

	}

	factsGet(obj="") {
		if(_.isEmpty(obj)) return this.facts();

		let splitted = obj.trim().split(".");
		let currentNode = this.facts();
		for(let i = 0; i<splitted.length; i++) {
			let node = splitted[i].trim();

			if(currentNode.hasOwnProperty(node)) currentNode = currentNode[node];
			else return;
		}

		return currentNode;
	}

	cleanupString(string="") {
		return string.trim();
	}
	globalDelete(sentence=null) {
		if(_.isEmpty(sentence)) throw "For globalExist, sentence is mandatory";

		sentence = this.cleanupString(sentence);
		let global = this.data._global;
		let index = global.indexOf(sentence);

		if(index<0) console.warn(`Cannot find [${sentence}]. This facts does not exist.`);
		else {
			global.splice(index, 1);
			this.system().size--;
		}
	}
	globalExist(sentence=null) {
		if(_.isEmpty(sentence)) throw "For globalExist, sentence is mandatory";

		sentence = this.cleanupString(sentence);
		return this.data._global.indexOf(sentence)>=0;
	}

	size() {
		return this.data._system.size;
	}
}




