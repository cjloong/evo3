//REFACTORING:BEGIN
// Should refactor these out of here. This is the main qunit test

//REFACTORING:END

require('./WorkingMemory.qunit.js');
require('./Memento.qunit.js');
require('./Predicates.qunit.js');
require('./Rules.qunit.js');
require('./InferenceEngine.qunit.js');
require('./Action.qunit.js');
require('./Rete.qunit.js');

// QUnit.module("Template", (hooks)=>{
// 	// Test objects
// 	QUnit.module("_Development",(hooks)=>{
// 		hooks.beforeEach((assert) => {

// 		});
// 		hooks.afterEach((assert) => {

// 		});

// 		QUnit.skip("TODO:Single Variable String at the middle PredSentence Test", (assert)=>{
// 		});
// 	});
// });


/*
Notes:
Rule base system consist of :-
1. A set of rules
2. Working memory
3. Inference engine

A set of rules:-
a. Represented as pattterns and actions

Inference Engine:-
3 stage
a. MATCH (input:Working Memory output: conflict set)
	- conflict set = rules + timestamp
	- input for RESOLVE

b. RESOLVE (input: select 1 rule from conflict set, output rule & data)
	- primary function is to select a rule from conflict set
	- Strategies for RESOLVE
		- LEX - choose a rule that makes the max number of test (specificity)
		- RECENCY - choose rule that matches the most recent data (to develop an idea)
		- MEA (means ends analysis) - recency of first pattern of rule (to partition rules else specificity)
		- REFRACTORNESS - dont repeat firing rules
c. EXECUTE ()
	- execute the actions
	- MAKE, DELETE, MODIFY (combination of make and delete)
	- changes working memory


ALGORITHMS
Rete algorithm - use at the matching phase
- make up of discrimination network and assimilitave network
- takes changes in working memory and produce changes in conflict set
- does it by looking at rules network (discrimination and assimilitave net) 
   
*/

/**
Working Memory represents the current state of things.

Patterns:
(card ^name 'ace') - class card, attribute name equals constant 'ace'
(card ^name <x>) - class card, attribute name equals whatever variable x is holding
(card ^rank>1)
~(card ^suite spade) - card.suite is not equals to spade
~(card ^suite spade player <x>) - player x does not have spade suite

Actions:
(make (card ^name ace ...))
(remove 2) - remove anything in WM that match 2nd pattern

modify - combination of make and remove

Additional actions
Read/print/halt

Power of patterns... Example
(array ^index<i> ^val<n>)
(array ^index(j>i) ^val(m>n))

-> modify 1 ^val<m>
   modify 2 ^val<n>

 The above pattern will sort
*/



















