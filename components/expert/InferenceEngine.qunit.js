let _ = require("lodash");
let WorkingMemory = require("./WorkingMemory.js").WorkingMemory;
let makeMemento = require("./Memento.js").makeMemento;
let RuleBuilder = require("./Rules.js").RuleBuilder;



/**
## Inference Engine
This currently acts as a facade to all the rest of the system.

*/
class InferenceEngine {

	constructor() {
		this.data = {
			wm: new WorkingMemory()
			, rules: []
			, fired:{}
		}
	}

	// public functions
	initWorkingMemory(wm=null) {
		if(wm==null) throw "wm is mandatory";
		if( ! (wm instanceof WorkingMemory)) throw "wm is not WorkingMemory";

		this.data.wm = wm;
		return this;
	}

	_match() {
		var self = this;
		// Currently we use brute force matching
		let conflictSet = this.data.rules.filter((rule)=>{
			return self.data.fired[rule.name]!==true && rule._isInConflict(self.data.wm);
		});

		return conflictSet;
	}

	_resolve(conflictSet=[]) {
		if(conflictSet.length===0) return null;
		else if (conflictSet.length===1) return conflictSet[0];
		else {
			// TODO: Resolution algorithm

			return conflictSet[0];
		}
	}

	_fireRule(rule) {
		if(rule!=null)  {
			rule._fireActions(this.data.wm);
			this.data.fired[rule.name]=true;
		}


	}

	forwardChain() {
		do {

			var conflictSet = this._match();
			var rule = this._resolve(conflictSet);
			this._fireRule(rule);

		} while (rule!=null);

	}
	// Match part
	// Rule building part
	get ruleBuilder() {
		if(this._ruleBuilder==null) this._ruleBuilder = new RuleBuilder();

		return this._ruleBuilder;
	}

	// Facade to RuleBuilder to add rule
	rule(name=null) {
		this.ruleBuilder.rule(name);
		return this;
	}

	end() {
		this.ruleBuilder.end();
		return this;
	}
	or() {
		this.ruleBuilder.or();
		return this;
	}
	and() {
		this.ruleBuilder.and();
		return this;
	}
	sentence(sentence = null) {
		this.ruleBuilder.sentence(sentence);
		return this;
	}
	then() {
		this.ruleBuilder.then();
		return this;
	}
	logRuleName() {
		this.ruleBuilder.logRuleName();
		return this;
	}
	globalAdd(sentence) {
		this.ruleBuilder.globalAdd(sentence);
		return this;
	}
	factAdd(fact, value) {
		this.ruleBuilder.factAdd(fact, value);
		return this;
	}

	endRule() {
		let rule = this.ruleBuilder.build();
		this.data.rules.push(rule);
		return this;
	}

}

QUnit.module("Inference Engine", (hooks)=>{
	QUnit.module("Internals",(hooks)=>{
		let wm;
		let engine;

		hooks.beforeEach((assert) => {
			wm = new WorkingMemory();
			engine = new InferenceEngine();

		});

		QUnit.test("Check rule builder", (assert)=>{
			const rb = engine.ruleBuilder;
			assert.ok(rb!=null, rb);
			assert.ok(rb instanceof RuleBuilder, rb);
		});
	});
	
	QUnit.module("_Development",(hooks)=>{
		let wm;
		let engine;

		hooks.beforeEach((assert) => {
			wm = new WorkingMemory();
			engine = new InferenceEngine();
			
			engine
			.rule("Good weekend")
				.sentence("Sat is sunny")
				.sentence("Sun is sunny")
			.then()
				.logRuleName()
				.globalAdd("Weekend sunny")
				.factAdd("weekend.activity", "Go out")
			.endRule()
			.rule("Good weekend")
				.or()
					.sentence("Sat is rainy")
					.sentence("Sun is rainy")
				.end()
			.then()
				.logRuleName()
				.globalAdd("Weekend rainy")
				.factAdd("weekend.activity", "Stay at home")
			.endRule()

			;


		});

		QUnit.test("Test good weekend", (assert)=>{
			wm
			.global("Sat is sunny")
			.global("Sun is sunny")
			;
			engine.initWorkingMemory(wm);
			engine.forwardChain();

			var result = wm.globalExist("Weekend rainy");
			assert.ok(result===false, `result=${result}`);
			var result = wm.globalExist("Weekend sunny");
			assert.ok(result===true, `result=${result}`);
		});

		QUnit.test("Test rainy weekend", (assert)=>{
			wm
			.global("Sat is sunny")
			.global("Sun is rainy")
			;

			console.log(wm.global().length);
			assert.ok(wm.global().length===2, wm.global().length);

			engine.initWorkingMemory(wm);
			engine.forwardChain();

			var result = wm.globalExist("Weekend rainy");
			assert.ok(result===true, `result=${result}`);
			var result = wm.globalExist("Weekend sunny");
			assert.ok(result===false, `result=${result}`);

		});
	});

});

