const _ = require("lodash");
const RuleModule = require("./Rules.js");
const Rule = RuleModule.Rule;
const RuleBuilder = RuleModule.RuleBuilder;
const WorkingMemory = require("./WorkingMemory.js").WorkingMemory;


const mandatory = (p)=> {throw `Missing parameter:${p}`};
const unimplemented = (fn) => {throw `Unimplemented method:${fn}`};









//*******************************************************************************************
//																	RETE Network
//*******************************************************************************************
class ReteNetwork {
    constructor(data={
        rules: {}
        , alphaNodeIndexes: {}
    }) {
        this.data = data;
    }

    get rules() {
        return this.data.rules;
    }
	get alphaNodeIndexes() {
		return this.data.alphaNodeIndexes;
	}
    registerRule(rule=mandatory("rule")) {
        console.debug(this.rules);
        if(this.rules.hasOwnProperty(rule.name)) throw `Duplicate rule name detected:${rule.name}`;

        this.rules[rule.name] = rule;

        let node = this._registerPredicate(rule);
        let endNode = new BetaEndpoint(rule);
        node.out.push(endNode);
        endNode.in.push(node);

    };

	// This is a routing function
    _registerPredicate(rule=mandatory("rule"), predicate) {
    	if(predicate==null){
    		var constructor = rule.predicate.constructor.name;
    		predicate = rule.predicate;
    	} else var constructor = predicate.constructor.name;
		console.debug("Registering:" + constructor);
    	switch(constructor) {
		case "PredOr": return this._registerPredOr(rule, predicate);
		case "PredAnd": return this._registerPredAnd(rule, predicate);
		case "PredSentence": return this._registerPredSentence(rule, predicate);
		default:
    			throw `Predicate of type:${constructor} is not supported.`;
    	}

    };

	_registerPredOr(rule=mandatory("rule"), predicate=mandatory("predicate")) {
//     	let predicate = rule.predicate;
		
		let nodes = predicate.data.predicates.map(pred=>{
			return this._registerPredicate(rule, pred);
		});

		// Linking
		let betaNode = new BetaOrNode(nodes);

		nodes.forEach(node=>{ 
			node.out.push(betaNode);
			betaNode.in.push(node);
		});
		return betaNode;
	};

	_registerPredAnd(rule=mandatory("rule"), predicate=mandatory("predicate")) {
//     	let predicate = rule.predicate;
		
		let nodes = predicate.data.predicates.map(pred=>{
			return this._registerPredicate(rule, pred);
		});
		// Linking
		let betaNode = new BetaAndNode(nodes);

		nodes.forEach(node=>{ 
			node.out.push(betaNode);
			betaNode.in.push(node);
		});
		return betaNode;

	};

	_registerPredSentence(rule=mandatory("rule"), predicate=mandatory("predicate")) {
    	predicate = predicate || rule.predicate;
		var node = this._alphaSentenceNode(predicate.sentence);
		console.debug("registered:" + predicate.sentence);
		//node.ruleLink.push(rule);

		return node;

	}

    _alphaSentenceNode(sentence=mandatory("sentence")) {
    	if(!this.alphaNodeIndexes.hasOwnProperty(sentence)) {
	        var newNode = new AlphaSentenceNode();
	        this.alphaNodeIndexes[sentence] = newNode;
	     	console.debug("Adding:" + sentence);
    	}

    	let node = this.alphaNodeIndexes[sentence];

    	return node;
    }


    queryRuleFromSentence(sentence=mandatory("sentence")){
    	return this.queryRuleFromSentence_V1(sentence);
    };

    queryRuleFromSentence_V1(sentence=mandatory("sentence")){
		var newConflictSet = [];

    	if(_.isString(sentence)) sentence = [sentence];

		var alphaNodes = this.alphaNodeIndexes;
    	sentence.forEach(s=>{
    		console.debug(`queryRuleFromSentence_V1:s=[${s}]`);
			if(!alphaNodes.hasOwnProperty(s)) throw `Unrecognized sentence:[${s}]`;

    		var currentAlphaNode = alphaNodes[s];

			currentAlphaNode.on = true;
    		console.debug(`queryRuleFromSentence_V1:currentAlphaNode.on=[${currentAlphaNode.on}]`);

    		console.debug(`queryRuleFromSentence_V1:Processing sentence[${s}. It has out links#[${currentAlphaNode.out.length}]]`)
    		if(currentAlphaNode.out.length>0) {
    			currentAlphaNode.out.forEach(node=>{
    				if(node instanceof BetaEndpoint) newConflictSet.push(node.rule);
    				else {
    					let endNodesRules = this.retrieveMatchRule(node);
    					newConflictSet = newConflictSet.concat(endNodesRules);
    				}
    			});
    		}
    	});

		let returnSet = _.uniq(newConflictSet);
		console.debug("queryRuleFromSentence_V1:" + returnSet)
		return returnSet;
    }

    retrieveMatchRule(node = mandatory("node")) {
		var constructor = node.constructor.name;
    	switch(constructor) {
    		case "BetaEndPoint" : 
    			console.log(`retrieveMatchRule:BetaEndPoint->${node}`)
    			return node;

    		case "BetaAndNode": 
    			var retNode = this._matchAndNode(node);
    			console.log(`retrieveMatchRule:BetaAndNode->${retNode}`)
    			return retNode;

    		case "BetaOrNode": 
    			var retNode = this._matchOrNode(node);
    			console.log(`retrieveMatchRule:BetaOrNode->${retNode}`)
    			return retNode;

    		default: throw `Logic for Constructor:[${constructor}] is not implemented.`
    	}
    	
		console.log(`retrieveMatchRule:[]`)
		return [];
    }

	_matchAndNode(node = mandatory("node")) {
		// Checking upstream nodes. If anyone is false, this node is false
		let found = node.in.find(node=>{
			return !node.on;
		});

		// if something found means there is a false in the AND node
		if(!_.isUndefined(found)) {
			console.debug(`_matchAndNode:Invalid. Found:${found}`);
			console.debug(found);
			return [];
		}

		console.log(`_matchAndNode:AND node pass with ${node.in.length} parameters`)
		node.on=true;
		var matched = [];
		node.out.forEach(node=>{
			if(node instanceof BetaEndpoint) matched.push(node.rule);
			else {
				matched = matched.concat( this.retrieveMatchRule(node) );
	
			}
		});

		return matched;
	}

	_matchOrNode(node = mandatory("node")) {
		// Checking upstream nodes. If anyone is false, this node is false
		let found = node.in.find(node=>{
			return node.on;
		});

		if(_.isUndefined(found)) {
			console.debug(`_matchOrNode:Invalid. Found:${found}`);
			console.debug(found);
			return [];
		} else {
			console.debug(`_matchOrNode:valid because of:${found}`);
			console.debug(found);
		}

		node.on=true;
		var matched = [];
		node.out.forEach(node=>{
			if(node instanceof BetaEndpoint) matched.push(node.rule);
			else {
				matched = matched.concat( this.retrieveMatchRule(node) );
			}
		});

		return matched;

	}
    //DEPRECATING:BEGIN
    queryRuleFromSentence_V0(sentence=mandatory("sentence")){
    	if(_.isString(sentence)) sentence = [sentence];

    	// For graph traversal
		var visited = [];
		var queue = [];
		var newConflictSet = [];

		var alphaNodes = this.alphaNodeIndexes;
    	sentence.forEach(s=>{
			if(!alphaNodes.hasOwnProperty(s)) throw `Unrecognized sentence:[${s}]`;

    		var currentAlphaNode = alphaNodes[s];
    		console.debug(`queryRuleFromSentence_V0:Processing sentence[${s}. It has rulelink#[${currentAlphaNode.out.length}]]`)
    		if(currentAlphaNode.out.length>0) {
    			currentAlphaNode.out.forEach(node=>{
    				if(node instanceof BetaEndpoint) newConflictSet.push(node.rule);
    				else {
	    				queue.push(node); // loading up non terminal nodes to be processed further
	    								  // using the graph queue search method
    				}
    			});
    		}

    		// Support multiple levels
			while(queue.length>0) {
				var currentNode = queue.shift();
				if(currentNode instanceof BetaEndpoint) newConflictSet.push(currentNode.rule);
				else {
					queue = queue.concat(currentNode.out);
				}

			}
    	});
    	return newConflictSet;
    }

    //DEPRECATING:END

}










//*******************************************************************************************
//                                            Node
//*******************************************************************************************

class Node {
	constructor(data={
		in: []
		, out: []
		, on: false
	}) {
		this.data = data;
	}

	get in(){return this.data.in;}
	get out(){return this.data.out;}
	get on(){return this.data.on;}
	set on(cond=mandatory("on")) {
		if(cond===true || cond===false) this.data.on = cond;
	}
}
class AlphaNode extends Node  { constructor(data){super(data)} };
class AlphaSentenceNode extends AlphaNode  { constructor(data){super(data)} };
class AlphaFactNode extends AlphaNode  { constructor(data){super(data)} };
class BetaNode extends Node { constructor(data){super(data)} };
class BetaAndNode extends BetaNode {
	constructor(nodes=mandatory("nodes"), data) {
		super(data);
		this.data.nodes = nodes
	}
}

class BetaOrNode extends BetaNode {
	constructor(nodes=mandatory("nodes"), data) {
		super(data);
		this.data.nodes= nodes;
	}
}

class BetaEndpoint extends BetaNode {
	constructor(rule=mandatory("rule"), data) {
		super(data);
		this.data.rule = rule;
	}
	get rule() {
		return this.data.rule;
	}

}
















QUnit.module("ReteNetwork", (hooks)=>{
	
	
	
	
	
	
	//*******************************************************************************************
	//                                            Under Development
	//*******************************************************************************************
	QUnit.module("_Development",(hooks)=>{
		let wm;
		let rb;
		let rete;

		hooks.beforeEach((assert) => {
            wm = new WorkingMemory();
            rb = new RuleBuilder();
            rete = new ReteNetwork();
		});

		QUnit.test("Multilevel test - complex test", (assert)=>{
		    var rule = rb
		    .rule("Good weekend")
				.sentence("Sat sunny")
				.sentence("Sun sunny")
				.or()
					.sentence("Friends around")
					.sentence("Brothers around")
				.end()
				.or()
					.sentence("There is booz")
					.sentence("There is bbq")
					.sentence("There is meat")
				.end()
            .then().logRuleName().build();
            rete.registerRule(rule);
			var result = rete.queryRuleFromSentence(["Sat sunny", "Sun sunny", "Brothers around", "There is booz"]);
			assert.ok(result.length===1, `result.length=${result.length}`);
			assert.ok(result[0].name=="Good weekend", `result.length=${result[0].name}`);


		});
	});

	
	
	
	
	
	
	//*******************************************************************************************
	//                                            AND Rules
	//*******************************************************************************************
	
	QUnit.module("AND Test",(hooks)=>{
		let wm;
		let rb;
		let rete;

		hooks.beforeEach((assert) => {
            wm = new WorkingMemory();
            rb = new RuleBuilder();
            rete = new ReteNetwork();
		});

		QUnit.test("AND test enough rules", (assert)=>{
		    var rule = rb
		    .rule("A is fierce")
				.sentence("A roars")
				.sentence("A growls")
				.sentence("A has sharp teeth")
            .then().logRuleName().build();
            rete.registerRule(rule);

		    var rule = rb
		    .rule("A is kind")
				.sentence("A purrs")
				.sentence("A eat vege")
				.sentence("A no sharp teeth")
            .then().logRuleName().build();
            rete.registerRule(rule);

			assert.ok(true,"Testing not enough rules");
			var result = rete.queryRuleFromSentence(["A roars", "A growls", "A has sharp teeth"]);
			assert.ok(result.length===1, `result.length=${result.length}`);
			assert.ok(result[0].name=="A is fierce", `result.length=${result[0].name}`);
		});

		QUnit.test("AND test not enough rules", (assert)=>{
		    var rule = rb
		    .rule("A is fierce")
				.sentence("A roars")
				.sentence("A growls")
				.sentence("A has sharp teeth")
            .then().logRuleName().build();
            rete.registerRule(rule);

		    var rule = rb
		    .rule("A is kind")
				.sentence("A purrs")
				.sentence("A eat vege")
				.sentence("A no sharp teeth")
            .then().logRuleName().build();
            rete.registerRule(rule);

			assert.ok(true,"Testing not enough rules");
			var result = rete.queryRuleFromSentence(["A roars", "A growls"]);
			assert.ok(result.length===0, `result.length=${result.length}`);
		});
	});

	
	
	
	
	
	
	
	//*******************************************************************************************
	//                                            OR Rules
	//*******************************************************************************************
	
	QUnit.module("OR Rules",(hooks)=>{
		let wm;
		let rb;
		let rete;

		hooks.beforeEach((assert) => {
            wm = new WorkingMemory();
            rb = new RuleBuilder();
            rete = new ReteNetwork();
		});


		QUnit.test("Matcher:OR suppoort-match with 2 rule - 1 match", (assert)=>{
		    var rule = rb
		    .rule("A is fierce")
		    	.or()
                	.sentence("A roars")
                	.sentence("A growls")
				.end()
            .then().logRuleName().build();
            rete.registerRule(rule);

		    var rule = rb
		    .rule("A is cat family")
		    	.or()
                	.sentence("A is a tiger")
                	.sentence("A is a lion")
                	.sentence("A is a cat")
				.end()
            .then().logRuleName().build();
            rete.registerRule(rule);


			var result = rete.queryRuleFromSentence(["A roars", "A growls"]);
			assert.ok(result.length===1, `result.length=${result.length}`);
			assert.ok(result[0].name=="A is fierce", `result=${result[0].name}`);

		});

		QUnit.test("Matcher:OR suppoort-match with 2 rule - 2 match", (assert)=>{
		    var rule = rb
		    .rule("A is fierce")
		    	.or()
                	.sentence("A is a tiger")
                	.sentence("A is a lion")
				.end()
            .then().logRuleName().build();
            rete.registerRule(rule);

		    var rule = rb
		    .rule("A is cat family")
		    	.or()
                	.sentence("A is a tiger")
                	.sentence("A is a lion")
                	.sentence("A is a cat")
				.end()
            .then().logRuleName().build();
            rete.registerRule(rule);


			var result = rete.queryRuleFromSentence(["A is a tiger", "A is a lion"]);
			assert.ok(result.length===2, `result.length=${result.length}`);
			assert.ok(result[0].name=="A is fierce", `result=${result[0].name}`);
			assert.ok(result[1].name=="A is cat family", `result=${result[1].name}`);

		});

		QUnit.test("Matcher:OR suppoort - single rule", (assert)=>{
		    var rule = rb
		    .rule("A is vicious")
		    	.or()
                	.sentence("A is a tiger")
                	.sentence("A is a lion")
				.end()
            .then().logRuleName().build();
            rete.registerRule(rule);

			var result = rete.queryRuleFromSentence("A is a tiger");
			assert.ok(result.length===1, `result.length=${result.length}`);
			assert.ok(result[0].name=="A is vicious", `result=${result[0].name}`);

			var result = rete.queryRuleFromSentence("A is a lion");
			assert.ok(result.length===1, `result.length=${result.length}`);
			assert.ok(result[0].name=="A is vicious", `result=${result[0].name}`);


			try{
				var result = rete.queryRuleFromSentence("A is a cat");	
				assert.ok(false, "Exception expected");
			} catch (e) {
		        assert.ok(true, `Exception:${e}`);
		        assert.ok("Unrecognized sentence:[A is a cat]"==e, `Checking ${e}`);
			}

		});



	});

	
	
	
	
	//*******************************************************************************************
	//                                            Simple Tests
	//*******************************************************************************************
	QUnit.module("Simple sentence",(hooks)=>{
		let wm;
		let rb;
		let rete;

		hooks.beforeEach((assert) => {
            wm = new WorkingMemory();
            rb = new RuleBuilder();
            rete = new ReteNetwork();
		});




		QUnit.test("Matcher:queryRuleFromSentence(sentence)", (assert)=>{
		    var rule = rb
		    .rule("A is vicious")
                .sentence("A is a tiger")
            .then().logRuleName().build();
            rete.registerRule(rule);

		    var rule = rb
		    .rule("A has sharp claws")
                .sentence("A is a tiger")
            .then().logRuleName().build();
            rete.registerRule(rule);

			var result = rete.queryRuleFromSentence("A is a tiger");
			assert.ok(result.length===2, `result.length=${result.length}`);
			assert.ok(result[0].name=="A is vicious", `result=${result[0].name}`);
			assert.ok(result[1].name=="A has sharp claws", `result=${result[1].name}`);

		});

		QUnit.test("Register empty rule", (assert)=>{
		    try{
    		    rete.registerRule();
    		    assert.ok(false, "Should have exception");
		    } catch (e) {
		        assert.ok(true, `Exception:${e}`);
		        assert.ok("Missing parameter:rule"==e, `Checking ${e}`);
		    }

		});
		QUnit.test("Duplicate rule registered", (assert)=>{
		    let rule1 = rb
		    .rule("A is vicious")
                .sentence("A is a tiger")
            .then().logRuleName().build();
            rete.registerRule(rule1);

		    try{
	            rete.registerRule(rule1);
    		    assert.ok(false, "Should have exception");
		    } catch (e) {
		        assert.ok(true, `Exception:${e}`);
		        assert.ok("Duplicate rule name detected:A is vicious"==e, `Checking ${e}`);
		    }

		});


		QUnit.test("Register a repeated predicate rule", (assert)=>{
		    var rule = rb
		    .rule("A is vicious")
                .sentence("A is a tiger")
            .then().logRuleName().build();
            rete.registerRule(rule);

		    var rule = rb
		    .rule("A has sharp claws")
                .sentence("A is a tiger")
            .then().logRuleName().build();
            rete.registerRule(rule);

		    var rule = rb
		    .rule("A has sharp teeth")
                .sentence("A is a tiger")
            .then().logRuleName().build();
            rete.registerRule(rule);

			var result = Object.keys(rete.rules);
			assert.ok(result.length===3, `result.length=${result.length}`);
			assert.ok(result[0]=="A is vicious", `result=${result[0]}`);
			assert.ok(result[1]=="A has sharp claws", `result=${result[1]}`);
			assert.ok(result[2]=="A has sharp teeth", `result=${result[2]}`);

			var result = Object.keys(rete.alphaNodeIndexes);
			assert.ok(result.length===1, `result.length=${result.length}`);
			assert.ok(result[0]=="A is a tiger", `result=${result[0]}`);

			var result = rete.alphaNodeIndexes["A is a tiger"].out
			assert.ok(result.length===3, result.length);
			assert.ok(result[0].rule.name=="A is vicious", result[0].rule.name);
			assert.ok(result[1].rule.name=="A has sharp claws", result[1].rule.name);
			assert.ok(result[2].rule.name=="A has sharp teeth", result[2].rule.name);

		});

		QUnit.test("Register a simple rule", (assert)=>{
		    
		    let rule1 = rb
		    .rule("A is vicious")
                .sentence("A is a tiger")
            .then().logRuleName().build();
            rete.registerRule(rule1);

		    let rule2 = rb
		    .rule("A is gentle")
                .sentence("A is a rabbit")
            .then().logRuleName().build();
            rete.registerRule(rule2);
			
			var result = Object.keys(rete.rules);
			assert.ok(result.length===2, `result.length=${result.length}`);
			assert.ok(result[0]=="A is vicious", `result=${result[0]}`);
			assert.ok(result[1]=="A is gentle", `result=${result[1]}`);

			var result = Object.keys(rete.alphaNodeIndexes);
			assert.ok(result.length===2, `result.length=${result.length}`);
			assert.ok(result[0]=="A is a tiger", `result=${result[0]}`);
			assert.ok(result[1]=="A is a rabbit", `result=${result[1]}`);

			var result = rete.alphaNodeIndexes["A is a rabbit"].out
			assert.ok(result.length===1, result.length);
			assert.ok(result[0].rule.name=="A is gentle", result[0].rule.name);

			var result = rete.alphaNodeIndexes["A is a tiger"].out
			assert.ok(result.length===1, result.length);
			assert.ok(result[0].rule.name=="A is vicious", result[0].rule.name);
		});
	});


});
