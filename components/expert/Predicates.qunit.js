const PredicateModule = require("./Predicates.js");
const PredFact = PredicateModule.PredFact;
const PredNot  = PredicateModule.PredNot;
const PredAnd  = PredicateModule.PredAnd;
const PredSentence  = PredicateModule.PredSentence;
const PredOr  = PredicateModule.PredOr;
const PredicateBuilder = PredicateModule.PredicateBuilder;
const Predicate = PredicateModule.Predicate;
const WorkingMemory = require("./WorkingMemory.js").WorkingMemory;

function makeWorkingMemory(strArray) {
	if(_.isString(strArray)) strArray = [strArray];
	if(!_.isArray(strArray)) throw "Expecting string or array";

	let wm = new WorkingMemory();
	strArray.forEach( (str) =>{
		wm.global(str);
	});
	return wm;
	
}

class PredFactCondFact extends Predicate {
	constructor(left, cond, right) {
		super();

		if(left==null) throw "left is mandatory";
		if(right==null) throw "right is mandatory";
		if(cond==null) throw "cond is mandatory";
		if(!_.isFunction(this["fn_" + cond])) throw `cond:${cond}  is not recognized to be a operator`;


		this.data = {
			left: left
			, right: right
			, cond: cond
		}
	}

	isMatch(workingMemory=null) {
		if(workingMemory==null) throw "workingMemory is mandatory";

		let leftval = workingMemory.facts(this.data.left);
		let rightval = workingMemory.facts(this.data.right);
		let cond = this.data.cond;
		return this["fn_" + cond](leftval, rightval);
	}

	fn_eq(left,right) {
		return left==right;
	}

}
QUnit.module("Fact Equa To Fact", (hooks)=>{


	// Test objects
	QUnit.module("_Development",(hooks)=>{
		let wm;

		hooks.beforeEach((assert) => {
			wm = new WorkingMemory();
			wm.facts("pet.a", "dog")
			.facts("pet.b", "cat")
			.facts("pet.c", "dog")
			.facts("pet.d", "cat");

		});

		QUnit.test("Undefined condition", (assert)=>{
			try{
				let predicateDogDog = new PredFactCondFact("pet.a", "xxx", "pet.c");
				assert.ok(false, "Exception should occur");
			} catch (e) {
				assert.ok(true, `Exception means correct:${e}`);
				assert.ok(e=="cond:xxx  is not recognized to be a operator");
			}
		});

		QUnit.test("Simple eq test", (assert)=>{
			let predicateDogDog = new PredFactCondFact("pet.a", "eq", "pet.c");
			let predicateDogCat = new PredFactCondFact("pet.a", "eq", "pet.b");
			let predicateCatCat = new PredFactCondFact("pet.b", "eq", "pet.d");

			var result = predicateDogDog.isMatch(wm);
			assert.ok(result===true, `result=${result}`);
			var result = predicateDogCat.isMatch(wm);
			assert.ok(result===false, `result=${result}`);
			var result = predicateCatCat.isMatch(wm);
			assert.ok(result===true, `result=${result}`);

		});
	});
});




QUnit.module("PredicateBuilder", (hooks)=>{


	// Test objects
	QUnit.module("_Development",(hooks)=>{
		hooks.beforeEach((assert) => {

		});
		hooks.afterEach((assert) => {

		});

		QUnit.test("Mixture of sentence and facts", (assert)=>{
		    const wm = makeWorkingMemory([
                "A is beautiful"
                , "B is beautiful"
                , "C is beautiful"
		    ])
		    .facts("pet.species", "dog")
		    ;
			let predicate = (new PredicateBuilder())
			.sentence("A is beautiful")
			.sentence("B is beautiful")
			.sentence("C is beautiful")
			.or()
			     .fact("pet.species", "dog")
			     .fact("pet.species", "cat")
			     .fact("pet.species", "iguana")
            .end()
			.build();

			var result = predicate.isMatch(wm);
			assert.ok(result===true, `result=${result}`);

		});

		QUnit.test("Predicate Builder Default", (assert)=>{
			let predicate = (new PredicateBuilder())
			.sentence("A is beautiful")
			.sentence("B is beautiful")
			.build();

			var result = predicate.isMatch(makeWorkingMemory([
				"A is beautiful", "B is beautiful"
			]));
			assert.ok(result===true, `result=${result}`);

			var result = predicate.isMatch(makeWorkingMemory([
				"A is beautiful"
			]));
			assert.ok(result===false, `result=${result}`);
		});

		QUnit.test("Predicate Builder Simple Nested", (assert)=>{
			let predicate = (new PredicateBuilder())
				.and()
					.sentence("Monday is sunny")
					.sentence("Tuesday is sunny")
					.sentence("Wednesday is sunny")
					.or()
						.sentence("Friday is rainy")
						.sentence("Friday is cloudy")
					.end()
					.sentence("Thursday is sunny")
				.end()
				.build()
			;

			var result = predicate.isMatch(makeWorkingMemory([
				"Monday is sunny"
				, "Tuesday is sunny"
				, "Wednesday is sunny"
				, "Thursday is sunny"
				, "Friday is rainy"
			]));
			assert.ok(result===true, "result=" + result);

			var result = predicate.isMatch(makeWorkingMemory([
				"Monday is sunny"
				, "Tuesday is sunny"
				, "Wednesday is sunny"
				, "Thursday is sunny"
				, "Friday is cloudy"
			]));
			assert.ok(result===true, "result=" + result);

			var result = predicate.isMatch(makeWorkingMemory([
				"Monday is sunny"
				, "Tuesday is sunny"
				, "Wednesday is sunny"
				, "Thursday is sunny"
				, "Friday is sunny"
			]));
			assert.ok(result===false, "result=" + result);

		});
	});
});


QUnit.module("PredFact tests", (hooks)=>{
	// Test objects
	
	QUnit.module("_Development",(hooks)=>{
		let wm;
		hooks.beforeEach((assert) => {
			wm = new WorkingMemory();
			wm.facts("day", "Tuesday");
			wm.facts("pet.animal", "Dog");
			wm.facts("null.test",null);
			wm.facts("nothing", null);
		});
		hooks.afterEach((assert) => {});


		QUnit.test("null test", (assert)=>{
			var pred = new PredFact("null.test", null);
			var result = pred.isMatch(wm);
			assert.ok(result===true, `result=${result}`);

			var pred = new PredFact("nothing", null);
			var result = pred.isMatch(wm);
			assert.ok(result===true, `result=${result}`);

			var pred = new PredFact("shouldbeundefined", null);
			var result = pred.isMatch(wm);
			assert.ok(result===false, `result=${result}`);

		});

		QUnit.test("fact Not defined test", (assert)=>{
			var pred = new PredFact("space", "Dog");
			var result = pred.isMatch(wm);
			assert.ok(result===false, `result=${result}`);

			var pred = new PredFact("pet.sex", "male");
			var result = pred.isMatch(wm);
			assert.ok(result===false, `result=${result}`);

		});

		QUnit.test("2 level property constant test", (assert)=>{
			var pred = new PredFact("pet.animal", "Dog");
			var result = pred.isMatch(wm);
			assert.ok(result===true, `result=${result}`);

			var pred = new PredFact("pet.animal", "Cat");
			var result = pred.isMatch(wm);
			assert.ok(result===false, `result=${result}`);

		});

		QUnit.test("Simple 1 level fact constant test", (assert)=>{
			var pred = new PredFact("day", "Tuesday");
			var result = pred.isMatch(wm);
			assert.ok(result===true, `result=${result}`);

			var pred = new PredFact("day", "Wednesday");
			var result = pred.isMatch(wm);
			assert.ok(result===false, `result=${result}`);
		});
	});
});

QUnit.module("Predicates", (hooks)=>{
	// Test objects
	
	QUnit.module("_Development",(hooks)=>{
		hooks.beforeEach((assert) => {

		});
		hooks.afterEach((assert) => {

		});

	});


	QUnit.module("AND/OR/NOT",(hooks)=>{
		hooks.beforeEach((assert) => {

		});
		hooks.afterEach((assert) => {

		});

		QUnit.test("Test Simple NOT predicate using array", (assert)=>{	
			try {
				var notPredicate = new PredNot( [new PredSentence("Tuesday is sunny")]);
				assert.ok(false, "Exception expected");
			} catch (e) {
				assert.ok(true, "There is exception");
				assert.ok(e=="PredNot expects a single predicate and does not support array", e);
			}
		});

		QUnit.test("Test Simple NOT predicate", (assert)=>{
			var notPredicate = new PredNot(new PredSentence("Tuesday is sunny"));
			var result = notPredicate.isMatch(makeWorkingMemory([
				"Tuesday is rainy"
				, "   Monday is rainy   "
			]));

			assert.ok(result===true, "Result is:" + result );

			var result = notPredicate.isMatch(makeWorkingMemory([
				"   Tuesday is sunny  "
				, "   Monday is rainy   "
			]));

			assert.ok(result===false, "Result is:" + result );

		});
		QUnit.test("Negative Test Simple OR predicate using array", (assert)=>{
			var orPredicate = new PredOr([
				new PredSentence("Tuesday is sunny")
				, new PredSentence("Monday is also sunny")
			]);

			let result = orPredicate.isMatch(makeWorkingMemory([
				"Tuesday is rainy"
				, "   Monday is rainy   "
			]));

			assert.ok(result===false, "Result is:" + result );
		});
		QUnit.test("Positive Test Simple OR predicate using array", (assert)=>{
			var orPredicate = new PredOr([
				new PredSentence("Tuesday is sunny")
				, new PredSentence("Monday is rainy")
			]);

			let result = orPredicate.isMatch(makeWorkingMemory([
				"Tuesday is rainy"
				, "   Monday is rainy   "
			]));
			assert.ok(result===true, "Result is:" + result );
		});

		QUnit.test("Negative Test Simple AND predicate using array", (assert)=>{
			var andPredicate = new PredAnd([
				new PredSentence("Tuesday is sunny")
				, new PredSentence("Monday is also sunny")
			]);

			let result = andPredicate.isMatch(makeWorkingMemory([
				"Tuesday is sunny"
				, "   Monday is rainy   "
			]));

			assert.ok(result===false, "Result is:" + result );
		});


		QUnit.test("Positive Test Simple AND predicate using array", (assert)=>{
			var andPredicate = new PredAnd([
				new PredSentence("Tuesday is sunny")
				, new PredSentence("Monday is rainy")
			]);

			let result = andPredicate.isMatch(makeWorkingMemory([
				"Tuesday is sunny"
				, "   Monday is rainy   "
			]));

			assert.ok(result===true, "Result is:" + result );
		});


		QUnit.test("Test Simple AND predicate using non array", (assert)=>{
			try{
				var pred = new PredAnd("Today is sunny");
				assert.ok(true, "Should be ok");
			} catch(e) {
				assert.ok(false, "Should not throw exception");
			}
		});
		
	});

	QUnit.module("Sentence Predicate",(hooks)=>{
		hooks.beforeEach((assert) => {

		});
		hooks.afterEach((assert) => {

		});
		QUnit.test("Undefined property", (assert)=>{
			const wm = new WorkingMemory();
			wm.facts("day.current", "Tuesday")
			.global("{{Tuesday}} to be raining");
			var p = new PredSentence("{{day.current}} to be raining");
			var result = p.isMatch(wm);
			assert.ok(result===true, `result=${result}`);
			var p = new PredSentence("{{day.UNKNOWN}} to be raining");
			var result = p.isMatch(wm);
			assert.ok(result===false, `result=${result}`);
		});

		QUnit.skip("TODO:Single Variable String at the middle PredSentence Test", (assert)=>{
			var p = new PredSentence("It needs to be {{day}} to be raining");
			assert.ok(p.varname()=="day", "varname:" + p.varname());

			var result = p.isMatch("It needs to be {{Tuesday}} to be raining", {
				day: "Tuesday"
			})
			assert.ok(result===true, "result:" + result);
			var result = p.isMatch("It needs to be {{Wednesday}} to be raining", {
				day: "Tuesday"
			})
			assert.ok(result===false, "result:" + result);

		});

		QUnit.test("Single Variable String PredSentence Test", (assert)=>{
			var p = new PredSentence("Tuesday a sunny day");
			assert.ok(p.varname()=="", "varname:" + p.varname());

			var p = new PredSentence("{{day}} is a sunny day");
			assert.ok(p.varname()=="day", "varname:" + p.varname());

			var wm = makeWorkingMemory("{{Tuesday}} is a sunny day");
			wm.facts("day", "Tuesday");
			var result = p.isMatch(wm);
			assert.ok(result===true, "result:" + result);

			var wm = makeWorkingMemory("{{Wednesday}} is a sunny day");
			wm.facts("day", "Tuesday");
			var result = p.isMatch(wm);
			assert.ok(result===false, "result:" + result);

		});
		QUnit.test("Simple String PredSentence Test", (assert)=>{
			try{
				var p = new PredSentence("Its a sunny day");
			} catch (e) {
				assert.ok(false, e);
			}

			// Checking if PredSentence holds
			var result = p.isMatch(makeWorkingMemory("Its a sunny day"));
			assert.ok(result===true, result);

		});
		
		QUnit.test("Blank PredSentence Test", (assert)=>{
			try{
				let p = new PredSentence();
			} catch (e) {
				assert.ok(e="Cannot have a empty string PredSentence", "Exception occured:" + e);
			}

		});


	});
});
