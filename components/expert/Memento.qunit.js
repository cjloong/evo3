var makeMemento = require("./Memento.js").makeMemento;
var WorkingMemory = require("./WorkingMemory.js").WorkingMemory;

QUnit.module("Working Memory Memento", (hooks)=>{
	QUnit.module("_Development",(hooks)=>{
		hooks.beforeEach((assert) => {

		});
		hooks.afterEach((assert) => {

		});
		QUnit.test("Min test", (assert)=>{
			const Memento = makeMemento(WorkingMemory);
			const memento = new Memento();
			let result;
			let test;

			try{
				memento.undo();
				assert.ok(false, "Should have exception");
			} catch (e) {
				assert.ok(true, "Assertion occured:" + e);
				assert.ok(e=="Nothing to undo.");
			}

			memento.facts("pet", "dog");
			memento.undo();
			result = memento.facts("pet");
			assert.ok(_.isUndefined(result), `result:${result}`);

			try{
				memento.undo();
				assert.ok(false, "Should have exception");
			} catch (e) {
				assert.ok(true, "Assertion occured:" + e);
				assert.ok(e=="Cannot undo further.");
			}

			memento.redo();
			result = memento.facts("pet");
			assert.ok(result=="dog", `result:${result}`);

			try{
				memento.redo();
				assert.ok(false, "Should have exception");
			} catch (e) {
				assert.ok(true, "Assertion occured:" + e);
				assert.ok(e=="Cannot redo further.");
			}

		});
		QUnit.test("Memento undo/redo too much", (assert)=>{
			const Memento = makeMemento(WorkingMemory);
			const memento = new Memento();
			let result;
			let test;

			memento.facts("pet.dog","Bobby");
			memento.save();
			memento.facts("pet.dog","Rocky");
			memento.save();
			memento.facts("pet.dog","Blacky");
			
			test = "UNDO:";
			result = memento.facts("pet.dog");
			assert.ok(result=="Blacky", `${test}-->result:${result}`);
			
			memento.undo();
			result = memento.facts("pet.dog");
			assert.ok(result=="Rocky", `${test}-->result:${result}`);

			memento.undo();
			result = memento.facts("pet.dog");
			assert.ok(result=="Bobby", `${test}-->result:${result}`);

			memento.undo();
			result = memento.facts("pet.dog");
			assert.ok(_.isUndefined(result), `${test}-->result:${result}`);
			try{
				memento.undo();
				assert.ok(false, "Should have exception");
			} catch (e) {
				assert.ok(true, `Exception [${e}] caught`);
				assert.ok(e=="Cannot undo further.", `Exception [${e}] caught`);
			}

			test = "REDO:";
			memento.redo();
			result = memento.facts("pet.dog");
			assert.ok(result=="Bobby", `${test}-->result:${result}`);
			memento.redo();
			result = memento.facts("pet.dog");
			assert.ok(result=="Rocky", `${test}-->result:${result}`);
			memento.redo();
			result = memento.facts("pet.dog");
			assert.ok(result=="Blacky", `${test}-->result:${result}`);
			try{
				memento.redo();
				assert.ok(false, "Should have exception");
			} catch (e) {
				assert.ok(true, `Exception [${e}] caught`);
				assert.ok(e=="Cannot redo further.", `Exception [${e}] caught`);
			}

		});
		QUnit.test("Memento undo/redo", (assert)=>{
			const Memento = makeMemento(WorkingMemory);
			const memento = new Memento();
			let result;

			memento.facts("pet.dog", "Bobby");
			memento.save();
			memento.facts("pet.cat", "Garfield");
			memento.save();
			memento.facts("pet.dog", "Rocky");
			memento.save();
			memento.facts("pet.cat", "Fat Cat");
 			memento.save();

			result = memento.facts("pet.cat");
			assert.ok(result=="Fat Cat", `result=${result}`);
			result = memento.facts("pet.dog");
			assert.ok(result=="Rocky", `result=${result}`);

			var test = "UNDO:";
			memento.undo();
			result = memento.facts("pet.cat");
			assert.ok(result=="Garfield", `${test} result=${result}`);
			result = memento.facts("pet.dog");
			assert.ok(result=="Rocky", `${test} result=${result}`);

			memento.undo();
			result = memento.facts("pet.cat");
			assert.ok(result=="Garfield", `${test} result=${result}`);
			result = memento.facts("pet.dog");
			assert.ok(result=="Bobby", `${test} result=${result}`);

			memento.undo();
			result = memento.facts("pet.cat");
			assert.ok(_.isUndefined(result), `${test} result=${result}`);
			result = memento.facts("pet.dog");
			assert.ok(result=="Bobby", `${test} result=${result}`);

			memento.undo();
			result = memento.facts("pet.cat");
			assert.ok(_.isUndefined(result), `${test} result=${result}`);
			result = memento.facts("pet.dog");
			assert.ok(_.isUndefined(result), `${test} result=${result}`);


			var test = "REDO:";
			memento.redo();
			result = memento.facts("pet.cat");
			assert.ok(_.isUndefined(result), `${test} result=${result}`);
			result = memento.facts("pet.dog");
			assert.ok(result=="Bobby", `${test} result=${result}`);

			memento.redo();
			result = memento.facts("pet.cat");
			assert.ok(result=="Garfield", `${test} result=${result}`);
			result = memento.facts("pet.dog");
			assert.ok(result=="Bobby", `${test} result=${result}`);

			memento.redo();
			result = memento.facts("pet.cat");
			assert.ok(result=="Garfield", `${test} result=${result}`);
			result = memento.facts("pet.dog");
			assert.ok(result=="Rocky", `${test} result=${result}`);

			memento.redo();
			result = memento.facts("pet.cat");
			assert.ok(result=="Fat Cat", `${test} result=${result}`);
			result = memento.facts("pet.dog");
			assert.ok(result=="Rocky", `${test} result=${result}`);




			var test = "UNDO:";
			memento.undo();
			result = memento.facts("pet.cat");
			assert.ok(result=="Garfield", `${test} result=${result}`);
			result = memento.facts("pet.dog");
			assert.ok(result=="Rocky", `${test} result=${result}`);

			memento.undo();
			result = memento.facts("pet.cat");
			assert.ok(result=="Garfield", `${test} result=${result}`);
			result = memento.facts("pet.dog");
			assert.ok(result=="Bobby", `${test} result=${result}`);

			memento.undo();
			result = memento.facts("pet.cat");
			assert.ok(_.isUndefined(result), `${test} result=${result}`);
			result = memento.facts("pet.dog");
			assert.ok(result=="Bobby", `${test} result=${result}`);

			memento.undo();
			result = memento.facts("pet.cat");
			assert.ok(_.isUndefined(result), `${test} result=${result}`);
			result = memento.facts("pet.dog");
			assert.ok(_.isUndefined(result), `${test} result=${result}`);



		});

		QUnit.test("Memento acts like Source", (assert)=>{
			const Memento = makeMemento(WorkingMemory);
			const memento = new Memento();

			memento.facts("pet.dog", "bobby");
			memento.facts("pet.cat", "Garfield");
			var result = memento.facts("pet");
			assert.ok(result.dog=="bobby", `result is:[${result.dog}]`);
			assert.ok(result.cat=="Garfield", `result is:[${result.cat}]`);
			
		});
	});
});

