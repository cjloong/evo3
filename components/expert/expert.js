class BugCatcher {
	static debugMode(on=false){
		this.on = on;
	}
	static assertShouldNotReachHere(msg="BUG:Should not reach this location") {
		if(this.on) throw msg;
		else console.error("E" + msg);
	}
}

BugCatcher.debugMode(true);

class Expert {
	constructor(data={
		workingMem:[]

	}) {

		this.data = data;
	}

	parseRule(expr) {

	}

	define(fact, lambda ) {

	}
}

class CondLogic {
	static and(...sentences) {
		return ["and"].concat(sentences);
	}
	static or(...sentences) {
		return ["or"].concat(sentences);
	}
	static not(sentence) {
		return ["not", sentence];
	}

	//
	// NOTE: Someone else need to bind the variable to bound
	//
	static eval(rule=['rule not defined'], ...bound) {
		bound = bound || [];

		switch(rule[0].trim().toLowerCase()) {
			case 'and':
				// checking any rule not in facts will be return false
				return !rule.filter((s, i)=>i!=0).some(s=>bound.indexOf(s)<0)
				break;
			case 'or':
				// Or is easy. Any rule match bound list will be true
				return rule.filter((s, i)=>i!=0).some(s=>bound.indexOf(s)>=0)
				break;
			case 'not':
				return bound.indexOf(rule[1])<0;
				break;
			default:
				throw `Invalid rule:${rule[0]}`;
		}


		BugCatcher.assertShouldNotReachHere();

	}
}


module.exports = {
    Expert: Expert
    ,CondLogic: CondLogic
}
