const ActionModule = require("./Action.js");
const AddFactAction = ActionModule.AddFactAction;
const AddGlobalAction = ActionModule.AddGlobalAction;
const LoggerAction = ActionModule.LoggerAction;
const Action = ActionModule.Action;
const WorkingMemory = require("./WorkingMemory.js").WorkingMemory;

QUnit.module("Action", (hooks)=>{
	let wm;

	// Test objects
	QUnit.module("_Development",(hooks)=>{

		hooks.beforeEach((assert) => {
		    wm = new WorkingMemory();
		});

		QUnit.skip("DeleteGlobalAction", (assert)=>{

		});

		QUnit.test("AddGlobalAction: Fact already exist will not be duplicated", (assert)=>{

			wm.global( "Today is sunny");

			let action = new AddGlobalAction("Today is sunny");
			var result = wm.global().length;
			assert.ok(result===1, `result=${result}`);
			action.exec(wm);

			var result = wm.global();
			assert.ok(result.length===1, `result=${result.length}`);
			assert.ok(result[0]=="Today is sunny", `result=${result[0]}`);
		});

		QUnit.test("AddGlobalAction: Fact does not exist", (assert)=>{

			let action = new AddGlobalAction("Today is sunny");
			var result = wm.global().length;
			assert.ok(result===0, `result=${result}`);
			action.exec(wm);

			var result = wm.global();
			assert.ok(result.length===1, `result=${result.length}`);
			assert.ok(result[0]=="Today is sunny", `result=${result[0]}`);
		});
	});

	QUnit.module("Logger Action",(hooks)=>{
		hooks.beforeEach((assert) => {
		    wm = new WorkingMemory();
		});

		QUnit.test("Logger Action min test", (assert)=>{
			let action = new LoggerAction();
			action.callback=(msg)=>{
				console.log(msg);
				assert.ok(msg=="Logger Action Called(default message)", msg);
			}
			action.exec(wm);
			assert.ok(true, "Not crashing.")
        });

		QUnit.test("Logger Action with message", (assert)=>{
			let action = new LoggerAction("this is a test message");
			action.callback=(msg)=>{
				console.log(msg);
				assert.ok(msg=="this is a test message", msg);
			}
			action.exec(wm);
			assert.ok(true, "Not crashing.")

		});

    });
});




