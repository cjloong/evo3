module.exports = {};



//
//                                 PREDICATGES
//


/*
A PredSentence is a class which holds a rule condition

IF PredSentence && PredSentence2

## Specification: PredSentence
1. A PredSentence can be constructed using a string
1. A PredSentence can be evaluated by passing in working memory.
1. PredSentence can contain simple string such as "Its a sunny day"
1. PredSentence can contain variables which will be replaced such as "?animal is a dog"

*/
class Predicate {
	isMatch(workingMemory=null) { return false }
}

class PredSentence extends Predicate {
	constructor(sentence=null) {
		super();
		
		// Assigning constants
		this.VARREGEX = /\s*({{.+}})*(.*)/;


		this._assertworkingMemory(sentence);

		var extracted = (this.VARREGEX).exec(sentence);
		console.debug(extracted);
		this.data = {
			sentence: (extracted[1]==null?extracted[0]:extracted[2]).trim()
			, varname: this.cleanVar(extracted[1])
		}
		
	}

	get sentence() {
		return this.data.sentence;
	}
	cleanVar(varName="") {
		return varName
			.replace("{{", "")
			.replace("}}", "")
			.trim();
	}
	varname() {
		return this.data.varname;
	}
	
	isMatch(workingMemory=null) {
		this._assertworkingMemory(workingMemory)
// 		if(_.isString(workingMemory)) return this._isMatchString(workingMemory, vardata);
// 		if(_.isString(workingMemory)) workingMemory = [workingMemory];

		let truePredicate = workingMemory.global().find((str)=>{
			return this._isMatchString(str, workingMemory);
		});
		
		
		return !_.isEmpty(truePredicate);
	}

	_isMatchString(str=null, workingMemory) {
		let varRegex = this.VARREGEX;
		str = str.trim();

		this._assertFacts(str);
		
		if(this.varname().trim()=="") return this.data.sentence==str;
		else {
			var extracted = (varRegex).exec(str);
			console.debug(str);
			var extractedVal = this.cleanVar(extracted[1]);

			return workingMemory.facts(this.varname())==extractedVal &&
				this.data.sentence == extracted[2].trim();
			
		}

	}

}

PredSentence._assertworkingMemory = PredSentence.prototype._assertworkingMemory = (workingMemory)=>{ if(workingMemory==null) throw "workingMemory cannot be empty" }
PredSentence._assertFacts = PredSentence.prototype._assertFacts = (workingMemory)=> {
	PredSentence._assertworkingMemory(workingMemory);
	if(!_.isString(workingMemory)) throw "workingMemory is not a string:" + workingMemory;
}


class PredAnd extends Predicate {
	constructor(predicates=[]) {
		super();

		if(!_.isArray(predicates)) predicates = [predicates];

		this.data = {
			predicates: predicates
		}
	}
	isMatch(workingMemory=null) 	{
		// Handling data type
// 		if(_.isString (workingMemory)) workingMemory = [workingMemory];

		let result = this.data.predicates.find((p)=>{
			return !p.isMatch(workingMemory);

		});

		console.debug("Result:" + result);
		return _.isEmpty(result);
	}
	
}


class PredOr extends Predicate {
	constructor(predicates=[]) {
		super();

		if(!_.isArray(predicates)) predicates = [predicates];

		this.data = {
			predicates: predicates
		}
	}
	isMatch(workingMemory=null) 	{
		// Handling data type
// 		if(_.isString (workingMemory)) workingMemory = [workingMemory];

		let result = this.data.predicates.find((p)=>{
			return p.isMatch(workingMemory);

		});

		console.debug("Result:" + result);
		return !_.isEmpty(result);
	}
	
}

class PredNot extends Predicate {
	constructor(predicate=null) {
		super();

		if(_.isEmpty(predicate)) throw "PredNot's predicate is mandatory";
		if(_.isArray(predicate)) throw "PredNot expects a single predicate and does not support array";
		if(!(predicate instanceof Predicate)) throw `predicate[${predicate}] need to be an instance of Predicate`;

		let p = new Predicate();
		this.data = {
			predicate: predicate
		}
	}
	isMatch(workingMemory=null) 	{
		// Handling data type
// 		if(_.isString (workingMemory)) workingMemory = [workingMemory];
		
		return !this.data.predicate.isMatch(workingMemory);
	}
}


class PredFact extends Predicate {
	constructor(factStr=null, equalTo) {
		super();
		if(factStr===null) throw "factStr is undefined. It is mandatory";

		this.data = {
			factStr: factStr
			, equalTo: equalTo
		}
	}

	isMatch(wm=null) {
		if(wm===null) throw "Working memory is mandatory";

		let varname = this.data.factStr;
		let val = wm.facts(varname);

		if(_.isUndefined(val)) return false;
		else return val == this.data.equalTo;
	}
}


class PredicateBuilder {
	constructor() {
		this.clear();
	}
	clear() {
		this.callstack = [{
			type: 'root'
			, predicates: []
		}];

		return this;
	}
	and() {
		this.callstack.push(
			{type:'and', predicates:[]}
		);

		return this;
	}

	or() {
		this.callstack.push(
			{type:'or', predicates:[]}
		);

		return this;
	}

	_notAtRoot() {
		return this.callstack[this.callstack.length - 1].type!="root";
	}
	_currentPredicates() {
		return this.callstack[this.callstack.length - 1].predicates;
	}
	fact(fact=null, compareTo) {
		if(fact==null) throw "fact parameter is mandatory";

		this._currentPredicates().push(new PredFact(fact, compareTo));
		return this;
	}
	
	sentence(s=null) {
		if(_.isEmpty(s)) throw "Sentence parameter is mandatory";
		if(!_.isString(s)) throw "Sentence expects a string";

		this._currentPredicates().push(new PredSentence(s));

		return this;
	}
	end() {
		let popped = this.callstack.pop();

		if(popped.type=="and") this._currentPredicates().push(new PredAnd(popped.predicates));
		else if(popped.type=="or") this._currentPredicates().push(new PredOr(popped.predicates));
		else if(popped.type=="root")  throw "Too many end. Past the last block defined";
		else throw "type is not defined. It can be one of and/or but it is:" + popped.type;

		return this;
	}

	build() {
		// Auto end
		while(this._notAtRoot()) this.end();

		// Root by default will output a AND predicate if multiple predicate detected
		let predicates = this._currentPredicates();
		if(predicates.length===1) return predicates[0];
		else return new PredAnd(predicates);
	}
}


module.exports.PredicateBuilder = PredicateBuilder;
module.exports.PredFact = PredFact;
module.exports.PredAnd = PredAnd;
module.exports.PredOr = PredOr;
module.exports.PredNot = PredNot;
module.exports.PredSentence = PredSentence;
module.exports.Predicate = Predicate;
