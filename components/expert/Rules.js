const WorkingMemory = require("./WorkingMemory.js").WorkingMemory;
const PredicateModule = require("./Predicates.js")
const PredicateBuilder = PredicateModule.PredicateBuilder;
const Predicate = PredicateModule.Predicate;

const ActionModule = require("./Action.js");
const AddFactAction = ActionModule.AddFactAction;
const AddGlobalAction = ActionModule.AddGlobalAction;
const LoggerAction = ActionModule.LoggerAction;
const Action = ActionModule.Action;

//
//								Rules Section
class Rule {
	constructor(name, predicate, actions) {
		if(name==null) throw "name is mandatory";
		if(predicate==null) throw "predicate is mandatory";
		if(actions==null) throw "actions is mandatory";
		if(! (predicate instanceof Predicate)) throw "predicate must be instance of Predicate class";
		if(! (actions instanceof Action) && !_.isArray(actions)) throw "actions must be instance of Action class or array";
		
		// Defaults
		if(actions instanceof Action) actions = [actions];

		this.data = {
			name: name
			, predicate: predicate
			, actions: actions
		}
	}

	get predicate() {
		return this.data.predicate;
	}

	get name() {
		return this.data.name;
	}
	
	_isInConflict(workingMemory) {
		if(workingMemory==null) throw "workingMemory is mandatory";
		if(! (workingMemory instanceof WorkingMemory )) throw "workingMemory need to be of type WorkingMemory";

		return this.data.predicate.isMatch(workingMemory);
	}

	_fireActions(workingMemory) {
		if(workingMemory==null) throw "workingMemory is mandatory";
		this.data.actions.forEach(a=>a.exec(workingMemory));
	}

	forwardPropagate(workingMemory) {
		if(workingMemory==null) throw "workingMemory is mandatory";

		if(this._isInConflict(workingMemory)) {
			this._fireActions(workingMemory);
		}

	}
}

class RuleBuilder {
	constructor() {
		this._initData();
	}
	_initData() {
		this.data= {
			name: "undefined"
			, predicate: null
			, actions: []
		}
		this.predicateBuilder = new PredicateBuilder();
	}

	rule(name) {
		if(name==null) throw "name is mandatory";
		this._initData();
		this.data.name=name;
		return this;
	}

	// Predicate part
	or() { this.predicateBuilder.or(); return this;}
	and() { this.predicateBuilder.and(); return this;}
	end() { this.predicateBuilder.end(); return this;}
	sentence(s) {
		if(s==null) throw "s is mandatory";

		this.predicateBuilder.sentence(s);
		return this;
	}

	then() {
		this.data.predicate = this.predicateBuilder.build();
		this.predicateBuilder.clear();

		this.data.actions=[];
		return this;
	}

	logRuleName() {
		this.data.actions.push(new LoggerAction(this.data.name));
		return this;
	}
	globalAdd(sentence) {
		this.data.actions.push(new AddGlobalAction(sentence));
		return this;
	}
	factAdd(fact, value) {
		this.data.actions.push(new AddFactAction(fact, value));
		return this;
	}

	build() {

		let rule = new Rule(this.data.name, this.data.predicate, this.data.actions);
		this._initData();
		return rule;
	}
}

module.exports = {};
module.exports.Rule = Rule;
module.exports.RuleBuilder = RuleBuilder;