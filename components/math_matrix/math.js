// eVo Simple Math library
var evoMath = function(){}

module.exports = evoMath;

evoMath.compareFloat = function(num1,num2, precision) {
	var multiplier = typeof(precision)=='undefined'? 1000000 : Math.pow(10, precision);
	return Math.round(num1 * multiplier) - Math.round(num2 * multiplier);
}


