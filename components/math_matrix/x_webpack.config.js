var path = require('path');
var webpack = require("webpack");
console.log("CWD:" + process.cwd());

module.exports = {
	context: '.'
//	, devtool: 'source-map'
	,devtool: 'eval-source-map'

    , entry: {
        qunit_matrix: path.join(__dirname, "qunit.matrix.js")
//        , matrix: __dirname +  "/matrix.js"
//        , c: ["./c", "./d"
    },
    output: {
        path: path.join(__dirname, "entries")
        , filename: "qunit.entry.js"
    }
}