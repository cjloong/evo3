# Linear Algebra
## Vector



# Statistics
## Probablity
### Independent Probablity
- coin flipping
    - What is the probability of H
        - P(H) = count of condition(1 head)/ count of all (head + tail = 2) = 1/2
$$
P(condition) = { \text {count that satisfy constraint} \over \text {count possible outcomes} }
$$

- rolling a 6 face dice
    - What is probability of 1,2,3
        - P(1 or 2 or 3) = 3 / 6 = 1/2
        - P(2 and 3) = 0 because 2 & 3 not possible on one roll


1. Rules for **non** mutually exclusive sets
    - P(A or B) = P(A) + P(B) - P(A and B) <--- so not double counting
1. Rules for mutually exclusive sets
    - P(A or B) = P(A) + P(B)
1. Compound probability of independent events
    - 2 flips of coin can give (H,H), (H,T), (T,H), (T,T)
    - P(HH) = 1 / 4
    - because independent event
    - if indenpendent event: P(H1) . P(H2) = 1/2 . 1/2 = 1/4
    - NOTE: Only can be done if independent events
1. P(at least 1 H in x flips) -
    - same as not getting all tails
    - 1 - P(all tails)
    = 1 - 1/2 . 1/2 . 1/2 = 7/8
1. Rolling doubles using 2 dice
    - 1,1 2,2 ... (6 possibilities)
    - 6/36 = 1/6

1. Basketball free throw (P n free throw in a row) = FT%
    = FT% ^ n

1. Prob of not equally likely event
    - eg unfair coin - eg. P(H) = 60%
    - we have to think in large sample size, 60% is head, 40% is tail
    - P(H,H) = P(H1) . P(H2) = .6 * .6 = .36 (just like basketball example above)
    - P(T, H, T) = P(T1) . P(H2) . P(T3) = .4 * .6 * .4
    
### Dependent Probablity
We pick 2 marble from same bag (without putting it back)
eg 3G 2R in bag
$$
P(G1 \cap G2) =  P(G1) . P(G1 | G2) \\\
= {3 \over 5} . {2 \over 4}
$$




# Trigonometry
## Basics
- soh cah toa
    - sin = opposite over hypoteneous 
    - cos = ajacent over hypoteneous 
    - tan = opposite over ajacent
- We also know that tan x = sin x / cos x
##  law of sin/cosine
![Law of Sin](./law_of_sin.png)
![Law of Cosine](./law_of_cosine.jpg)


## Radians
$$
2 \pi r = 360 ^{\circ} \\\
\text{therefore to convert degree to rad} \\\
 { 1 ^{\circ} }= {\pi r \over 180 ^{\circ} }
\text{therefore to convert rad to degree} \\\
 1 rad = {180 ^{\circ} \over {\pi} }
$$

---
# Series
## Arithmetic Series : Formula to compute it
Arithmetic series is as follows:
$$
\sum\limits_{k=1}^n a + (n-1)r
$$

We can find the sum using the formula below:
$$
    S_n = { (a_1 + a_n) \over 2 } . n
$$

## Solving Geometric series
Given
$$
ar_​0 + ar_1 + ... + ar_{n−1}
​$$

We can solve the equation with the formula:
$$
Sn = {a_1 . {(1-r^n) \over (1 - r)} }
$$


## Interest computation is a geometric series
Say we have 5% every month. Initial investment is 200. Every month she put in
200. Say we label amount for month as
$$
a_0 \text{ is initial month} \\\
a_1 \text{ is initial month 1} \\\
...\\\
a_1 \text{ is initial month 1}
$$
Then we can say that
$$
a_0 = 200\\\
a_1 = 200 * 1.005 \\\
a_2 = (200 * 1.005) * 1.005 = 200 (1.005)^2\\\
a_3 = ((200 * 1.005) * 1.005) = 200 (1.005)^3
$$
is of form
$$
S = 200  + 200*1.05
$$

Given formula
$$
Sn = {a_1 . {(1-r^n) \over (1 - r)} }
$$

For fifth month, we get
$$
S_5 = 200 . {(1-1.005^5) \over (1-1.005)} \\\
    = 200 . {(1 -  1.0252513) \over (-0.005) } \\\
    = 200 . (5.0502600) \\\
    = 1010.0520
$$

Now, we have a geometric series. To find how much

## Geometric Series, Convergence, Divergence
If a infinite series becomes a number, it convergence.
If an infinite series becomes infinity, it diverges.

To see if a geometric series
$$
lim_{n\to\infty} \sum\limits_{k=1}^n ar^k
$$

$$
\text{converges if 0< |r| <1 }
$$

It will converge to
$$
    lim_{n\to\infty} \sum\limits_{k=1}^n ar^k = {a \over (1-r)}
$$

---

# Partial Fraction
## Partial Fraction Expansion
To do partial fraction expansion, numerator need to have lower degree
compared with denominator.  By this we mean to change an equation to form
$$
{ A \over {x+a} } +  { B \over {x+b} }
$$


For example, to find partial fraction
$$
{x^2 - 2x - 37} \over {x^2 - 3x - 40}
$$

We can't find partial fraction because numerator and denominator
have same degree. So we need to factor out the numerator by doing divison

```
                           1
              _________________
x^2 - 3x - 40 | x^2 - 2x - 37
                x^2 - 3x - 40
                _____________
                       x  + 3
                  
```

Now by factoring, we have
$$
1 + { {x + 3} \over {x^2 - 3x - 40} }
$$

Solving further we get ----------- **(1)**
$$
1 + { {x + 3} \over {(x - 8)(x + 5)} }
$$

Say we get
$$
1 +   { A \over (x - 8)}  + { B \over  (x + 5)  }  
$$


Manipulating this we will get -------------- **(2)**
$$
1 +  { {A . (x+5) + B . (x-8) } \over { (x-8) (x+5) } }
$$


From **(1)** and from **(2)** we know
$$
A (x+5) + B (x-8) = x + 3
$$

Solving for A, we set x = 8
$$
A (8 + 5) + B(8-8) = 8 + 3 \\\
13A = 11\\\
A= {11 \over 13}
$$

Solving for B, we set x = -5
$$
A ( -5 + 5) + B (-5 -8) = -5 + 3\\\
0 -13B = -2\\\
B = {2 \over 13}
$$


Now we have the partial fraction expansion of
$$
{{x^2 - 2x - 37} \over {x^2 - 3x - 40} } = 1 +   { A \over (x - 8)}  + { B \over  (x + 5)  } \\\
\text{which is equal} \\\
1 +   { {11 \over 13} \over (x - 8)}  + { {2 \over 13} \over  (x + 5)  } \\\
\text{which simplifies to} \\\
1 +   { {11} \over 13(x - 8)}  + { {2} \over  13(x + 5)  } \\\
$$

---

# Conic Sections
## Features of a circle from its expanded equation
Given equation
$$
x^2 + y^2 + 4x - 4y -17 = 0
$$

We first convert it to form
$$
    (x-a)^2 + (y-b)^2 = r^2
$$

We know 
$$ 
center = (a,b)
$$

And
$$
radius = r
$$

