//Version 1 of the library will use numeric.js instead of local copy. Both should be compatible
// as v1 acts as a bridge to numeric.js for the meantime.
// V0 will diverge to parallel and multiserver next time.

var _ = require('lodash');
var numeric = require('numeric');

var MatrixV1 = module.exports = class {
    constructor(matrix=[[]]) {
        this.version = 1;
        this._array = matrix;

        if(Array.isArray(matrix)) {
            this._array = matrix;
        } else if (typeof matrix == 'string') {
            this._array = this._parseMatrixString(matrix);
        } else if (matrix instanceof MatrixV1) {
            return matrix;
        } else if (typeof matrix == 'number') {
            this._array = [[matrix]];
        } else throw "Unrecognized matrix form:" + matrix;        
    }

    array(A=null) {
        if(A===null) return this._array;
        else this._array = A;
    }

    ij(i=null,j=null, value) {
        if(i === null) return this.array();
        else if (j=== null) return this.array()[i];
        else if (typeof value!= 'undefined') {
            this.array()[i][j] = value;
            return this.array()[i][j];
        }
        else return this.array()[i][j];
    }

    size() {
        return numeric.dim(this._array);
    }

    rowCount(){
        return numeric.dim(this._array)[0];
    }

    colCount(){
        return numeric.dim(this._array)[1];
    }

    AxB(b){
        var _b = this.matrixify(b);
        var _result = numeric.dot(this._array, _b._array);
        return new MatrixV1(_result);
    }

    matrixify(M) {
       if(M instanceof MatrixV1) return M;
	   else return new MatrixV1(M);
    }


    transpose() {
        var _A = this._array;

        return this.matrixify(numeric.transpose(_A));
    }

    I(n){
        return this.matrixify(numeric.identity(n));
    }

    // Default format is space and comma (Matlab style)
    // It gets converted to comma separated.
    // The 2 string format does not conflict, so we convert
    // to comma separated
    //TODO: Make this function more efficient
    _parseMatrixString(matrixString) {

        const processedRows = matrixString.trim()
                                   .replace(/ *; */g,";")
                                   .replace(/^ +/g,"")
                                   .replace(/ +$/g,"")
                                   .replace(/ *; *$/g,";")
                                   .replace(/\s+/g,",")
                                   .replace(/;/g,"\n")
                                   .replace(/,$/,"")
                                   .replace(/^,/,"")
                                   .replace(/\n+$/,"")
                                   .split("\n")
                                   .map((line)=>(line.replace(/^ *,/,"")))
                                   .join("\n")
                                   ;
        
        var ret = numeric.parseCSV(processedRows);
        return ret;

    }

    add(B) {
        const _B = this.matrixify(B)._array;
        var result = this.matrixify(numeric.add(this._array, _B));
        return result;
    }


    minus(B) {
        const _B = this.matrixify(B)._array;
        var result = this.matrixify(numeric.sub(this._array, _B));
        return result;
    }

    inv() {
        var result = this.matrixify(numeric.inv(this._array));
        return result.isFinite() ? result : null;
    }

    _gaussJordanElimination () {
        var result = this.matrixify(numeric.inv(this._array));
        return result;
    }

    isFinite(){
        const a = this._array;
        for(var i = 0; i<a.length; i++ ) {
            for(var j = 0; j<a[i].length; j++ ) {
                if(!Number.isFinite(a[i][j])) return false;
            }
        }

        return true;
    }

    roundToInt() {
        var result = this.matrixify(numeric.round(this._array));
        return result;
    }

    equals(b) {
        const _B = this.matrixify(b);
        let result = numeric.eq(this._array, _B._array)
        return numeric.all(result);
    }
    
    zero(m=1, n=1) {
        return this.matrixify(numeric.rep([m,n],0));
    }

    
    one(m=1, n=1) {
        return this.matrixify(numeric.rep([m,n],1));
    }

    elementMul(y) {
        return this.matrixify(numeric.mul(this._array, y));
    }

    sumAll() {
        return numeric.sum(this._array);
    }

    sumSquare(){
        var sq = this.elementSq()._array;
        return numeric.sum(sq);
    }

    elementLog() {
        return this.matrixify(numeric.log(this._array));
    }

    clone() {
        return this.matrixify(numeric.clone(this._array));
    }

    elementPlus(y) {
        return this.matrixify(numeric.add(this._array,y));
    }


    elementAbs() {
        return this.matrixify(numeric.abs(this._array));
    }


    /////////////////////////////////////////////////////////////////////////////////////
    // Non numeric.js functions
    elementSigmoid() {
        return this.mapAll((x)=>( 1/(1 + Math.pow(Math.E, -x))));
    }
    elementSq() {
        return this.mapAll((x)=>(x*x));
    }

    mapAll(cb) {
        if(typeof cb=='undefined') return;

        var A = numeric.clone(this._array);
        var dim = this.size();

        for(var r = 0, rlen = dim[0]; r<rlen; r++) {
            for(var c = 0, clen = dim[1]; c<clen; c++) {
                A[r][c] = cb(A[r][c]);
            }
        }

        return this.matrixify(A);
    }    
    appendLeft(param) {
	   return this._appendCommon(param, true);
    }
    appendRight(param) {
        return this._appendCommon(param, false);
    }

    _appendCommon(param, isAppendLeft) {

        var M = this.matrixify(param);
        var msize = M.size();
        var rawM = M.array();

        if(M.size()[0]!==this.size()[0]) throw "Append left/right can only happen if both arrays have same number of rows";
        else {
            var A = _.cloneDeep(this._array);
            for(var m = 0, mlen = msize[0]; m<mlen; m++) {
                if(isAppendLeft) A[m] = rawM[m].concat(A[m]);
                else A[m] = A[m].concat(rawM[m]);
            }

        }

        return new MatrixV1(A);
    }

    colSum() {

    	return this.colReduce(function(r,currentRow){

            currentRow.forEach(function(i, index){
                r[index] += i;

            });

            return r;
        });
    }

    colMean(){
        var m = this.size()[0];
        var sumToMean = this.colSum();
        sumToMean.array()[0].forEach(function(i, index, arr){
            arr[index] /= m;
        });

        return sumToMean;
    }

    colReduce(reduceCb){
        var n = this.colCount();
        var R= this.zero(1,n);
        var rawR = R._array[0];

        this._array.reduce(reduceCb,rawR);
        return R;
    }

    colMaxMinSum(){
        return this.colReduce(function(result, current){

            for(var i = 0, ilen = current.length;i<ilen; i++){
                var item = current[i];

                if(result[i]===0) {
                    // Initialize the array
                    var currentResult = result[i] = {};
                    currentResult.sum = currentResult.max = currentResult.min = item;

                } else {
                    var currentResult = result[i];
                    currentResult.sum += item;
                    if(currentResult.max < item) currentResult.max = item;
                    if(currentResult.min > item) currentResult.min = item;
                }

            }

            return result;

        }).ij(0,0);

    }

    mapAllInPlace(cb) {
        if(typeof cb=='undefined') return;

        var A = this._array;
        var dim = this.size();

        for(var r = 0, rlen = dim[0]; r<rlen; r++) {
            for(var c = 0, clen = dim[1]; c<clen; c++) {
                A[r][c] = cb(A[r][c]);
            }
        }

        return this;
    }

    // Count the number of unique items in column
    colUnique(){
        return this.colReduce(function(result, current){

            for(var i = 0, ilen = current.length;i<ilen; i++){
                if(result[i]===0) {
                    result[i] = [];
                }

                var curResult = result[i];
                if(curResult.indexOf(current[i]) === -1) {
                    curResult[curResult.length] = current[i];
                }


            }
            return result;

        }).ij(0,0);

    }

    rowMaxColIndex() {

        var reducedRows = this.rowReduce(function(result, row, rowIndex){
            var maxIndex = 0;
            var maxVal = row[0];

            for(var i = 1; i<row.length; i++) {
                if(row[i]>maxVal) {
                    maxVal = row[i];
                    maxIndex = i;
                }
            }
            result.ij(rowIndex,0, maxIndex)
            return result;
        });

        return reducedRows;
    }

    rowReduce(reduceCb) {
        var m = this.rowCount();
        var R = this.zero(m,1);
        this._array.reduce(reduceCb, R);

        return R;

    }    

};

// Statics
MatrixV1.zero = MatrixV1.prototype.zero;
MatrixV1.one = MatrixV1.prototype.one;
MatrixV1.I = MatrixV1.prototype.I;
MatrixV1.matrixify = MatrixV1.prototype.matrixify;

// Alias
MatrixV1.prototype.mul = MatrixV1.prototype.AxB;
//MatrixV1.prototype._gaussJordanElimination = MatrixV1.prototype.inv; // For backward compatibility. To deprecate
