// eVo Simple Matrix Library
// ## Important References
// - http://introcs.cs.princeton.edu/java/95linear/
// - http://introcs.cs.princeton.edu/java/95linear/Matrix.java.html
// TODO Extract out matrix libraries

var _ = require('lodash');

var Matrix = function(matrix){
	this._data = {};
	if(Array.isArray(matrix)) {
		this._data.array = matrix;
	} else if (typeof matrix == 'string') {
		this._data.array = this._parseMatrixString(matrix);
	} else if (matrix instanceof Matrix) {
		return matrix;
	} else if (typeof matrix == 'number') {

		this._data.array = [[matrix]];
	} else if (typeof matrix == 'undefined') {
		this._data.array = [[]];
	} else throw "Unrecognized matrix form:" + matrix;
}

module.exports = Matrix;

Matrix.defaults = {

}

Matrix.prototype.data = function() {
	return this._data;
}

Matrix.prototype.array = function(A) {
	if(typeof a== 'undefined') return this.data().array;
	else this.data().array = A;
}


Matrix.prototype._parseMatrixString = function(matrixString) {
	var rawRows = matrixString.split(";");

	var R = new Array(rawRows.length);
	for(var r = 0, rlen = rawRows.length; r<rlen; r++) {
		//Remove leading and trailing spaces
		//Convert double space to single space
		var rawCols = rawRows[r].trim().replace(/\s+/g, " ").split(" ");
		
		R[r] = new Array(rawCols);
		for(var c = 0, clen = rawCols.length; c<clen; c++) {
			R[r][c] = parseFloat(rawCols[c]);
		}

	}

	return R;
}

Matrix.prototype.ij = function(i,j) {
	if(typeof i == 'undefined') return this.array();
	else if (typeof j=='undefined') return this.array()[i];
	else return this.array()[i][j];
}


Matrix.prototype.swapRow = function(i, j) {
	var A = this.array();

	var tmp = A[i];
	A[i] = A[j];
	A[j] = tmp;

}


// Multiply current matrix with matrix B returning a 
// new matrix - Matrix x Matrix
Matrix.prototype.AxB = function(B) {
	var A = this.array() || [[]]
	// Check dimensions for A
	var dimA = this.size();
	var dimB = B.size();

	if(dimA[1]!==dimB[0]) throw "Current dimension A:" + dimA.join("x") +
								" and B:" + dimB.join("x") + " cannot be multiplied";
	
	// Filling the result array
	var R = new Array(dimA[0]);
	for(var r = 0, rlen = dimA[0]; r<rlen; r++) {
		R[r] = new Array(dimB[1]);
		for(var c = 0, clen = dimB[1]; c<clen; c++) {
			var sum = 0;
			for(var acol = 0, alen = dimA[1]; acol<alen; acol++) {
				sum += A[r][acol] * B.ij(acol,c);
			}
			R[r][c]=sum;

		}

	}

	return new Matrix(R); 
								
}

//
// Multiply matrix with a constant
//
//
Matrix.prototype.cxA = function(k) {
	var A = this.array() || [];
	return A.map(function(row){
		return row.map(function(col){
			return col * k;
		});
	})
}

Matrix.prototype.size = function(){
	var A = this.array() || [[]];
	return [
		A.length, A[0].length
	];
}
Matrix.prototype.rowCount = function(){
	var A = this.array() || [[]];
	return A.length;
}
Matrix.prototype.colCount = function(){
	var A = this.array() || [[]];
	return A[0].length;
}


Matrix.prototype.transpose = function(){
	var A = this.array() || [[]];
	var dimA = this.size();

	var R = new Array(dimA[1]);
	for(var r = 0, rlen = dimA[1]; r<rlen; r++) {
		R[r] = new Array(dimA[0]);
		for(var c = 0, clen = dimA[c]; c<clen; c++) {
			R[r][c] = A[c][r];
		}

	}

	return new Matrix(R);
}

Matrix.prototype._op = function(B, isMinus) {
	var rawA = this.array();
	var rawB = B.array();

	var dimA = this.size();
	var dimB = B.size();

	//TODO: Make function for validation
	if(dimA[0]!==dimB[0] || dimA[1]!==dimB[1]) {
		throw "Dimension of A(" + dimA.join("x") + " ) and B(" + dimB.join("x") + ") does not match"
	}

	var R = new Array(dimA[0]);
	for(var r = 0, rlen = dimA[0]; r<rlen; r++){
		R[r] = new Array(dimA[1]);
		for(var c = 0, clen = dimA[1]; c<clen; c++) {
			if(isMinus) {
				R[r][c] = rawA[r][c] - rawB[r][c];
			} else {
				R[r][c] = rawA[r][c] + rawB[r][c];
			}
		}
	}

	return new Matrix(R);
}

Matrix.prototype.minus = function(B) {
	return this._op(B, true)

}
Matrix.prototype.add = function(B) {
	return this._op(B, false)
}
Matrix.prototype.mul = function(B) {
	return this.AxB(B);
}
Matrix.prototype.inv = function(){
	var dim = this.size();
	if(dim[0]!==dim[1]) throw "Inverse can only be done to square matrix. Current dimension is:" + dim.join("x");

	return this.gaussJordanElimination(Matrix.I(dim[0]), true);
}
Matrix.prototype.I = Matrix.I = function(size) {
	var R = new Array(size);
	for(var r = 0; r<size; r++) {
		R[r]=new Array(size);
		for(var c = 0; c<size; c++) {
			R[r][c] = r===c? 1:0;
		}
	}

	return new Matrix(R);
}



Matrix.prototype._cloneArray = function(){
	var A = this.array();
	var dim = this.size();

	var R = new Array(dim[0]);
	for(var r = 0, rlen = dim[0]; r<rlen; r++) {
		R[r] = new Array(dim[1]);
		for(var c = 0, clen = dim[1]; c<clen; c++) {
			R[r][c] = A[r][c];
		}

	}

	return R;
}

Matrix.prototype.mapAll = function(cb) {
	if(typeof cb=='undefined') return;

	var A = this.array();
	var dim = this.size();

	for(var r = 0, rlen = dim[0]; r<rlen; r++) {
		for(var c = 0, clen = dim[1]; c<clen; c++) {
			A[r][c] = cb(A[r][c]);
		}
	}

	return this;
}


//gauss jordan elimination
Matrix.prototype.gaussJordanElimination = function(Aaug, returnOnSingular){
	
	var dimA = this.size();
	var A = this._cloneArray();
	var swapsMade = [];
	var rawAaug = null;
	if(typeof Aaug!='undefined') rawAaug = Aaug._cloneArray();
	if(typeof returnOnSingular=='undefined') returnOnSingular = false;

	if(dimA[0]!==dimA[1]) throw "Only square matrix is supported. Matrix dimension now is:" + dimA.join("x");

	for(var pivot = 0, pivotLen = dimA[1]; pivot<pivotLen; pivot++) {
		// Step 1. if current pivot is 0, find swapRow
		if(A[pivot][pivot]===0) {
			var success = findZeroRowAndSwap();
			if(!success) {
				if(returnOnSingular) return undefined;

				continue;
			}
		}

		//Step2. Divide pivot row by first column
		var divisor = A[pivot][pivot];
		if(divisor!==0) { // safety net as checking is already done after swap
			A[pivot] = A[pivot].map(function(e){ return e/divisor;});
			if(rawAaug!=null) rawAaug[pivot] = rawAaug[pivot].map(function(e){ return e/divisor;});

			// Step3. Eliminate all other rows by pivot. NOTE: pivot val is
			//        now 1.
			var pivotRow = A[pivot];
			for(var r=0, rlen = dimA[0]; r<rlen; r++) {
				if(r!==pivot) {

					var rowDivisor = A[r][pivot];
					A[r] = elimination(A[r], pivotRow, rowDivisor);
					if(rawAaug!=null) rawAaug[r] = elimination(rawAaug[r], rawAaug[pivot], rowDivisor);
				}
			}

		}


	}

	return new Matrix(rawAaug);

	// Support functions

	function elimination(row, pivotRow, divisor) {
		return row.map(function(e, index){
			var pivotValue = pivotRow[index];
			return e - pivotValue * divisor;
		});
	}

	function findZeroRowAndSwap(){
		for(var ar = pivot, arlen = dimA[0]; ar<arlen; ar++) {
			if(A[ar][pivot]!==0) {
				lowLevelSwapRow(A, pivot, ar);
				if(rawAaug!=null) lowLevelSwapRow(rawAaug, pivot, ar);
				return true;
			}
		}

		return false;
	}

	function lowLevelSwapRow(M, rowi,rowj) {
		var tmp = M[rowi];
		M[rowi] = M[rowj];
		M[rowj] = tmp;
	}

}


Matrix.prototype.roundToInt = function() {
	return this.mapAll(function(i){
		return Math.round(i);
	});
}

Matrix.prototype.equals = function(B) {
	var dimA = this.size();
	var dimB = B.size();
	var rawA = this.array();
	var rawB = B.array();

	if(dimA[0]!==dimB[0] || dimA[1]!==dimB[1]) return false;
	else {
		for(var r = 0, rlen = dimA[0]; r<rlen; r++) {
			for(var c = 0, clen = dimA[0]; c<clen; c++) {
				if(rawA[r][c]!==rawB[r][c]) return false;
			}
		}

		return true;
	}

}


// Unimplemented algorithms
Matrix.prototype.pinv = function(){
	throw "Not implemented yet.";
}
Matrix.prototype.SVD = function(){
	throw "Not implemented yet.";
}


Matrix.one = Matrix.prototype.one = function(r,c) {
	if(typeof r =='undefined') r = 1;
	if(typeof c =='undefined') c = 1;

	return Matrix.createFilled(1,r,c);
}

Matrix.zero = Matrix.prototype.zero = function(r,c) {
	if(typeof r =='undefined') r = 1;
	if(typeof c =='undefined') c = 1;

	return Matrix.createFilled(0,r,c);
}

Matrix.createFilled = Matrix.prototype.createFilled = function(v,r,c) {
	if(typeof r =='undefined') r = 1;
	if(typeof c =='undefined') c = 1;

	var Rraw = new Array(r);
	for(var i = 0; i<r; i++) {
		Rraw[i] = new Array(c);
		for(var j = 0; j<c; j++) {
			Rraw[i][j] = v;
		}

	}

	return new Matrix(Rraw);

}

Matrix.prototype.appendLeft = function(param) {

	var M = new Matrix(param);
	var msize = M.size();
	var rawM = M.array();

	if(M.size()[0]!==this.size()[0]) throw "Append left/right can only happen if both arrays have same number of rows";
	else {
		var A = _.cloneDeep(this.array());
		for(var m = 0, mlen = msize[0]; m<mlen; m++) {
			A[m] = rawM[m].concat(A[m]);
		}

	}

	return new Matrix(A);
}

Matrix.matrixify = function(M) {
	if(M instanceof Matrix) return M;
	else return new Matrix(M);
}

Matrix.prototype.colSum = function(){
	return this.colReduce(function(r,currentRow){

		currentRow.forEach(function(i, index){
			r[index] += i;

		});

		return r;

	});


}
Matrix.prototype.colMean = function(){
	var m = this.size()[0];
	var sumToMean = this.colSum();
	sumToMean.array()[0].forEach(function(i, index, arr){
		arr[index] /= m;
	});

	return sumToMean;
}

Matrix.prototype.colReduce = function(reduceCb){
	var n = this.size()[1];
	var R= Matrix.zero(1,n);
	var rawR = R.array()[0];

	this.array().reduce(reduceCb,rawR);
	return R;
}

Matrix.prototype.prettyPrint = function(){
	console.table(this.array());
}


Matrix.prototype.colMaxMinSum = function(){
	return this.colReduce(function(result, current){

		for(var i = 0, ilen = current.length;i<ilen; i++){
			var item = current[i];

			if(result[i]===0) {
				// Initialize the array
				var currentResult = result[i] = {};
				currentResult.sum = currentResult.max = currentResult.min = item;

			} else {
				var currentResult = result[i];
				currentResult.sum += item;
				if(currentResult.max < item) currentResult.max = item;
				if(currentResult.min > item) currentResult.min = item;
			}

		}

		return result;

	}).ij(0,0);
    
}


Matrix.prototype.elementMul = function(val) {
	return this.mapAll(function(i){
		return i*val;
	});
}

Matrix.prototype.elementMinus = function(val) {
	return this.mapAll(function(i){
		return i - val;
	});
}

Matrix.prototype.elementSq = function() {
	return this.mapAll(function(i){
		return i*i;
	});
}


