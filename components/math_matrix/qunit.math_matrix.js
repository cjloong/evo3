var Matrix = require("../math_matrix/math_matrix.js");
var numeric = require("numeric");
var _ = require("lodash");

function MatrixFactory(version = 0) {
	if(version === 1) return Matrix.___v1;
	if(version === 0) return Matrix.___v0;
	else return Matrix;
}



QUnit.module("Matrices", ()=>{

	QUnit.module("Unit Test:Matrices");
	QUnit.test("Matrix warm up", function(assert){
		function test(version=0) {
			var Matrix = (MatrixFactory(version));
			var M = new Matrix();
			assert.ok(true, "No problem when empty initialization.")
			assert.ok(true, `Finish testing version:${version}`);
		}

		test();
		test(0);
		test(1);
	});

	QUnit.test("Matrix 2x2", function(assert){
		function test(version=0) {
			var Matrix = (MatrixFactory(version));
			var A = new Matrix([[
				1,1
			],[
				2,3
			]]);

			assert.ok(A.ij(1,1)===3, "elements 1,1==3:" + A.ij(1,1));
			assert.ok(A.ij(0,1)===1, "elements 1,1==1:" + A.ij(0,1));
			assert.ok(A.ij(1,0)===2, "elements 1,1==2:" + A.ij(1,0));
			assert.ok(A.ij(0,0)===1, "elements 1,1==1:" + A.ij(0,0));

			assert.ok(true, `Finish testing version:${version}`);
		}
		test();
		test(0);
		test(1);

	});



	QUnit.test("Dimensions test", function(assert){
		function test(version=0) {
			var Matrix = (MatrixFactory(version));
			var A = new Matrix([[
				1,1
			],[
				2,3
			],[
				4,5
			]]);

			var dim = A.size();
			assert.ok(dim[0] === 3);
			assert.ok(dim[1] === 2);
			assert.ok(A.rowCount() === 3);
			assert.ok(A.colCount() === 2);

			assert.ok(true, `Finish testing version:${version}`);
		}
		test();
		test(0);
		test(1);

	});

	QUnit.test("Matrix A x B", function(assert){
		function test(version=0) {
			var Matrix = (MatrixFactory(version));
			var A = new Matrix([[
				1,1
			],[
				2,3
			]]);

			var B = new Matrix([[
				1,1
			],[
				2,3
			]]);


			var R = A.AxB(B);


			assert.ok(A.ij(1,1)===3, "elements 1,1==3:" + A.ij(1,1));
			assert.ok(A.ij(0,1)===1, "elements 1,1==1:" + A.ij(0,1));
			assert.ok(A.ij(1,0)===2, "elements 1,1==2:" + A.ij(1,0));
			assert.ok(A.ij(0,0)===1, "elements 1,1==1:" + A.ij(0,0));

			assert.ok(B.ij(1,1)===3, "elements 1,1==3:" + B.ij(1,1));
			assert.ok(B.ij(0,1)===1, "elements 1,1==1:" + B.ij(0,1));
			assert.ok(B.ij(1,0)===2, "elements 1,1==2:" + B.ij(1,0));
			assert.ok(B.ij(0,0)===1, "elements 1,1==1:" + B.ij(0,0));

			assert.ok(R.ij(0,0)===3);
			assert.ok(R.ij(0,1)===4);
			assert.ok(R.ij(1,0)===8);
			assert.ok(R.ij(1,1)===11);
		}

		test();
		test(0);
		test(1);

	});



	QUnit.test("Matrix A transpose", function(assert){
		function test(version=0) {
			var Matrix = (MatrixFactory(version));
			var A = new Matrix([[
				1,4
			],[
				2,5
			],[
				3,6
			]]);


			var R = A.transpose();

			assert.ok(A.ij(0,0)===1);
			assert.ok(A.ij(0,1)===4);
			assert.ok(A.ij(1,0)===2);
			assert.ok(A.ij(1,1)===5);
			assert.ok(A.ij(2,0)===3);
			assert.ok(A.ij(2,1)===6);

			assert.ok(R.ij(0,0)===1);
			assert.ok(R.ij(0,1)===2);
			assert.ok(R.ij(0,2)===3);
			assert.ok(R.ij(1,0)===4);
			assert.ok(R.ij(1,1)===5);
			assert.ok(R.ij(1,2)===6);

		}

		test();
		test(0);
		test(1);
	});

	QUnit.test("Matrix I", function(assert){
		function test(version=0) {
			var Matrix = (MatrixFactory(version));
			var AI = Matrix.I(3);
			var BI = AI.I(4);

			checkMatrix(AI);
			checkMatrix(BI);

			function checkMatrix(M) {

				for(var r = 0, rlen = M.array().length; r<rlen; r++) {
					for(var c = 0, clen = M.array().length; c<clen; c++) {
						if(r===c) assert.ok(M.ij(r,c)===1);
						else assert.ok(M.ij(r,c)===0);
					}	
				}
			}

		}

		test();
		test(0);
		test(1);
	});

	QUnit.test("Matrix equation parsing", function(assert){
		function test(version=0) {
			var Matrix = (MatrixFactory(version));
			var A = new Matrix("1 5 1;2 5 5 ; 5 3 2");
			assert.ok(A.ij(0,0)===1);
			assert.ok(A.ij(0,1)===5);
			assert.ok(A.ij(0,2)===1);
			assert.ok(A.ij(1,0)===2);
			assert.ok(A.ij(1,1)===5);
			assert.ok(A.ij(1,2)===5);
			assert.ok(A.ij(2,0)===5);
			assert.ok(A.ij(2,1)===3);
			assert.ok(A.ij(2,2)===2);
		}

		test();
		test(0);
		test(1);

	});



	QUnit.test("Addition/Subtraction", function(assert){
		function test(version=0) {
			var Matrix = (MatrixFactory(version));

			var A = new Matrix("1  2  3  4;  5  6  7  8");
			var B = new Matrix("9 10 11 12; 13 14 15 16");

			var R = A.add(B);
			assert.ok(R.ij(0,0)===10);
			assert.ok(R.ij(0,1)===12);
			assert.ok(R.ij(0,2)===14);
			assert.ok(R.ij(0,3)===16);
			assert.ok(R.ij(1,0)===18);
			assert.ok(R.ij(1,1)===20);
			assert.ok(R.ij(1,2)===22);
			assert.ok(R.ij(1,3)===24);

			var R = R.minus(A);
			assert.ok(R.ij(0,0)===9);
			assert.ok(R.ij(0,1)===10);
			assert.ok(R.ij(0,2)===11);
			assert.ok(R.ij(0,3)===12);
			assert.ok(R.ij(1,0)===13);
			assert.ok(R.ij(1,1)===14);
			assert.ok(R.ij(1,2)===15);
			assert.ok(R.ij(1,3)===16);

		}

		test();
		test(0);
		test(1);

	});


	QUnit.test("Gaussian Elimination with I - ie inverse function", function(assert){
		function test(version=0) {
			var Matrix = (MatrixFactory(version));

			var A = new Matrix("1 2 1;3 4 5;9 1 1");
			var Aaug = Matrix.I(3);

			assert.ok(A.ij(0,0)===1);
			assert.ok(A.ij(0,1)===2);
			assert.ok(A.ij(0,2)===1);
			assert.ok(A.ij(1,0)===3);
			assert.ok(A.ij(1,1)===4);
			assert.ok(A.ij(1,2)===5);
			assert.ok(A.ij(2,0)===9);
			assert.ok(A.ij(2,1)===1);
			assert.ok(A.ij(2,2)===1);

			// Call elimination
			var precision = 4;
			var Ainv = A._gaussJordanElimination(Aaug)

			assert.ok(A.ij(0,0)===1);
			assert.ok(A.ij(0,1)===2);
			assert.ok(A.ij(0,2)===1);
			assert.ok(A.ij(1,0)===3);
			assert.ok(A.ij(1,1)===4);
			assert.ok(A.ij(1,2)===5);
			assert.ok(A.ij(2,0)===9);
			assert.ok(A.ij(2,1)===1);
			assert.ok(A.ij(2,2)===1);

			var AAinv = A.mul(Ainv);
			var AinvA = Ainv.mul(A);
			assert.ok(AAinv.roundToInt().equals(Matrix.I(3)));
			assert.ok(AinvA.roundToInt().equals(Matrix.I(3)));

		}

		test();
		test(0);
		test(1);
		
	});

	QUnit.test("Matrix inv - non singular", function(assert){
		function test(version=0) {
			var Matrix = (MatrixFactory(version));

			var A = new Matrix("1 2 3;4 3 2; 1 1 0");
			var Ainv = A.inv();
			assert.ok(A.mul(Ainv).roundToInt().equals(Matrix.I(3)));
			assert.ok(Ainv.mul(A).roundToInt().equals(Matrix.I(3)));

		}

		test();
		test(0);
		test(1);
	});

	QUnit.test("Matrix inv - singular - Gauss jordan", function(assert){
		function test(version=0) {
			var Matrix = (MatrixFactory(version));

			var A = new Matrix("2 6;1 3");
			var I = Matrix.I(2);

			var Ainv = A._gaussJordanElimination(I);
			assert.ok(Ainv !== null, "Gauss Jordan still process singular matrix.");

			var Ainv = A.inv();
			assert.ok(Ainv == null, "Inverse function will return undefined or null.");
		}

		test();
		test(0);
		test(1);

	});


	QUnit.test("Matrix append left", function(assert){
		function test(version=0) {
			var Matrix = (MatrixFactory(version));

			var A = new Matrix("2 6;1 3");
			A = A.appendLeft("1 1;1 1");
			assert.ok(A.array()[0].join(":") == "1:1:2:6");
			assert.ok(A.array()[1].join(":") == "1:1:1:3");
		}

		test();
		test(0);
		test(1);
	});

	QUnit.test("Compute column sum and column mean of columns", function(assert){
		function test(version=0) {
			var Matrix = (MatrixFactory(version));

			var A = new Matrix("1 1 1;2 2 2;3 3 3;4 4 4;5 5 5");
			var sumA = A.colSum();
			assert.ok(sumA.ij(0,0)===15, "1");
			assert.ok(sumA.ij(0,1)===15, "2");
			assert.ok(sumA.ij(0,2)===15, "3");


			var meanA = A.colMean();;
			assert.ok(meanA.ij(0,0)===3, "4");
			assert.ok(meanA.ij(0,1)===3, "5");
			assert.ok(meanA.ij(0,2)===3, "6");
		}

		test();
		test(0);
		test(1);
	});

	QUnit.test("Matrix.one", function(assert){

		function test(version=0) {
			var Matrix = (MatrixFactory(version));
			var O = Matrix.one(3,2);
			assert.ok(O.array()[0].join(":") == "1:1");
			assert.ok(O.array()[1].join(":") == "1:1");
			assert.ok(O.array()[2].join(":") == "1:1");

		}

		test();
		test(0);
		test(1);
	});

	QUnit.test("clone", (assert)=>{
		function test(version=0) {
			var Matrix = (MatrixFactory(version));
			var M = Matrix.matrixify("1 1 1;2 2 2;3 3 3");
			var M_ = M.clone()
			M.mapAllInPlace((i)=>(i+1));
			assert.ok(M_.array()[0].join(":") == "1:1:1");
			assert.ok(M_.array()[1].join(":") == "2:2:2");
			assert.ok(M_.array()[2].join(":") == "3:3:3");
			assert.ok(M.array()[0].join(":") == "2:2:2");
			assert.ok(M.array()[1].join(":") == "3:3:3");
			assert.ok(M.array()[2].join(":") == "4:4:4");

		}

		test();
		test(0);
		test(1);
		
	});

	QUnit.test("mapAll", (assert)=>{
		function test(version=0) {
			var Matrix = (MatrixFactory(version));
			var M = Matrix.matrixify("1 1 1;2 2 2;3 3 3");
			var M_ = M.mapAll((i)=>(i+1));
			assert.ok(M.array()[0].join(":") == "1:1:1");
			assert.ok(M.array()[1].join(":") == "2:2:2");
			assert.ok(M.array()[2].join(":") == "3:3:3");
			assert.ok(M_.array()[0].join(":") == "2:2:2");
			assert.ok(M_.array()[1].join(":") == "3:3:3");
			assert.ok(M_.array()[2].join(":") == "4:4:4");

			var M_ = M.mapAllInPlace((i)=>(i+1));
			assert.ok(M.array()[0].join(":") == "2:2:2");
			assert.ok(M.array()[1].join(":") == "3:3:3");
			assert.ok(M.array()[2].join(":") == "4:4:4");
			assert.ok(M_.array()[0].join(":") == "2:2:2");
			assert.ok(M_.array()[1].join(":") == "3:3:3");
			assert.ok(M_.array()[2].join(":") == "4:4:4");
		}

		test();
		test(0);
		test(1);
		
	});
	QUnit.test("elementMul", (assert)=>{
		function test(version=0) {
			var Matrix = (MatrixFactory(version));
			var M = Matrix.matrixify("1 1 1;2 2 2;3 3 3");
			var M_ = M.elementMul(2);
			assert.ok(M_.array()[0].join(":") == "2:2:2");
			assert.ok(M_.array()[1].join(":") == "4:4:4");
			assert.ok(M_.array()[2].join(":") == "6:6:6");

		}

		test();
		test(0);
		test(1);
		
	});
	QUnit.test("elementPlus", (assert)=>{
		function test(version=0) {
			var Matrix = (MatrixFactory(version));
			var M = Matrix.matrixify("1 1 1;2 2 2;3 3 3");
			var M_ = M.elementPlus(1);
			assert.ok(M_.array()[0].join(":") == "2:2:2");
			assert.ok(M_.array()[1].join(":") == "3:3:3");
			assert.ok(M_.array()[2].join(":") == "4:4:4");

		}

		test();
		test(0);
		test(1);
		
	});
	QUnit.test("elementAbs", (assert)=>{
		function test(version=0) {
			var Matrix = (MatrixFactory(version));
			var M = Matrix.matrixify("1 -1 1;2 2 -2;-3 3 3");
			var M_ = M.elementAbs();
			assert.ok(M_.array()[0].join(":") == "1:1:1");
			assert.ok(M_.array()[1].join(":") == "2:2:2");
			assert.ok(M_.array()[2].join(":") == "3:3:3");

		}

		test();
		test(0);
		test(1);

	});
	QUnit.test("elementSq", (assert)=>{
		function test(version=0) {
			var Matrix = (MatrixFactory(version));
			var M = Matrix.matrixify("1 1 1;2 2 2;3 3 3");
			var M_ = M.elementSq();
			assert.ok(M_.array()[0].join(":") == "1:1:1");
			assert.ok(M_.array()[1].join(":") == "4:4:4");
			assert.ok(M_.array()[2].join(":") == "9:9:9");

		}

		test();
		test(0);
		test(1);
		
	});
	QUnit.test("sumAll", (assert)=>{
		function test(version=0) {
			var Matrix = (MatrixFactory(version));
			var M = Matrix.matrixify("1 1 1;2 2 2;3 3 3");
			var sum = M.sumAll();
			assert.ok(sum===(3+6+9));

		}

		test();
		test(0);
		test(1);
		
	});

	QUnit.test("sumSq", (assert)=>{
		function test(version=0) {
			var Matrix = (MatrixFactory(version));
			var M = Matrix.matrixify("1 1 1;2 2 2;3 3 3");
			var sumSq = M.sumSquare();
			assert.ok(sumSq===(3+4*3+9*3));

		}

		test();
		test(0);
		test(1);
		
	});
	QUnit.test("elementSigmoid", (assert)=>{
		function test(version=0) {
			var Matrix = (MatrixFactory(version));
			var M = Matrix.matrixify("1 1 1;2 2 2;3 3 3");
			var M_ = M.elementSigmoid();
			assert.ok(_.round(M_.ij(0,0),5)===0.73106);
			assert.ok(_.round(M_.ij(0,1),5)===0.73106);
			assert.ok(_.round(M_.ij(0,2),5)===0.73106);
			assert.ok(_.round(M_.ij(1,0),5)===0.8808);
			assert.ok(_.round(M_.ij(1,1),5)===0.8808);
			assert.ok(_.round(M_.ij(1,2),5)===0.8808);
			assert.ok(_.round(M_.ij(2,0),5)===0.95257);
			assert.ok(_.round(M_.ij(2,1),5)===0.95257);
			assert.ok(_.round(M_.ij(2,2),5)===0.95257);

		}

		test();
		test(0);
		test(1);
		
	});
	QUnit.test("elementLog", (assert)=>{
		function test(version=0) {
			var Matrix = (MatrixFactory(version));
			var M = Matrix.matrixify("1 1 1;2 2 2;3 3 3");
			var M_ = M.elementLog();
			assert.ok(_.round(M_.ij(0,0),5)===0);
			assert.ok(_.round(M_.ij(0,1),5)===0);
			assert.ok(_.round(M_.ij(0,2),5)===0);
			assert.ok(_.round(M_.ij(1,0),5)===0.69315);
			assert.ok(_.round(M_.ij(1,1),5)===0.69315);
			assert.ok(_.round(M_.ij(1,2),5)===0.69315);
			assert.ok(_.round(M_.ij(2,0),5)===1.09861);
			assert.ok(_.round(M_.ij(2,1),5)===1.09861);
			assert.ok(_.round(M_.ij(2,2),5)===1.09861);

		}

		test();
		test(0);
		test(1);
		
	});

	QUnit.test("Matrix string parser test", (assert)=>{
		function test(version=0) {
			var Matrix = (MatrixFactory(version));
			var theta1 = new Matrix(`
			   0.00000   0.90930  -0.75680;
			   0.47943   0.59847  -0.97753;
			   0.84147   0.14112  -0.95892;
			   0.99749  -0.35078  -0.70554
			`);
			assert.ok(theta1.rowCount()===4,"theta1 rowcount check");
			assert.ok(theta1.colCount()===3,"theta1 colcount check");
		
		}

		test();
		test(0);
		test(1);
	});

	QUnit.test("Matrixify", (assert)=>{
		function test(version=0) {
			var Matrix = (MatrixFactory(version));
			var X = new Matrix(`
			   0.84147   0.41212;
			   0.90930  -0.54402;
			`);
			
			assert.ok(X.array()[0].join(":") == "0.84147:0.41212");
			assert.ok(X.array()[1].join(":") == "0.9093:-0.54402");

		}

		test();
		test(0);
		test(1);

	});

	QUnit.skip("Matrix pinv - singular and non singular", function(assert){
		assert.ok(false, "Not implemented yet.")
	});

	QUnit.skip("mapall", (assert)=>{});
	QUnit.skip("matrixify function - static and class", (assert)=>{});
	QUnit.skip("colUnique", (assert)=>{});
    QUnit.skip("rowMaxColIndex()", (assert)=>{});
    QUnit.skip("rowReduce(reduceCb)", (assert)=>{});
    QUnit.skip("ij setter", (assert)=>{});



});
