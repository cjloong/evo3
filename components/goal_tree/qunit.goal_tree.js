class GoalTree {
	constructor( 
		graph = {
			atoms: []
			, nodes: []
			, relationships : []
		}
	){
		this.graph = graph;
	}
	
	ruleAtom(fact, predicate) {
		this.graph.atoms.push({fact, predicate});

		return this;
	}

	rule(fact, ...andSet){
		console.debug(`fact=${fact}`);
		console.debug(`andset=${andSet}`);

		// Constructing node
		let pred = this.makepredicate(andSet);
		this.graph.nodes.push({fact,predicate: pred, andSet});

		return this;

	};
	
	isFunction(obj) {
		return !!(obj && obj.constructor && obj.call && obj.apply);
	}

	makepredicate(set=[]) {
		let rules = set.map(item=>{
			console.debug("Generating normal rule");
			return (fact)=>{
				console.debug(fact);
				return fact.indexOf(item)>=0;
			}
		});

		return function andRule(fact) {
			for(let i = 0; i<rules.length; i++) {
				if(rules[i](fact)===false) return false;
			}
			return true;
		}
	}

	makeSimplePredicate() {}

	objectToFacts(obj={}) {
		var facts = this.graph.atoms
		.filter(atom=>atom.predicate(obj))
		.map(atom=>(atom.fact));

		console.debug(`Extract facts from atom:[${JSON.stringify(facts)}]`);
		return facts;
	}

	evalObject(atom={}, method="bruteForce") {
		// Get fact array from atom evaluator first
		var facts = this.objectToFacts(atom);
		return this.evalFacts(facts, method);
	}

	evalFacts(
		facts = []
		, method= "bruteForce"
	){

		console.debug("Running:" + `_algorithm_${method} with facts:[${JSON.stringify(facts)}]` );


		return this[`_algorithm_${method}`](facts)
	};

	_algorithm_bruteForce(facts=[], nodes=this.graph.nodes) {

		console.debug(`_algorithm_bruteForce with facts:[${JSON.stringify(facts)}]` );

		let stackTrace = []
		stackTrace.push(facts.map(fact=>({fact,andSet:['made known to me.']})));

		while(true) {
			let conclusiveNodes = [];
			let inconclusiveNodes = [];
			let newFacts = [];
			nodes.forEach(node=>{
				console.debug("------------------------------------------------")
				console.debug("Evaluating:" + JSON.stringify(node));
				console.debug(node.predicate);
				console.debug("------------------------------------------------")
				if(node.predicate(facts)===true) {
					conclusiveNodes.push(node);
					newFacts.push(node);//.fact);
					console.debug("...can evalFacts that: " + JSON.stringify(node) + ` using facts: ${JSON.stringify(facts)}`);
				} else {
					inconclusiveNodes.push(node);
					console.debug("...cannot evalFacts that: " + JSON.stringify(node) + ` using facts: ${JSON.stringify(facts)}`);
				}
			})

			if(newFacts.length>0) {
				console.debug(`Found ${newFacts.length} facts.`);
				// New facts was found
				// Filter inconclusiveNodes for already evalFactsd (maybe from another route)
				inconclusiveNodes = inconclusiveNodes.filter(node=>(
					conclusiveNodes.map(n=>n.fact).indexOf(node.fact) <0
				));

				stackTrace.push(newFacts);
				nodes = inconclusiveNodes.map(n=>n);
				facts = facts.concat(newFacts.map(f=>f.fact));

			} else {
				// Ran all possible rules
				console.debug("No more facts found. Returning conclusion");
				console.debug("Running brute force method to derive conclusions.");
				console.debug("New facts found:" + newFacts.map(f=>f.fact).join(","));
				console.debug("Rules remaining:" + inconclusiveNodes.map(n=>n.fact).join(","));
				console.debug("Rules ran:" + conclusiveNodes.map(n=>n.fact).join(","));
				console.debug("Stack trace:" + JSON.stringify(stackTrace));
				return stackTrace;
			}

		}




	}

}

QUnit.module("GoalTree", (assert)=>{
	// Test objects
	QUnit.module("_Development",(assert)=>{

	
    });


	QUnit.module("Unit Test", (assert)=>{

		QUnit.test("Predicate function deduction",(assert)=>{
			let goalTree = new GoalTree();

			let model = {
				color: {
					r: 150, g:150, b:120
				}
				, size: 100
				, screenSize: 128
			}

			// Make the atoms
			goalTree.ruleAtom("is dark color", f=> {
				console.debug(f);
				return (f.color.r + f.color.g + f.color.b) / 3  > 127;
			});

			goalTree.ruleAtom("is large", f=> (f.size > (f.screenSize/2) ));
			goalTree.rule("is too dark", "is dark color", "is large");


			console.log(goalTree.graph);
			let answers = goalTree.evalObject(model);
			{
				let answer = answers.pop();
				assert.ok("Only 1 top answer", answer.length===1);
				assert.ok("Should evaluate to is too dark", answer[0].fact=="is too dark");
			}

			{
				let answer = answers.pop();
				assert.ok("Cause by 2 facts", answer.length===2);
				assert.ok("is dark color", answer[0]=="is too dark");
				assert.ok("is large", answer[0]=="is large");
			}
		});

		QUnit.test("Simple string only single deduction",(assert)=>{
			let fixture = new GoalTree();

			fixture.rule("is mamal","has hair");

			// This is OR	
			fixture.rule("is carnivor","has hair","Has claws","Has sharp teeth","Forward pointing eyes");
			fixture.rule("is carnivor","eats meat");
			
			// This is and rule
			fixture.rule("is Cheetah", "is mamal", "is carnivor","has spots","is very fast");

			console.log(fixture.graph);
			let answer = fixture.evalFacts(["eats meat","has hair", "has spots", "is very fast"]);
			console.log(answer);
			console.debug(JSON.stringify(answer));			
			const cheetah = answer.pop();

			console.log(`It ${cheetah.map(f=>f.fact).join(",")} because ${cheetah.map(f=>f.andSet.join(","))}`);
			answer.pop().map(f=>{
				console.debug(`It ${f.fact} because it  ${f.andSet.join(",")}`);
			});
			answer.pop().map(f=>{
				console.debug(`It ${f.fact} because it ${f.andSet.join(",")}`);
			});
// 			console.log("Because it " + answer.pop().map(f=>f.fact).join(","));
// 			console.log("Because it " + answer.pop().map(f=>f.fact).join(","));
			
			assert.ok("It is cheetah", cheetah.indexOf("is cheetah")>=0)
			//assert.ok(answer.indexOf("is Cheetah")>=0);

		});
	});

});
