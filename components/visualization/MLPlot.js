var dimple = require("dimple/dist/dimple.v2.2.0.js");
var Matrix = require("../math_matrix/math_matrix.js");
var _ = require("lodash");
var $ = require("jquery");

/**
 * 
 * ML Plot is a simple interface to draw graphs for 
 * machine learning visualization. It is specifically written
 * to facilitate graph rendering as tools to design machine
 * learning projects.
 * 
 */
module.exports = class MLPlot {

	
	constructor({container="body", width=590, height=400}) {
		this.data.container = container;
		this.data.width=width;
		this.data.height=height;
	}

	get data(){
		if(this._data==null) {
			this._data = {};
		}
		return this._data;
	}

	get chart() {
		var data = this.data;
		var svg = data.svg = data.svg || dimple.newSvg(data.container, data.width, data.height);

		if(data.chart==null) {
			data.chart = data.chart || new dimple.chart(svg);
			var xAxis = data.chart.addMeasureAxis("x", "x");
			var yAxis = data.chart.addMeasureAxis("y", "y");
			yAxis.ticks = 10;
			xAxis.ticks = 10;
			xAxis.showGridlines = true;
		}

		return data.chart;

	}

	plotPoints({group="grouping", x = [], y = [], xlabel="X Axis", ylabel = "Y axis"}){

		var chart = this.chart;
		var axis = this.axis;
		var plotdata = this._genXYData(x,y);

 		var series = chart.addSeries(["x", "y", group], dimple.plot.bubble);
// 		var series = chart.addSeries(["x","y", group], dimple.plot.bubble);
// 		var lseries = chart.addSeries(["line",group], dimple.plot.line);

// 		debugger;
		series.data = plotdata;
// 		lseries.data = plotdata;
		chart.draw();
		
	}



	plotLine({group=null, x = [], y = [], xlabel="X Axis", ylabel = "Y axis"}){

		var data = this.data;
		data.groups = data.groups || {};
		var chart = this.chart;
		var axis = this.axis;
		var plotdata = this._genXYData(x,y);

		// Storing the group series
		if(data.groups[group]==null) {
			var series = chart.addSeries("plotname", dimple.plot.line);
			series.interpolation = "cardinal";
			series.data = plotdata;
			data.groups[group] = series;
		} else {
			var series = data.groups[group];
			series.data = series.data.concat(plotdata);
		}

		chart.draw();
		
	}


	redraw() {
		this.chart().draw();
	}

	// Private functions

	// This function collapse to array into dimple friendly structure
	_genXYData(x=[], y=[], plotname="plot"){
		return x.map((e, index)=>{
			return {
				x: e
				, y: y[index]
				, plotname: "plotname-" + [e, y[index]].join("x")
			}
		});
	}

}