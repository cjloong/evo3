var MLPlot = require("./MLPlot.js");
var Matrix = require("../math_matrix/math_matrix.js");
var dimple = require("dimple/dist/dimple.v2.2.0.js");
var LinearRegression = require("../linear_regression/LinearRegression.js");
var GradientDescent = require("../trainers/GradientDescent.js");
var Plotly = require('plotly.js/lib/core');
require("babel-polyfill");
var $ = require("jquery");


// Unit test support functions
var emptyUi = ()=>{
	$("#qunit-fixture").empty();
}


function extractPlotStrings(groupName){
	var $plot = $(".dimple-" + groupName).toArray();
	return $plot.reduce((result, item, rows)=>{
		var data = item.id.split("-");
		return (result += ":" + [parseInt(data[1],10),parseInt(data[2],10)].join(","));
	}, "");

}




// Actual Tests
QUnit.module("Visualization", (hooks)=>{
	QUnit.module("Experiments:D3 primitives", (hooks) => {
		hooks.beforeEach(()=>{
		    $('#ui').remove();
		    $('#qunit-fixture').remove();
			$('body').append('<div id="qunit-fixture"><div id="ui"></div></div>');

		});
		hooks.afterEach(()=>{
			
		});


		QUnit.test("Testing domain, scales and rang:Linear Scalee", (assert)=>{
			//  Linear scale test - no domain, so x=y
			var scale = d3.scale.linear(); // By default it is 1 to 1 = ie. x = y
			assert.ok(scale(1)===1, "Result:" + scale(1) );
			assert.ok(scale(100)===100, "Result:" + scale(100) );
			assert.ok(scale(1000)===1000, "Result:" + scale(1000) );
			
			//  Linear scale test - no range, so map to 0-1
			var scale = d3.scale.linear().domain([0,1000]); // By default it is 1 to 1 = ie. x = y
			assert.ok(scale(1)===0.001, "Result:" + scale(1) );
			assert.ok(scale(100)===0.1, "Result:" + scale(100) );
			assert.ok(scale(1000)===1, "Result:" + scale(1000) );

			var scale = d3.scale.linear(); // By default it is 1 to 1 = ie. x = y
			scale.domain([0,100]).range([0,1000], "Result:" + scale(1) );
			assert.ok(scale(1)===10, "Result:" + scale(1) );
			assert.ok(scale(100)===1000, "Result:" + scale(1) );
			assert.ok(scale(1000)===10000, "Result:" + scale(1000) );

			// Ordinal scales



		});
	});

	QUnit.module("Unit Test:Visualization", (hooks) => {
		hooks.beforeEach(()=>{
		    $('#ui').remove();
		    $('#qunit-fixture').remove();
			$('body').append('<div id="qunit-fixture"><div id="ui"></div></div>');

		});
		hooks.afterEach(()=>{

		});


		QUnit.test("Warm up",(assert)=>{
			assert.ok(true, "TODO: Do more warmup tests")
		});



		QUnit.test("Scratch on dimple", (assert)=>{

			var svg = dimple.newSvg("#ui", 590, 400);
			var myChart = new dimple.chart(svg);
			var xAxis = myChart.addMeasureAxis("x", "x");
			var yAxis = myChart.addMeasureAxis("y", "y");
			xAxis.showGridlines = true;
			yAxis.ticks = 10;
			xAxis.ticks = 10;
			var s1 = myChart.addSeries(["x", "y", "plot1"], dimple.plot.bar);
			var s2 = myChart.addSeries(["x","y", "plot2"], dimple.plot.bubble);
			var s3 = myChart.addSeries(["x","y", "plot3"], dimple.plot.bubble);
			s1.data = [
				{x:1,y:10}
				, {x:2,y:10}
			];
			s2.data = [
				{x:5,y:10}
				, {x:5,y:5}
			];
			s3.data = [
				{x:1,y:1}
				, {x:2,y:5}
			];

			myChart.draw();

			var plotJoin = extractPlotStrings("plot1");
			assert.ok(plotJoin.indexOf("2,10")!==-1);
			assert.ok(plotJoin.indexOf("1,10")!==-1);

			var plotJoin = extractPlotStrings("plot2");
			assert.ok(plotJoin.indexOf("5,10")!==-1);
			assert.ok(plotJoin.indexOf("5,5")!==-1);

			var plotJoin = extractPlotStrings("plot3");
			assert.ok(plotJoin.indexOf("1,1")!==-1);
			assert.ok(plotJoin.indexOf("2,5")!==-1);

		});
		QUnit.test("Instantiation of a simple graph", (assert)=>{
			var plotter = new MLPlot({container:"#ui"});
			plotter.plotPoints({
				group: "plot1"
				, x: [1,2,3,4,5,6]
				, y: [1,2,3,4,5,6]
			})

			plotter.plotPoints({
				group: "plot2"
				, x: [8,8,8,8]
				, y: [1,2,3,4]
			})

			var plotJoin = extractPlotStrings("plot1");
			assert.ok(plotJoin.indexOf("1,1")!==-1);
			assert.ok(plotJoin.indexOf("2,2")!==-1);
			assert.ok(plotJoin.indexOf("3,3")!==-1);
			assert.ok(plotJoin.indexOf("4,4")!==-1);
			assert.ok(plotJoin.indexOf("5,5")!==-1);
			assert.ok(plotJoin.indexOf("6,6")!==-1);

			var plotJoin = extractPlotStrings("plot2");
			assert.ok(plotJoin.indexOf("8,1")!==-1);
			assert.ok(plotJoin.indexOf("8,2")!==-1);
			assert.ok(plotJoin.indexOf("8,3")!==-1);
			assert.ok(plotJoin.indexOf("8,4")!==-1);


			plotter.plotPoints({
				group: "plot2"
				, x: [9,9,9,9]
				, y: [1,2,3,4]
			})
			var plotJoin = extractPlotStrings("plot2");
			assert.ok(plotJoin.indexOf("8,1")!==-1);
			assert.ok(plotJoin.indexOf("8,2")!==-1);
			assert.ok(plotJoin.indexOf("8,3")!==-1);
			assert.ok(plotJoin.indexOf("8,4")!==-1);
			assert.ok(plotJoin.indexOf("9,1")!==-1);
			assert.ok(plotJoin.indexOf("9,2")!==-1);
			assert.ok(plotJoin.indexOf("9,3")!==-1);
			assert.ok(plotJoin.indexOf("9,4")!==-1);


		});

	});
	

	QUnit.module("Human Test:Visualization", 
		{beforeEach: ()=>{
			$('body').append('<div id="qunit-fixture"><div id="ui"></div></div>');
		}, afterEach: (assert)=>{
		    $('#qunit-fixture').remove();
		}
	});

	QUnit.test("Plot line",(assert)=>{
		var plotter = new MLPlot({container:"#ui"});
		plotter.plotLine({
			group: "plot2"
			, y: [9,9,9,9]
			, x: [1,2,3,4]
		})
		plotter.plotLine({
			group: "plot2"
			, y: [1,2,3,4]
			, x: [1,1,1,1]
		})
		plotter.plotLine({
			group: "plot2"
			, y: [1,2,3,4]
			, x: [1,3,8,20]
		})
		plotter.plotPoints({
			group: "plot2"
			, y: [1,2,3,4]
			, x: [4,3,2,-1]
		})

		plotter.plotLine({
			group: "plot2"
			, y: [21,22,23,24]
			, x: [-1,-5,-10,20]
		})

		assert.ok(true, "No validation. Validation by user.");
	});

	QUnit.test("Plot continuous line", (assert)=>{
		var plotter = new MLPlot({container:"#ui"});
		plotter.plotLine({
			group: "line1"
			, x: [1, 2]
			, y: [1, 2]
		});
		plotter.plotLine({
			group: "line2"
			, x: [0, 1]
			, y: [2, 3]
		});

		plotter.plotLine({
			group: "line1"
			, x: [3, 4]
			, y: [3, 4]
		});
		
		assert.ok(true, "Human visual test needed!!");
	});

	QUnit.test("Plotly test",(assert)=>{
		var trace1 = {
		  x: [1, 2, 3, 4],
		  y: [10, 15, 13, 17],
		  mode: 'markers'
		};

		var trace2 = {
		  x: [2, 3, 4, 5],
		  y: [16, 5, 11, 10],
		  mode: 'lines'
		};

		var trace3 = {
		  x: [1, 2, 3, 4],
		  y: [12, 9, 15, 12],
		  mode: 'lines+markers'
		};

		var data = [ trace1, trace2, trace3 ];

		var layout = {
		  title:'Line and Scatter Plot',
		  height: 400,
		  width: 480
		};

		Plotly.newPlot('ui', data, layout);

		assert.ok(true, "Human visual inspection needed");
		
	});

	QUnit.test("Plot gradient descent realtime",(assert)=>{
		var classifier = new LinearRegression({
			trainer: GradientDescent
			, trainerParam: {
				learningRate: 1.0
			}
		});
		var X = new Matrix("1000;2000;3000;4000;5000");
		var y = new Matrix("1000;2000;3000;4000;5000");
		classifier.fit(X, y, {
			learningRate:0.01
			, maxIterations: 1000
		});
		var prediction = classifier.predict("7;8;9");

		assert.ok(Number.isNaN(prediction.ij(0,0)), "No solution 1");
		assert.ok(Number.isNaN(prediction.ij(1,0)), "No solution 2");
		assert.ok(Number.isNaN(prediction.ij(2,0)), "No solution 3");

		// Draw the graph now
		var plotter = new MLPlot({container:"#ui"});

		// After feature normalization, shoule be ok
		classifier.fit(X, y, {
			learningRate: 1.75
			, maxIterations: 1000
			, reportEveryXIteration: 1 // Iterations to report back
			, featureNormalization: true
			//TODO: Does not draw in realtime yet because single thread still in gradient descent
			, asyncProgress: (progress)=>{
				plotter.plotLine({
					group:"CostIterationLine"
					, x : [progress.iteration]
					, y : [progress.cost]
				})
			}
		});


		var prediction = classifier.predict("7;8;9");

		assert.ok(!Number.isNaN(prediction.ij(0,0)), "Prediction comparison 1 - is number");
		assert.ok(!Number.isNaN(prediction.ij(1,0)), "Prediction comparison 2 - is number");
		assert.ok(!Number.isNaN(prediction.ij(2,0)), "Prediction comparison 3 - is number");

		assert.ok(_.round(prediction.ij(0,0),0)==7,"0,0=7");
		assert.ok(_.round(prediction.ij(1,0),0)==8,"0,0=8");
		assert.ok(_.round(prediction.ij(2,0),0)==9,"0,0=9");

	});




	QUnit.module("_Development:Visualization", 
		{beforeEach: ()=>{
			$('body').append('<div id="qunit-fixture"><div id="ui"></div></div>');
		}, afterEach: (assert)=>{
		    $('#qunit-fixture').remove();
		}
	});
	QUnit.skip("Plot labels x y testing",(assert)=>{});


});

	
