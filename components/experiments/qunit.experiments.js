var dimple = require("dimple/dist/dimple.v2.2.0.js");
// var LinearRegression = require("../linear_regression/LinearRegression.js");
// var GradientDescent = require("../trainers/GradientDescent.js");
var Plotly = require('plotly.js/lib/core');
require("babel-polyfill");
var $ = require("jquery");
var Yaml = require('js-yaml');
var crossfilter = require("crossfilter");

// Actual Tests
QUnit.module("Experiments", (hooks)=>{

	QUnit.module("Map tests", (hooks) => {
		hooks.beforeEach(()=>{
		    $('#ui').remove();
		    $('#qunit-fixture').remove();
			$('body').append('<div id="qunit-fixture"><div id="ui"></div></div>');

		});
		hooks.afterEach(()=>{
			
		});


		QUnit.test("Basic Map", (assert)=>{
			
		});
	});
	QUnit.module("CrossFilter", (hooks) => {
		hooks.beforeEach(()=>{
		    $('#ui').remove();
		    $('#qunit-fixture').remove();
			$('body').append('<div id="qunit-fixture"><div id="ui"></div></div>');

		});
		hooks.afterEach(()=>{
			
		});


		QUnit.skip("Crossfilter with json and csv. Dictionary based data", (assert)=>{
			let rawData = {
				dictionaries: {
					fruit: ["apple", "orange", "grape"]
					, shop: ["downtown", "uptown"]
				}
				, data :
`date	fruit	shop	quantity	unitprice
21-01-16	0	0	2031	1.03
21-01-16	1	0	1031	1.53
22-01-16	0	0	3231	1.53
22-01-16	1	0	1231	1.51
21-01-16	0	1	2031	1.03
21-01-16	1	1	1031	1.53
22-01-16	0	1	3231	1.53
22-01-16	1	1	1231	1.51
22-01-16	2	1	231	12.51
`			}

		});

		QUnit.test("Crossfilter experiments", (assert)=>{
	let rawData = 
`date	fruit	shop	quantity	unitprice
21-01-16	apple	downtown	2031	1.03
21-01-16	orange	downtown	1031	1.53
22-01-16	apple	downtown	3231	1.53
22-01-16	orange	downtown	1231	1.51
21-01-16	apple	uptown	2031	1.03
21-01-16	orange	uptown	1031	1.53
22-01-16	apple	uptown	3231	1.53
22-01-16	orange	uptown	1231	1.51
22-01-16	grape	uptown	231	12.51
`;

			let data = d3.tsv.parse(rawData, (d)=>{
				var dateParser = d3.time.format("%d-%m-%Y");
				console.debug(d);

				// Data conversion
				d.quantity = +d.quantity;
				d.unitprice = +d.unitprice;
				d.date = dateParser.parse(d.date);
				return d;
			});

			assert.ok(data.length===9,`Data length is ${data.length}`);

			// Crossfilter init
			let ndx = crossfilter(data);
			let dimFruit = ndx.dimension((x)=>(
				x.fruit
			));
			assert.ok(ndx.size()===9, `Size is ${ndx.size()}` );

			// Top - by fruit (default sorting order of alphabets)
			var result = dimFruit.top(5);
			var index = 0;
			assert.ok(result[index].fruit=="orange", `Fruit is ${result[index++].fruit}`);
			assert.ok(result[index].fruit=="orange", `Fruit is ${result[index++].fruit}`);
			assert.ok(result[index].fruit=="orange", `Fruit is ${result[index++].fruit}`);
			assert.ok(result[index].fruit=="orange", `Fruit is ${result[index++].fruit}`);
			assert.ok(result[index].fruit=="grape", `Fruit is ${result[index++].fruit}`);

			var result = dimFruit.bottom(5);
			var index = 0;
			assert.ok(result[index].fruit=="apple", `Fruit is ${result[index++].fruit}`);
			assert.ok(result[index].fruit=="apple", `Fruit is ${result[index++].fruit}`);
			assert.ok(result[index].fruit=="apple", `Fruit is ${result[index++].fruit}`);
			assert.ok(result[index].fruit=="apple", `Fruit is ${result[index++].fruit}`);
			assert.ok(result[index].fruit=="grape", `Fruit is ${result[index++].fruit}`);

		});

		QUnit.test("D3 data loading and parsing", (assert)=>{
	let rawData = 
`date	fruit	shop	quantity	unitprice
21-01-16	apple	downtown	2031	1.03
21-01-16	orange	downtown	1031	1.53
22-01-16	apple	downtown	3231	1.53
22-01-16	orange	downtown	1231	1.51
`;

			let data = d3.tsv.parse(rawData, (d)=>{
				var dateParser = d3.time.format("%d-%m-%Y");
				console.debug(d);

				// Data conversion
				d.quantity = +d.quantity;
				d.unitprice = +d.unitprice;
				d.date = dateParser.parse(d.date);
				return d;
			});

			console.table(data);
			assert.ok(data.length===4, data.length);

				
		});

	});

	QUnit.module("D3 primitives:Scales", (hooks) => {
		hooks.beforeEach(()=>{
		    $('#ui').remove();
		    $('#qunit-fixture').remove();
			$('body').append('<div id="qunit-fixture"><div id="ui"></div></div>');

		});
		hooks.afterEach(()=>{
			
		});


		QUnit.test("Testing domain, scales and rang:Linear Scalee", (assert)=>{
			//  Linear scale test - no domain, so x=y
			var scale = d3.scale.linear(); // By default it is 1 to 1 = ie. x = y
			assert.ok(scale(1)===1, "Result:" + scale(1) );
			assert.ok(scale(100)===100, "Result:" + scale(100) );
			assert.ok(scale(1000)===1000, "Result:" + scale(1000) );
			
			//  Linear scale test - no range, so map to 0-1
			var scale = d3.scale.linear().domain([0,1000]); // By default it is 1 to 1 = ie. x = y
			assert.ok(scale(1)===0.001, "Result:" + scale(1) );
			assert.ok(scale(100)===0.1, "Result:" + scale(100) );
			assert.ok(scale(1000)===1, "Result:" + scale(1000) );

			var scale = d3.scale.linear(); // By default it is 1 to 1 = ie. x = y
			scale.domain([0,100]).range([0,1000], "Result:" + scale(1) );
			assert.ok(scale(1)===10, "Result:" + scale(1) );
			assert.ok(scale(100)===1000, "Result:" + scale(1) );
			assert.ok(scale(1000)===10000, "Result:" + scale(1000) );

			// Ordinal scales

		});
	});


});

	
