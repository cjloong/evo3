var path = require('path');
var webpack = require("webpack");

module.exports = {
	context: '.'
	,devtool: 'eval-source-map'

    , entry: {
        qunit_linear_regression: path.join(__dirname, "qunit.linear_regression.js")
    },
    output: {
        path: path.join(__dirname, "entries")
        , filename: "qunit.entry.js"
    }
}
