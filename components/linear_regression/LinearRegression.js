var Matrix = require("../math_matrix/math_matrix.js");
var NormalEquation = require("../trainers/NormalEquation.js");
var GradientDescent = require("../trainers/GradientDescent.js");
var featureNormalizer = require("../data_processor/DataProcessor.js").featureNormalizer;

var LinearRegression = module.exports = function (param){
	this.init(param);
}



LinearRegression.defaults = {
	trainer: NormalEquation
	, trainerParam: {}
	, featureNormalization: false
	, fnNormalizeFeature: featureNormalizer
}
LinearRegression.prototype.init = function(param) {
 	this._data = _.defaultsDeep({}, param, LinearRegression.defaults);
}


// Getter/setters
LinearRegression.prototype.data = function(){
	return this._data;
}

LinearRegression.prototype.theta = function(newtheta) {
	if(typeof newtheta == 'undefined') return this.data().theta;
	else {
		this.data().theta = Matrix.matrixify(newtheta);
	}
}
//
//
//
//LinearRegression.prototype._fitNormalEquation = function(X, y) {
//
//	// Preparation
//	var _X = Matrix.matrixify(X);
//	var _y = Matrix.matrixify(y);
//	var sz_X = _X.size();
//	var sz_y = _y.size();
//	var data = this.data();
//
//	// Error checking
//	if(sz_X[0]!==sz_y[0]) throw "Training set(X) and result(y) must have same length.";
//	else {
//		var m = sz_X[0];
//		var n = sz_X[1];
//
//		// Initializing theta array
//		data.theta = Matrix.zero(sz_X[1] + 1, 1);
//
//		// Append 1 to left of X
//		_X = _X.appendLeft(Matrix.one(m,1));
//
//		data.theta = _X.transpose()
//			.mul(_X)
//			.inv()
//			.mul(_X.transpose()).mul(_y);
//		
//	}
//
//	
//}

LinearRegression.prototype.H = function(X, theta) {
	var _X = Matrix.matrixify(X)

	_X = _X.appendLeft(Matrix.one(_X.size()[0],1));
	var _theta = Matrix.matrixify(theta);
	return _X.AxB(_theta);
}

// LinearRegression.prototype._prepareMatrix = function(obj) {
// 	if(obj instanceof Matrix) return obj;
// 	else return new Matrix(obj);
// }

// h(X) = X * theta = t0 + t1x1 + t2x2 + ... + tnxm
LinearRegression.prototype.predict = function(X) {

    var MX = Matrix.matrixify(X);
    var data = this.data();

	if(data.theta == null) throw "Theta is not defined. Perhaps training was not done properly. Consider running fit function correctly first.";
    
    if(data.featureNormalization===true) {
        var normParam = data.normParam;
        data.fnNormalizeFeature(MX,null,normParam);
    }
    return this.H(MX, this.theta());

}

// Returns the coefficient of determination R^2 of the prediction.
// The coefficient R^2 is defined as (1 - u/v), where u is the regression
// sum of squares ((y_true - y_pred) ** 2).sum() and v is the residual sum
// of squares ((y_true - y_true.mean()) ** 2).sum(). Best possible score
// is 1.0 and it can be negative (because the model can be arbitrarily worse).
// A constant model that always predicts the expected value of y, disregarding
// the input features, would get a R^2 score of 0.0.
LinearRegression.prototype.score = function(X, y) {
	var predictY = this.predict(X);
	var trueY = Matrix.matrixify(y);
	var minus = trueY.minus(predictY);
	var trueYMean = trueY.colMean().ij(0,0);

	var residule = trueY.mapAll(function(i){
		return i = trueYMean;
	});
	
	// Sum square of errors
	var sumSquare = minus.elementSq().colSum().ij(0,0);
	var meanSquare = residule.elementSq().colSum().ij(0,0);

	return 1 - sumSquare/meanSquare;

// 	function square(i) {
// 		return i*i;
// 	}
	
}


LinearRegression.prototype.persistFitness = function(trainingSet) {
	throw "TODO: Implement me";
}


LinearRegression.prototype.costFunction = function costFunction(X, y, theta) {
	var m = X.rowCount();
	var E = X.mul(theta).minus(y);
	var multiplier = 1 / m; //TODO: Refactor alpha out because alpha is only applicable to gradient descent
	var gradient = X.transpose().mul(E).elementMul(multiplier);
	return {
		J: (1/(2*m) * E.sumSquare() ) //TODO: Implement J cost
		, gradient: gradient
	}

}

// LinearRegression.prototype.asyncFit = function(X, y, param){

// }

LinearRegression.prototype.fit = function(X, y, param) {
	var _X = Matrix.matrixify(X);
	var _y = Matrix.matrixify(y);
	var m = _X.rowCount();

	var data = this.data();
	var param = _.defaultsDeep({}, param, data.trainerParam);//

	//TODO: Make this configurable
	param.costFunction = this.costFunction;

	// TODO: Refactor out to a data processing class. Not part of machine learning
	var featureNormalization = param.featureNormalization;

	if(featureNormalization) {
		// mean xi' = (xi - mean(xi))/(max(xi) - min(xi))
		// denominator can use std as well
		data.normParam = data.fnNormalizeFeature(_X,_y);
		data.featureNormalization = true;
// 		this.normalizeFeature(_X, _y, normParam);
		
	}

	var MX = _X.appendLeft(Matrix.one(m));

	var generator = data.trainer(MX,_y,param);
	do {
		var ret = generator.next();
		//TODO: Handle some progress reporting here
		if(!ret.done && param.asyncProgress!=null) {
			param.asyncProgress(ret.value);
		}
	} while(!ret.done);

	data.trainerParam = param;
	data.theta = ret.value;

}

//
// Normalize the feature. This is the same for all machine learning.
// TODO: Refactor this to machine learning common
//
//LinearRegression.normalizeFeature = 
//LinearRegression.prototype.normalizeFeature = function(X, y, normParam){
//	var MX = Matrix.matrixify(X);
//	var mean = normParam.mean;
//	var S = normParam.max - normParam.min;
//
//
//	return MX.mapAllInPlace(function(i){
//		return (i - mean)/S;
//	});
//	// 
//}

