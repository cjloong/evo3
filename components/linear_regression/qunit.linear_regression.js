var Matrix = require("../math_matrix/math_matrix.js");
var LinearRegression = require("./LinearRegression.js");
var evoMath = require("../math_matrix/math.js"); //TODO: Deprecate this
var GradientDescent = require ("../trainers/GradientDescent.js");

QUnit.module("Linear Regression", (assert)=>{
	
	QUnit.module("Unit Test:Linear Regression Test");
	QUnit.test("Linear Regression Warmup", function(assert){
		var LR = new LinearRegression();
		assert.ok(true,"Empty instantiation");
	});

	QUnit.test("Fit a perfect line - univariate (theta1=0)", function(assert){
		var classifier = new LinearRegression();
		var X = new Matrix("1;2;3;4;5");
		var y = new Matrix("1;2;3;4;5");

		var ret = classifier.fit(X, y);

		var theta = classifier.theta();
		assert.ok(evoMath.compareFloat(theta.ij(0,0), 0) === 0);
		assert.ok(evoMath.compareFloat(theta.ij(1,0), 1) === 0);

		var prediction = classifier.predict("7;8;9");

		assert.ok(prediction.ij(0,0).toFixed(2)==="7.00");
		assert.ok(prediction.ij(1,0).toFixed(2)==="8.00");
		assert.ok(prediction.ij(2,0).toFixed(2)==="9.00");

	});

	QUnit.test("Fit a perfect line - univariate (theta1=3)", function(assert){
		var classifier = new LinearRegression();
		var X = new Matrix("1;2;3;4;5");
		var y = new Matrix("4;5;6;7;8");
		classifier.fit(X, y);
		assert.ok(classifier.data().theta != null);

		var theta = classifier.theta();
		assert.ok(evoMath.compareFloat(theta.ij(0,0), 3) === 0);
		assert.ok(evoMath.compareFloat(theta.ij(1,0), 1) === 0);

		var prediction = classifier.predict("7;8;9");

		assert.ok(prediction.ij(0,0).toFixed(2)==="10.00");
		assert.ok(prediction.ij(1,0).toFixed(2)==="11.00");
		assert.ok(prediction.ij(2,0).toFixed(2)==="12.00");

	});

	QUnit.test("Scoring a perfect line - univariate - simulate 1 mistake (theta1=3)", function(assert){
		var classifier = new LinearRegression();
		var X = new Matrix("1;2;3;4;5");
		var y = new Matrix("4;5;6;7;8");

		classifier.fit(X, y);

		var theta = classifier.theta();
		assert.ok(evoMath.compareFloat(theta.ij(0,0), 3) === 0);
		assert.ok(evoMath.compareFloat(theta.ij(1,0), 1) === 0);

		var prediction = classifier.predict("7;8;9");

		assert.ok(prediction.ij(0,0).toFixed(2)==="10.00");
		assert.ok(prediction.ij(1,0).toFixed(2)==="11.00");
		assert.ok(prediction.ij(2,0).toFixed(2)==="12.00");

		var score = classifier.score("10;11;12","13;11;15")
		assert.ok(score.toFixed(2)==="0.98"); //TODO: Not sure about the accuracy. Need to revisit


	});

	QUnit.test("Gradient Descent - Perfect Linear plots - with normalization", function(assert){
		var classifier = new LinearRegression({
			trainer: GradientDescent
			, trainerParam: {
				learningRate: 1.0
			}
		});
		var X = new Matrix("1000;2000;3000;4000;5000");
		var y = new Matrix("1000;2000;3000;4000;5000");
		classifier.fit(X, y, {
			learningRate:0.01
			, maxIterations: 1000
		});
		var prediction = classifier.predict("7;8;9");

		assert.ok(Number.isNaN(prediction.ij(0,0)), "No solution 1");
		assert.ok(Number.isNaN(prediction.ij(1,0)), "No solution 2");
		assert.ok(Number.isNaN(prediction.ij(2,0)), "No solution 3");

		// After feature normalization, shoule be assert.ok

		classifier.fit(X, y, {
			learningRate: 0.01
			, maxIterations: 100000
			, featureNormalization: true
		});
		var prediction = classifier.predict("7;8;9");

		assert.ok(!Number.isNaN(prediction.ij(0,0)), "Prediction comparison 1 - is number");
		assert.ok(!Number.isNaN(prediction.ij(1,0)), "Prediction comparison 2 - is number");
		assert.ok(!Number.isNaN(prediction.ij(2,0)), "Prediction comparison 3 - is number");

		assert.ok(evoMath.compareFloat(prediction.ij(0,0),7,2)===0, "Prediction comparison 1");
		assert.ok(evoMath.compareFloat(prediction.ij(1,0),8,2)===0, "Prediction comparison 2");
		assert.ok(evoMath.compareFloat(prediction.ij(2,0),9,2)===0, "Prediction comparison 3");


	});


	QUnit.test("Gradient Descent - Perfect Linear plots", function(assert){
		var classifier = new LinearRegression({
			trainer: GradientDescent
			, trainerParam: {
				learningRate: 1.0
			}
		});
		var X = new Matrix("1;2;3;4;5");
		var y = new Matrix("4;5;6;7;8");
		classifier.fit(X, y, {
			learningRate:0.1
			, maxIterations: 1000
		});

		assert.ok(classifier.data().theta!=null, "Theta not defined after training.");

		var prediction = classifier.predict("7;8;9");

		assert.ok(prediction.ij(0,0).toFixed(2)==="10.00");
		assert.ok(prediction.ij(1,0).toFixed(2)==="11.00");
		assert.ok(prediction.ij(2,0).toFixed(2)==="12.00");

	});


	QUnit.module("_Development:Linear Regression");
	QUnit.skip("Fit - Regularized Normal Equation", function(assert){});
	QUnit.skip("Fit - Regularized Gradient Descent", function(assert){});
	QUnit.skip("Fit - Gradient Descent with different parameters", function(assert){});
});
