var Matrix = require("../math_matrix/math_matrix.js");
var NeuralNetwork = require("./NeuralNetwork.js");

QUnit.module("Neural Network", (assert)=>{

	QUnit.module("Unit Test:Neural Network");
	QUnit.test("Neural Network Warmup", function(assert){
		var nn = new NeuralNetwork();
		assert.ok(true,"Empty instantiation");
	});

	QUnit.test("Prediction test - Forward Propagation only", function(assert){
		var nn = new NeuralNetwork();
		var theta1 = new Matrix(`
		   0.00000   0.90930  -0.75680;
		   0.47943   0.59847  -0.97753;
		   0.84147   0.14112  -0.95892;
		   0.99749  -0.35078  -0.70554
		`);
		assert.ok(theta1.rowCount()===4,"theta1 rowcount check");
		assert.ok(theta1.colCount()===3,"theta1 colcount check");

		var theta2 = new Matrix(`
		   0.00000   0.93204   0.67546  -0.44252  -0.99616;
		   0.29552   0.99749   0.42738  -0.68777  -0.92581;
		   0.56464   0.97385   0.14112  -0.87158  -0.77276;
		   0.78333   0.86321  -0.15775  -0.97753  -0.55069
		`);
		assert.ok(theta2.rowCount()===4,"theta1 rowcount check");
		assert.ok(theta2.colCount()===5,"theta1 colcount check");


		var X = new Matrix(`
		   0.84147   0.41212;
		   0.90930  -0.54402;
		   0.14112  -0.99999;
		  -0.75680  -0.53657;
		  -0.95892   0.42017;
		  -0.27942   0.99061;
		   0.65699   0.65029;
		   0.98936  -0.28790
		`);
		assert.ok(X.rowCount()===8,"theta1 rowcount check");
		assert.ok(X.colCount()===2,"theta1 colcount check");

		nn.theta(theta1);  // Inserting layer 1
		nn.theta(theta2);  // Inserting layer 2

		var prediction = nn.predict(X);
		// Theta1 = reshape(sin(0 : 0.5 : 5.9), 4, 3);
		// Theta2 = reshape(sin(0 : 0.3 : 5.9), 4, 5);
		// X = reshape(sin(1:16), 8, 2);
		// p = predict(Theta1, Theta2, X)
		// % you should see this result
		// p = 
		//   4
		//   1n
		//   1
		//   4
		//   4
		//   4
		//   4
		//   2
		assert.ok(_.round(prediction.ij(0,0),3)===3, "1 Prediction comparison 3:" + prediction.ij(0,0),3);
		assert.ok(_.round(prediction.ij(1,0),3)===0, "2 Prediction comparison 0:" + prediction.ij(1,0),3);
		assert.ok(_.round(prediction.ij(2,0),3)===0, "3 Prediction comparison 0:" + prediction.ij(2,0),3);
		assert.ok(_.round(prediction.ij(3,0),3)===3, "4 Prediction comparison 3:" + prediction.ij(3,0),3);
		assert.ok(_.round(prediction.ij(4,0),3)===3, "5 Prediction comparison 3:" + prediction.ij(4,0),3);
		assert.ok(_.round(prediction.ij(5,0),3)===3, "6 Prediction comparison 3:" + prediction.ij(5,0),3);
		assert.ok(_.round(prediction.ij(6,0),3)===3, "7 Prediction comparison 3:" + prediction.ij(6,0),3);
		assert.ok(_.round(prediction.ij(7,0),3)===1, "8 Prediction comparison 1:" + prediction.ij(7,0),3);


	});

	QUnit.test("NN AND architecture - Forward Propagation only",(assert)=>{

		var nn = new NeuralNetwork();
		var theta1 = new Matrix(`
			-30 20 20
		`);
	// 	var theta2 = new Matrix(`-10 20 20`);
		nn.theta(theta1);  // Inserting layer 1
	// 	nn.theta(theta2);  // Inserting layer 2

		var one_one = (new Matrix("1 1"));
		var result = nn.predict(one_one);
		assert.ok(result.ij(0,0)===1, "1 && 1");

		var one_zero = (new Matrix("1 0"));
		var result = nn.predict(one_zero);
		assert.ok(result.ij(0,0)===0, "1 && 0");


		var zero_one = (new Matrix("0 1"));
		var result = nn.predict(zero_one);
		assert.ok(result.ij(0,0)===0, "0 && 1");


		var zero_zero = (new Matrix("0 0"));
		var result = nn.predict(zero_zero);
		assert.ok(result.ij(0,0)===0, "0 && 0");


	});

	QUnit.test("NN OR architecture - Forward Propagation only",(assert)=>{
		var nn = new NeuralNetwork();
		var theta1 = new Matrix(`
			-10 20 20
		`);
	// 	var theta2 = new Matrix(`-10 20 20`);
		nn.theta(theta1);  // Inserting layer 1
	// 	nn.theta(theta2);  // Inserting layer 2

		var one_one = (new Matrix("1 1"));
		var result = nn.predict(one_one);
		assert.ok(result.ij(0,0)===1, "1 && 1");

		var one_zero = (new Matrix("1 0"));
		var result = nn.predict(one_zero);
		assert.ok(result.ij(0,0)===1, "1 && 0");


		var zero_one = (new Matrix("0 1"));
		var result = nn.predict(zero_one);
		assert.ok(result.ij(0,0)===1, "0 && 1");


		var zero_zero = (new Matrix("0 0"));
		var result = nn.predict(zero_zero);
		assert.ok(result.ij(0,0)===0, "0 && 0");

	});

	QUnit.test("NN NOR architecture - Forward Propagation only",(assert)=>{
		var nn = new NeuralNetwork();
		var theta1 = new Matrix(`
			10 -20 -20
		`);
	// 	var theta2 = new Matrix(`-10 20 20`);
		nn.theta(theta1);  // Inserting layer 1
	// 	nn.theta(theta2);  // Inserting layer 2

		var one_one = (new Matrix("1 1"));
		var result = nn.predict(one_one);
		assert.ok(result.ij(0,0)===0, "1 && 1");

		var one_zero = (new Matrix("1 0"));
		var result = nn.predict(one_zero);
		assert.ok(result.ij(0,0)===0, "1 && 0");


		var zero_one = (new Matrix("0 1"));
		var result = nn.predict(zero_one);
		assert.ok(result.ij(0,0)===0, "0 && 1");


		var zero_zero = (new Matrix("0 0"));
		var result = nn.predict(zero_zero);
		assert.ok(result.ij(0,0)===1, "0 && 0");

	});

	QUnit.test("NN XNOR architecture - Forward Propagation only",(assert)=>{
		var nn = new NeuralNetwork();
		var theta1 = new Matrix(`
			-30 20 20;
			10 -20 -20
		`);
		nn.theta(theta1);

		var theta2 = new Matrix(`
			-10 20 20
		`);
		nn.theta(theta2);

		// XNOR
		var one_one = (new Matrix("1 1"));
		var result = nn.predict(one_one);
		assert.ok(result.ij(0,0)===1, "1 && 1");

		var one_zero = (new Matrix("1 0"));
		var result = nn.predict(one_zero);
		assert.ok(result.ij(0,0)===0, "1 && 0");


		var zero_one = (new Matrix("0 1"));
		var result = nn.predict(zero_one);
		assert.ok(result.ij(0,0)===0, "0 && 1");


		var zero_zero = (new Matrix("0 0"));
		var result = nn.predict(zero_zero);
		assert.ok(result.ij(0,0)===1, "0 && 0");

	});

	QUnit.module("_Development:Neural Network");
	QUnit.skip("Andrew Ng Machine Learning assignment",(assert)=>{});
	QUnit.skip("XOR NN - forward propagation only", (assert)=>{});
	QUnit.skip("Cost function - without regularization",(assert)=>{});
	QUnit.skip("Cost function - with regularization",(assert)=>{});
	QUnit.skip("Back propagation",(assert)=>{});
	QUnit.skip("Training and predicting OR - Back propagation",(assert)=>{});
	QUnit.skip("Internal nodes setup",(assert)=>{});
});
