var Matrix = require("../math_matrix/math_matrix.js");
var GradientDescent = require("../trainers/GradientDescent.js");
var _ = require("lodash");

module.exports = class NeuralNetwork {

	constructor(param) {
		this._data = _.defaultsDeep({}, param, NeuralNetwork.defaults);	
	}

	data() {
		return this._data;
	}

	// Theta now is an array
	theta(newtheta){
		var data = this.data();
		if(typeof data.theta=='undefined') {
			data.theta = [];
		}

		if(typeof newtheta == 'undefined') return data.theta;
		else {

			data.theta[data.theta.length] = Matrix.matrixify(newtheta);
			return data.theta;
		}
	}

	thetaPop() {
		return this.data().theta.pop();
	}

	score(X,y) {
		var prediction = this.predict(X);
		var m = prediction.rowCount();
		var diff = prediction.minus(y).elementAbs().colSum().ij(0,0);

		return (m-diff)/m;
	}

	predict(X){
		// Forward propagation
		var a1 = Matrix.matrixify(X);
		

		var data = this.data();
		var theta = this.theta();
		
		// doing forward propagation
		for(var i = 0; i<theta.length; i++) {
			var m = a1.rowCount();
			a1 = a1.appendLeft(Matrix.one(m,1));
			var curTheta = theta[i];
			var z2 = curTheta.mul(a1.transpose());
			a1 = z2.elementSigmoid().transpose(); 
		}

		if(a1.rowCount()===1) {
			return a1.mapAll(function(item){
				return item>=0.5 ? 1:0;
			});

		} else {
			return a1.rowMaxColIndex();
		}
	}

}

// var NeuralNetwork = module.exports = function (param){
// 	this.init(param);
// }


// ////////////////////////////////////////////////////////////////////
// // Construction
// NeuralNetwork.defaults = {
// //	trainer: GradientDescent
// //	, trainerParam: {
// //		maxIterations: 1000
// //	}
// }

// NeuralNetwork.prototype.init = function(param) {
//  	this._data = _.defaultsDeep({}, param, NeuralNetwork.defaults);	
// }


// ////////////////////////////////////////////////////////////////////
// // Getter/setters
// //TODO: Move to parent object
// NeuralNetwork .prototype.data = function(){
// 	return this._data;
// }

// //TODO: Move to parent object
// NeuralNetwork.prototype.theta = function(newtheta) {
// 	var data = this.data();

// 	if(typeof newtheta == 'undefined') return data.theta;
// 	else {
// 		data.theta = Matrix.matrixify(newtheta);
// 		return data.theta;
// 	}
// }



// NeuralNetwork.prototype.fit = function(X, y, param) {
// 	throw "TODO:";
// }

// //TODO: Maybe can refactor this to a common parent object
// NeuralNetwork.prototype.score = function(X, y) {
// 	var prediction = this.predict(X);
// 	var m = prediction.rowCount();
// 	var diff = prediction.minus(y).elementAbs().colSum().ij(0,0);
	
// 	return (m-diff)/m;
// }


// // Output clasification {0,1}
// NeuralNetwork.prototype.predict = function(X) {
// 	throw "TODO:";
// }

// // Output the chance of true { 0 >= h(x) <=1}
// NeuralNetwork.H = NeuralNetwork.prototype.H = function(X, theta) {
// 	throw "TODO:";
// }

