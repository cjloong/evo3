# Linear Programming
Given a set of variables (real values)
Given a set of constraints
Maximize and minize linear functions

Use for optimizations
3 possible case of LP
- Infeasible
- Unbounded (no finite optimum)
- (Usually)Finite optimum (there is an optimal solution)
	- it could be multiple optimal solutions
	
Algorithm - Simplex Algorithm
- start at the vertex
- if improvement at adjacent, take it
- keep doing that until no more improvement

There are rare case that algorithm is exponential but it does not show up much in practice

Another class of algorithm
- Interior point methods
	- start at middle of polyhedron
	- move until reach optimal solution
	
Both can work, sometimes each work better than the others.
Fitting line to points minimizing least sq error (regresssion) usually done using
calculus. LP can do it as well.

NOTE: Assignment problems usually have a integer solution. 0 1
Other LP may not.

Example:
A factory can produce 2 products x and y
Profit is 20 for 1 and 30 for 2
ie. 20x + 30y (objective function)

Constraints
Max per day  is 400 for x and 300 for y
So, x<=400 and y<=300
Production rate is 60 tons/hr for x
50 tons/hr for y
1 day is 8 hrs


In math:
Find max 20x + 30y

Subjec to:
0<=x<=400
0<=y<=300

x/60 + y/50 <=8

Say if y = 300
x/60 + 300/50 = 8
x/60 = 8 - 6 = 2
x = 2 * 60 = 120 (this is one possible value)


if max demand for y is 200 (ie. 0 <= y <= 200)
x / 60 + 200 / 50  = 8
x / 60 = 8 - 4 = 4
x = 60 * 4 = 240


10x + 30y = 20 . 240 + 200 . 30 = 4800 + 6000 = 10800

Question 2
An advertising agency wants to run a campaign for a new product on print media and TV. A print advertisement 
costs $20,000 and can reach 1 million people; a TV advertisement costs $50,000 and can reach 2 million. Assume 
for simplicity that different advertisements reach different people. There can be at most 40 advertisements 
on print and 15 advertisements on TV. The agency has a budget of $1 million for the campaign.

What is the best campaign that will reach the maximum number of people? Formulate a Linear Program for this problem. 
Draw on the plane the feasible region and find the optimal solution. How many TV advertisements should the agency run?

Let x = print, y = tv

Maximize 1000000x + 2000000y

Constraints
0<=x<=40
0<=y<=15
20000x + 50000y <= 1000000


If y = 15,
20000x + 50000 . 15 = 1000000
20000 . x = 1000000 - 50000 . 15
x = 12.5

If x = 40
20000 . 40 + 50000 y = 1000000
y = 4
1

NP
- decision problem vs optimization

P and NP

# Genomics
- We will consider personal genomics
	- adapting medicine for personal genome
- Life is made of cells
	- each cells have chromosome
	- chrom. have inherited information
	- DNA is a molecular language (polymer)
	- double helical structure
	- ACGD building block
- History
	- b4 90's virus genome
	- 90's we are able to sequence yeast (organism with nucleus)
	- after that worms
	- 2004 - can do genome
	- now different individuals
- Human genome
	- 3 billion (exactly same length) mostly identical to reference human genome
	- 0.1 percent, abt 3 mill are different from standard humans
	- majority of 3 mil are nothing special (normal variation)
		- things like height, hair color, ...
	- ten of thousands of variants that matter (only present in 1/2 population or less)
		- normal less save and may cause disease
		- some would be harmful, not all
	- only a small (hundreds) part are mutation (distinguish me to parents/siblings)
	
	