/**
 * This is unit test for bdd over qunit library
 */

class BDD {
	constructor({okCb=null}){
		if(okCb!=null) {
			this.ok = okCb;
		}

	}

	get __test(){
		return this._test = this._test || {};
	}
	get world(){
		return this._world= this._world || {};
	}
	
	_storeTest(set, regex, fn) {
		var tests = this.__test;
		var set  = tests[set.toLowerCase()] = tests[set.toLowerCase()] || [];
		set.push({
			regex: regex
			, fn: fn
		});
	}

	// Storing the test implementations
	given(regex, fn) {
		var lastSet = this.lastSet = "given";
		this._storeTest(lastSet, regex, fn);
		return this;
	}
	when(regex, fn) {
		var lastSet = this.lastSet = "when";
		this._storeTest(lastSet, regex, fn);
		return this;
	}
	then(regex, fn) {
		var lastSet = this.lastSet = "then";
		this._storeTest(lastSet, regex, fn);
		return this;
	}
	and(regex, fn) {
		var lastSet = this.lastSet;
		this._storeTest(lastSet, regex, fn);
		return this;
	}

	// Specification parsing
	spec(specString){
		var tests = this.__test;
		var scenarios = this.__scenarios = this.__scenarios || [];

		var specs = specString.split("\n").map((i)=>(i.trim())).filter((i)=>(i.length>0));

		// Creating the spec sequence
		var currentScenario = specs.reduce((r,i)=>{

			var iparsed = /^(given|when|then|and)(.*)/i.exec(i);
			var setLine = iparsed[1].trim();
			var testLine = iparsed[2].trim();

			var set = setLine.toLowerCase();
			var realSet = set;
			if(set!="and") {
				r.lastSet = set;
			} else {
				set = r.lastSet;
			}

			//TODO: If cannot find have to report error and throw the set away
			var filteredRegexTest = r.tests[set].filter((i)=>(i.regex.test(iparsed[2].trim())));
			var setImpl = filteredRegexTest.map((i)=>{
				var paramExtract = i.regex.exec(testLine).filter((i, index, rows)=>(index%2===1));


				return {
					param: paramExtract
					, isValidTest:true
					, fn: i.fn
					, name: i.regex.toString()
					, testLine: testLine
					, set: set
					, realSet: realSet
				}
			});

			if(setImpl.length>1) {
				console.warn("Ambigious implementations matched. We used first matched");
				console.debug(setImpl.map((i)=>(i.name)));
			} else if(setImpl.length===1) {
				r.impl.push(setImpl[0]);
			} else {
				//error
				r.impl.push({
					isValidTest:false
					, testLine: testLine
					, set: set
					, realSet: realSet

				})
			}


			return r;
		},{lastSet: "given", impl:[], tests: tests});


		scenarios.push(currentScenario);

		return this;
	}

	// Local test runner. We need to bridge this to other frameworks
	test() {
		var self = this;
		var tests = this.__test;
		var scenarios = this.__scenarios = this.__scenarios || [];
		scenarios.forEach((S)=>{
			S.impl.every((testSet)=>{
				if(testSet.isValidTest) {
					this.ok(true, `${testSet.set.toUpperCase()}->${testSet.testLine}`);
					testSet.fn(self.world, ... testSet.param);
					return true;
				} else {
					this.ok(false, `NOT IMPLEMENTED:${testSet.set.toUpperCase()}->${testSet.testLine}`);
					return testSet.set.toLowerCase()=="then";
				}
			});
		});
	}

	ok(cond, message) {
		if(!cond) console.error(message);
	}
}


QUnit.module("BDD", (assert)=>{
	// Test objects
	class Cat {
		
		constructor(name) {
			this.name = name;
			this.state = "ok";
		}
		
		feed(food) {
			if(food=="chili") this.state="nasty";
			else this.state="nice";
		}
	}

	var CatBdd = function(assert=null,okCb=null){
 		var catBdd = new BDD({
			okCb : okCb
 		});
		catBdd.given(/A cat called "(.+):?(.*)"/i, (world, name)=>{
			world.animal = new Cat(name); 
		}).given(/A bob the cat/i, (world)=>{
			world.animal = new Cat("Bob"); 
		}).when(/We feed it "(.*)"/i, (world, food)=>{
			var cat = world.animal;
			cat.feed(food);
		}).then(/It becomes "(.*)"/i, (world, state)=>{
			var cat = world.animal;
			assert.ok(cat.state==state, `---->${state}==${cat.state}`);
		}); // Test defaults;

		return catBdd;
	}
	


	QUnit.module("_Development",(assert)=>{

		QUnit.test("Non matched given implementations",(assert)=>{
			var okCount = 0;
			var nokCount = 0;

			var bdd = CatBdd(assert, (cond,message)=>{
				if(cond) {
					okCount++;
				} else {
					nokCount++;
				}
				assert.ok(true, message);
			});
			bdd.spec(`
				Given a dog that is undefined
				When we feed it "chili"
				Then It becomes "nasty"
			`).test();
			assert.ok(okCount===0);
			assert.ok(nokCount===1);

		});

		QUnit.test("Non matched when implementations",(assert)=>{
			var okCount = 0;
			var nokCount = 0;
			var bdd = CatBdd(assert, (cond,message)=>{
				if(cond) {
					okCount++;
				} else {
					nokCount++;
				}
				assert.ok(true, message);
			});
			bdd.spec(`
				Given a cat called "Ah Kow"
				When we feed it fresh "chili"
				Then It becomes "nasty"
			`).test();
			
			assert.ok(okCount===1);
			assert.ok(nokCount===1);
		});

		QUnit.test("Non matched first then implementations",(assert)=>{
			var okCount = 0;
			var nokCount = 0;
			var bdd = CatBdd(assert, (cond,message)=>{
				if(cond) {
					okCount++;
				} else {
					nokCount++;
				}
				assert.ok(true, message);
			});
			bdd.spec(`
				Given a cat called "Ah Kow"
				When we feed it "chili"
				Then It becomes "nasty"
				And It becomes WRONG "nasty"
				And It becomes "nasty"
			`).test();
			
			assert.ok(okCount===4, "Then will go on despite errors");
			assert.ok(nokCount===1);

		});
	});

	QUnit.module("Unit Test", (assert)=>{
		QUnit.test("Warmup", (assert)=>{
			assert.ok(true, "Warmup ok");
		});
		QUnit.test("Testing basic", (assert)=>{
			var bdd = CatBdd(assert);

			// Test other specs
			bdd.spec(`
					Given a cat called "Max"
					When we feed it "fish"
					Then It becomes "nice"
			`).test();

			bdd.spec(`
					Given a cat called "Bob"
					When we feed it "chili"
					Then It becomes "nasty"
			`).test();


		});	

	});
	
});