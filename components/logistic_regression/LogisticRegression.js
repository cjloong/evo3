var Matrix = require("../math_matrix/math_matrix.js");
var GradientDescent = require("../trainers/GradientDescent.js");

var _ = require("lodash");
var LogisticRegression = module.exports = function (param){
	this.init(param);
}

////////////////////////////////////////////////////////////////////
// Construction
LogisticRegression.defaults = {
	trainer: GradientDescent
	, trainerParam: {
		maxIterations: 10000
		, learningRate: 0.1
	}
}

LogisticRegression.prototype.init = function(param) {
 	this._data = _.defaultsDeep({}, param, LogisticRegression.defaults);	
}


////////////////////////////////////////////////////////////////////
// Getter/setters
LogisticRegression .prototype.data = function(){
	return this._data;
}

LogisticRegression.prototype.theta = function(newtheta) {
	var data = this.data();

	if(typeof newtheta == 'undefined') return data.theta;
	else {
		data.theta = Matrix.matrixify(newtheta);
		return data.theta;
	}
}


LogisticRegression.prototype.fit = function(X, y, param) {
	var data = this.data();
	// Find out more about y, how many different y are there
	var _X = Matrix.matrixify(X);
	var _y = Matrix.matrixify(y);
	var diffLabels = _y.colUnique();

	if(diffLabels.length<3) return this._fit1(_X, _y, param);
	else {
		data.diffLabels = diffLabels;
		for(var i = 0; i<diffLabels.length; i++) {
			var result = this._fitCommon(X, y, param, function(e){
				return e===diffLabels[i] ? 1 : 0;
			});

			if(i==0) data.theta = result.result;
			else data.theta = data.theta.appendRight(result.result);

		}

	}
}

LogisticRegression.prototype._fit1 = function(X, y, param) {
	var data = this.data();
	var result = this._fitCommon(X, y, param);
	data.trainerParam = result.param;
	data.theta = result.result;
	data.diffLabels = null;
}

LogisticRegression.prototype._fitCommon = function(X, y, param, fnRemapY) {
	var data = this.data();
	var _X = Matrix.matrixify(X);
	var _y = Matrix.matrixify(y);
	if(typeof fnRemapY != 'undefined') {
		_y = _y.mapAll(fnRemapY);
	}

	var m = _X.rowCount();

	var param = _.defaultsDeep({}, param, data.trainerParam);//
								//REFACTORING:, LinearRegression.modes.GradientDescent);
	param.costFunction = this.costFunction;

	var featureNormalization = param.featureNormalization;

	if(featureNormalization) {
		// mean xi' = (xi - mean(xi))/(max(xi) - min(xi))
		// denominator can use std as well
        var normParam = param.normParam = _X.colMaxMinSum();
        normParam.mean = normParam.sum / m;
        
		this.normalizeFeature(_X, _y, normParam);
		
	}

	// TODO: Refactor out the cost function
	var MX = _X.appendLeft(Matrix.one(m));

	var generator = data.trainer(MX,_y,param);
	do {
		var ret = generator.next();

		//TODO: Handle some progress reporting here

	} while (!ret.done)

	return {
		result: ret.value
		, param: param
	}
}


LogisticRegression.prototype.score = function(X, y) {
	var prediction = this.predict(X);
	var m = prediction.rowCount();
	var diff = prediction.minus(y).elementAbs().colSum().ij(0,0);
	
	return (m-diff)/m;
}


// Output clasification {0,1}
LogisticRegression.prototype.predict = function(X) {
	var theta = this.theta();
	var _X = Matrix.matrixify(X);
 	var _X = _X.appendLeft(Matrix.one(X.rowCount(), 1));
	var data = this.data();

	if(theta.colCount()===1) {
		var R = this.H(_X,theta).mapAll(function(item){
			return item>=0.5 ? 1:0;
		});
	} else {
		// Multiple lable clasification
// 		var results = [];
// 		debugger;
// 		//TODO: Vectorize theta. We do not need the loop
// 		for(var i = 0; i<theta.length; i++) {
// 			var R = this.H(_X,theta[i]);
// 			results[results.length] = R;
// 		}
		var R = _X.mul(theta).rowMaxColIndex().mapAll(function(lblIndex){
			return data.diffLabels[lblIndex];
		});

	}

	return R;	
}

// Output the chance of true { 0 >= h(x) <=1}
LogisticRegression.H = LogisticRegression.prototype.H = function(X, theta) {
	var _X = Matrix.matrixify(X);
	var _theta = Matrix.matrixify(theta);

	return _X.mul(_theta).elementSigmoid();

}

//TODO: Move this to matrix?
//TODO: Deprecate this
LogisticRegression.sigmoid = LogisticRegression.prototype.sigmoid = function(num){

		// TODO: Handle other param types
	var M = Matrix.matrixify(num);

// 	M.mapAll(function(element){
// 		return 1/(1 + Math.pow(Math.E, -element));
// 	});

	return M.elementSigmoid();
}

// Use to compute descent and to train theta
// We dont take this.theta because this function is typically called
// by a training function to locate/search the correct theta
//TODO: Deprecate this
//@Deprecate
LogisticRegression.costFunction = LogisticRegression.prototype.costFunction = function(X, y, theta) {
	return LogisticRegression.costRegularized(X,y,theta);

}

LogisticRegression.costRegularized = LogisticRegression.prototype.costRegularized = function(X, y, theta, lambda) {
	var _y = Matrix.matrixify(y);
	var _yt = _y.transpose();
	var m = _yt.colCount();
	var _theta = Matrix.matrixify(theta).clone();
// 	var gradient = Matrix.zero(_theta.rowCount(),1);
	var _X = Matrix.matrixify(X); //.appendLeft(Matrix.one(m,1));
	
	var _sig = LogisticRegression.H(_X, _theta);
	var _ytrue = _yt.mul(_sig.elementLog());
	var _yfalse = _yt.elementMul(-1).elementPlus(1).mul(_sig.elementMul(-1).elementPlus(1).elementLog());
	var J = (_ytrue.add(_yfalse)).elementMul((-1/m));

// + Gradient descent code		
// +		var Merr = MX.mul(theta).minus(My);
// +		var sumMXMerr = MX.transpose().mul(Merr);
// +		var multiplier = alpha / m;
// +		sumMXMerr = sumMXMerr.elementMul(multiplier);
// +		theta = theta.minus(sumMXMerr);

	// Computing gradient
	var gradient = _X.transpose().mul((_sig.minus(_y))).elementMul((1/m));

	//debugger;
	// theta(1)=0;
	// J=J+sum(theta .^ 2) * lambda / (2*m);
	// grad = grad + (lambda / m) * theta;
	if(typeof lambda!='undefined') {
		// Computing J
		_theta.ij(0,0,0);
 		var jaccum = J.ij(0,0);
 		jaccum += _theta.elementSq().sumAll() * lambda / (2*m);
 		J.ij(0,0,jaccum);

		// Computing gradient
		gradient = gradient.add(_theta.elementMul( lambda/m));


	}

	return {
		J: J.ij(0,0)
		, gradient : gradient
	};


}
