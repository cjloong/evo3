var Matrix = require("../math_matrix/math_matrix.js");
var LogisticRegression = require("./LogisticRegression.js");

QUnit.module("Logistic Regression", ()=>{
	
	QUnit.module("Unit Test:Logistic Regression");
	QUnit.test("Logistic Regression Warmup", function(assert){
		var LR = new LogisticRegression();
		assert.ok(true,"Empty instantiation");
	});

	QUnit.test("Sigmoid function", function(assert){
		var LR = new LogisticRegression();

		var ans = LR.sigmoid(1200000);
		assert.ok(ans.ij(0,0)===1);

		var ans = LR.sigmoid(0);
		assert.ok(ans.ij(0,0)===0.5);

		var ans = LR.sigmoid(-25000);
		assert.ok(ans.ij(0,0)===0);
		// Sigmoid of matrix
		var ans = LR.sigmoid("4 5 6");
		assert.ok(_.round(ans.ij(0,0),3)===0.982);
		assert.ok(_.round(ans.ij(0,1),3)===0.993);
		assert.ok(_.round(ans.ij(0,2),3)===0.998);

		var ans = LR.sigmoid(Matrix.I(2));
		assert.ok(_.round(ans.ij(0,0),3)===0.731);
		assert.ok(_.round(ans.ij(0,1),3)===0.500);
		assert.ok(_.round(ans.ij(1,0),3)===0.500);
		assert.ok(_.round(ans.ij(1,1),3)===0.731);

	});

	QUnit.test("Prediction test", function(assert){
		var X = new Matrix("8 1 6;3 5 7;4 9 2");
		var LR = new LogisticRegression();
		LR.theta("0;0;1;10");
		var prediction = LR.predict(X);
		assert.ok(prediction.ij(0,0)===1);
		assert.ok(prediction.ij(1,0)===1);
		assert.ok(prediction.ij(2,0)===1);


		LR.theta("0;4;3;-8");
		var prediction = LR.predict(X);
		assert.ok(prediction.ij(0,0)===0);
		assert.ok(prediction.ij(1,0)===0);
		assert.ok(prediction.ij(2,0)===1);

		LR.theta("0;3;0;-8");
		var prediction = LR.predict(X);
		assert.ok(prediction.ij(0,0)===0);
		assert.ok(prediction.ij(1,0)===0);
		assert.ok(prediction.ij(2,0)===0);
	});


	QUnit.test("Cost Function test", function(assert){
		var LR = new LogisticRegression();
		var X = new Matrix("8 1 6;3 5 7;4 9 2");
		var m = X.rowCount();
		X = X.appendLeft(Matrix.one(m, 1));
		var y = new Matrix("1;0;1");
		var theta = LR.theta("-2;-1;1;2");

		var cost = LR.costFunction(X,y,theta);
		assert.ok(_.round(cost.J,4)===4.6832);

		assert.ok(_.round(cost.gradient.ij(0,0), 5) === 0.31722);
		assert.ok(_.round(cost.gradient.ij(1,0), 5) === 0.87232);
		assert.ok(_.round(cost.gradient.ij(2,0), 5) === 1.64812);
		assert.ok(_.round(cost.gradient.ij(3,0), 5) === 2.23787);

	});

	QUnit.test("Regularized Cost Function test - alpha = not part of function call.", function(assert){
		var LR = new LogisticRegression();
		var X = new Matrix("8 1 6;3 5 7;4 9 2");
		var m = X.rowCount();
		X = X.appendLeft(Matrix.one(m, 1));
		var y = new Matrix("1;0;1");
		var theta = LR.theta("-2;-1;1;2");

		var cost = LR.costRegularized(X,y,theta);
		assert.ok(_.round(cost.J,4)===4.6832);

		assert.ok(_.round(cost.gradient.ij(0,0), 5) === 0.31722);
		assert.ok(_.round(cost.gradient.ij(1,0), 5) === 0.87232);
		assert.ok(_.round(cost.gradient.ij(2,0), 5) === 1.64812);
		assert.ok(_.round(cost.gradient.ij(3,0), 5) === 2.23787);

	});

	QUnit.test("Regularized Cost Function test - alpha = 0", function(assert){
		var LR = new LogisticRegression();
		var X = new Matrix("8 1 6;3 5 7;4 9 2");
		var m = X.rowCount();
		X = X.appendLeft(Matrix.one(m, 1));
		var y = new Matrix("1;0;1");
		var theta = LR.theta("-2;-1;1;2");

		var cost = LR.costRegularized(X,y,theta,0);
		assert.ok(_.round(cost.J,4)===4.6832);

		assert.ok(_.round(cost.gradient.ij(0,0), 5) === 0.31722);
		assert.ok(_.round(cost.gradient.ij(1,0), 5) === 0.87232);
		assert.ok(_.round(cost.gradient.ij(2,0), 5) === 1.64812);
		assert.ok(_.round(cost.gradient.ij(3,0), 5) === 2.23787);

	});

	QUnit.test("Regularized Cost Function test - alpha = 3", function(assert){
		var LR = new LogisticRegression();
		var X = new Matrix("8 1 6;3 5 7;4 9 2");
		var m = X.rowCount();
		X = X.appendLeft(Matrix.one(m, 1));
		var y = new Matrix("1;0;1");
		var theta = LR.theta("-2;-1;1;2");

		var cost = LR.costRegularized(X,y,theta,3);
		assert.ok(_.round(cost.J,4)===7.6832);

		assert.ok(_.round(cost.gradient.ij(0,0), 5) === 0.31722);
		assert.ok(_.round(cost.gradient.ij(1,0), 5) === -0.12768);
		assert.ok(_.round(cost.gradient.ij(2,0), 5) === 2.64812);
		assert.ok(_.round(cost.gradient.ij(3,0), 5) === 4.23787);

	});

	QUnit.test("Scoring support", function(assert){
		var LR = new LogisticRegression();
		var X = new Matrix("8 1 6;3 5 7;4 9 2");
		var y = new Matrix("1;0;1");
		var ytest = new Matrix("1;0;0");

		LR.fit(X,y);
		var score = LR.score(X, ytest);

		assert.ok(_.round(score,3)===0.667);
	});

	QUnit.test("Fit function: Multiple labels classification", function(assert){
		var LR = new LogisticRegression();
		var X = (new Matrix("1 2 3 4 5 6 7 8 9 10")).transpose();
		var y = (new Matrix("1 1 2 2 3 3 4 4 5 5")).transpose();
		LR.fit(X,y);
		var prediction = LR.predict(X);
		assert.ok(_.round(prediction.ij(0,0), 0) === 1);
		assert.ok(_.round(prediction.ij(1,0), 0) === 1);
		assert.ok(_.round(prediction.ij(2,0), 0) === 2);
		assert.ok(_.round(prediction.ij(3,0), 0) === 2);
		assert.ok(_.round(prediction.ij(4,0), 0) === 3);
		assert.ok(_.round(prediction.ij(5,0), 0) === 3);
		assert.ok(_.round(prediction.ij(6,0), 0) === 4);
		assert.ok(_.round(prediction.ij(7,0), 0) === 4);
		assert.ok(_.round(prediction.ij(8,0), 0) === 5);
		assert.ok(_.round(prediction.ij(9,0), 0) === 5);


	});

	QUnit.test("Fit function:0 and 1", function(assert){
		var LR = new LogisticRegression();
		var X = new Matrix("8 1 6;3 5 7;4 9 2");
		var y = new Matrix("1;0;1");

		LR.fit(X,y);
		var prediction = LR.predict(X);
		assert.ok(_.round(prediction.ij(0,0), 0) === 1);
		assert.ok(_.round(prediction.ij(1,0), 0) === 0);
		assert.ok(_.round(prediction.ij(2,0), 0) === 1);


	});

	QUnit.module("_Development: Logistic Regression");
	QUnit.skip("Feature normalization support", function(assert){});
	QUnit.skip("One for all", function(assert){});



	QUnit.skip("Parameterize the alpha and lambda units", function(assert){});
});
