var Matrix = require("../math_matrix/math_matrix.js");

/**
 * 
 */
var NormalEquation = module.exports = function*(X, y) {

	// Preparation
	var _X = Matrix.matrixify(X);
	var _y = Matrix.matrixify(y);
	var sz_X = _X.size();
	var sz_y = _y.size();
	var theta;

	// Error checking
	if(sz_X[0]!==sz_y[0]) throw "Training set(X) and result(y) must have same length.";
	else {
		var m = sz_X[0];
		var n = sz_X[1];

		// Initializing theta array
		theta = Matrix.zero(sz_X[1] + 1, 1);

 		// Append 1 to left of X
 		//REFACTORED to fit function
// 		_X = _X.appendLeft(Matrix.one(m,1));

		theta = _X.transpose()
			.mul(_X)
			.inv()
			.mul(_X.transpose()).mul(_y);
		
	}

	return theta;

}


