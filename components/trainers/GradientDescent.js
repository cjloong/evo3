var _ = require("lodash");

var Matrix = require("../math_matrix/math_matrix.js");

var GradientDescent = module.exports = function*(X, y, param) {

	// Appending defaults
	var _param= _.defaultsDeep({}, param, GradientDescent.defaults);	
	var MX = Matrix.matrixify(X);
	var m = MX.size()[0];
	var My = Matrix.matrixify(y);
	var alpha = _param.learningRate;
	var maxIterations = _param.maxIterations;
	var costFunction = _param.costFunction;
	var reportIteration = typeof param.reportEveryXIteration == 'undefined' ? 100: param.reportEveryXIteration;
// 	var featureNormalization = param.featureNormalization;

	if(My.rowCount()!==m) throw "y must have same number of rows as training set. X[" 
								+ MX.size().join("x")
								+ "] y[" + My.size().join("x") + "]";

	var n = MX.colCount();
	var theta = Matrix.zero(n);


	for(var i = 0; i<maxIterations; i++) {
		var result = costFunction(MX, My, theta);
		if( _.round( result.gradient.sumAll(), 5)==0.00000 ){
			yield {
				iteration: i
				, gradient: result.gradient//TODO.formatSmall()
				, cost: result.J
			}
			break;
		}

		theta = theta.minus(result.gradient.elementMul(alpha));

		if(i%reportIteration===0) {
			yield {
				iteration: i
				, gradient: result.gradient//TODO.formatSmall()
				, cost: result.J
			}
		}

	}
	
	// Validation complete, continue with GD
	return theta;
}

/* DEPRECATED:BEGIN
//Defaults of mode must be made after function is defined
GradientDescent.defaults = {
	learningRate : 0.1
	, maxIterations : 100
	//TODOprofile = false;
};

GradientDescent._search = function(X, y, param){

	// Appending defaults
	var _param= _.defaultsDeep({}, param, GradientDescent.defaults);	
	var MX = Matrix.matrixify(X);
	var m = MX.size()[0];
	var My = Matrix.matrixify(y);
	var alpha = _param.learningRate;
	var maxIterations = _param.maxIterations;
	var costFunction = _param.costFunction;

// 	var featureNormalization = param.featureNormalization;

	if(My.rowCount()!==m) throw "y must have same number of rows as training set. X[" 
								+ MX.size().join("x")
								+ "] y[" + My.size().join("x") + "]";



	// Preprocessing - Feature normalization
	//TODO: Refactor out this one to learning algorithm, not search
// 	if(featureNormalization) {
// 		// mean xi' = (xi - mean(xi))/(max(xi) - min(xi))
// 		// denominator can use std as well
//         var normParam = param.normParam = MX.colMaxMinSum();
//         normParam.mean = normParam.sum / m;
        
// 		param.normalize(MX, My, normParam);
		
// 	}

	// Pre run initialization

	var n = MX.colCount();
	var theta = Matrix.zero(n);


	for(var i = 0; i<maxIterations; i++) {
		//TODO: Check for additional stop conditions

// 		+		
// +		var Merr = MX.mul(theta).minus(My);
// +		var sumMXMerr = MX.transpose().mul(Merr);
// +		var multiplier = alpha / m;
// +		sumMXMerr = sumMXMerr.elementMul(multiplier);
// +		theta = theta.minus(sumMXMerr);

		var result = costFunction(MX, My, theta);

		theta = theta.minus(result.gradient.elementMul(alpha));


	}


	
	// Validation complete, continue with GD
	return theta;
}
DEPRECATED:END */
