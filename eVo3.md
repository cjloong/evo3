# eVo3
This is the collection of stuffs.

## General build process
1. Everything is configured using webpack. Execute webpack using this process:

    $ webpack --progress --color --watch


1. We configure the webpack modules in src/<module_name> directories. Each module has
a unit test qunit.<module_name>.js. It has an entry folder to webpack its bundle to. Dir
structure is as follows:

    $ <project_root>/components/<module_name>/

