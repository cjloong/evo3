/**
 * This webpack configuration listens to components and apps of the system.
 */
const path = require('path');
const webpack = require("webpack");
const WebpackNotifierPlugin = require("webpack-notifier");

//Components
const component = "./components";
module.exports = [{

	context : path.resolve(component)
	, node: {fs: "empty"} //TODO: Check if node apps will be affected. Should only affect this on browser stuffs
	, entry: {
		"qunit.machine_learning": [
		   	path.resolve(path.join(component, "expert", "qunit.expert.js"))
//		    , path.resolve(path.join(component, "logic", "qunit.logic.js"))
//		    , path.resolve(path.join(component, "goal_tree", "qunit.goal_tree.js"))
//		    , path.resolve(path.join(component, "experiments", "qunit.experiments.js"))
//		    , path.resolve(path.join(component, "visualization", "qunit.visualization.js"))
//		    , path.resolve(path.join(component, "linear_regression", "qunit.linear_regression.js"))
//		    , path.resolve(path.join(component, "logistic_regression", "qunit.logistic_regression.js"))
//		    , path.resolve(path.join(component, "math_matrix", "qunit.math_matrix.js"))
//		    , path.resolve(path.join(component, "neural_network", "qunit.neural_network.js"))
//		    , path.resolve(path.join(component, "scratch", "qunit.scratch.js"))
//		    , path.resolve(path.join(component, "trainers", "qunit.trainers.js"))
//		    , path.resolve(path.join(component, "ui", "qunit.ui.js"))
//		    , path.resolve(path.join(component, "tools", "qunit.tools.js"))
//		    , path.resolve(path.join(component, "parser", "qunit.parser.js"))
//		    , path.resolve(path.join(component, "bdd", "qunit.bdd.js"))
//		    , path.resolve(path.join(component, "nlp", "qunit.nlp.js"))
        	]

	}
	//For faster code gen:	devtool : 'eval-source-map'
	, devtool: '#source-map'



	, output : {
		path : './components'
		, filename : "[name].bundle.js"
		, sourcePrefix: ""
	}
	, plugins: [
        new WebpackNotifierPlugin()
    ]
	,module: {
		loaders: [
			{ test: /\.json$/, loader: "json" }
			, { test: /\.css$/, loader: "style-loader!css-loader" }
			, {
				test: /\.jsx?$/
				, exclude: /(node_modules|bower_components)/
				, loader: 'babel' // 'babel-loader' is also a legal name to reference
				, query: {
					presets: ['es2015', 'react']
					, plugins: ['transform-runtime']
				}
			}
    		]

	}
}];
