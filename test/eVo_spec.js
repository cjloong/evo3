var webdriver = require('selenium-webdriver'),
    By = webdriver.By,
    until = webdriver.until
    ;
var assert = require('chai').assert;

var path = require("path");
var helper = require("./helper/helper.js");
var fs = require("fs");

var browser = null;
var resembelanceTolerance = 99.99999999;

describe('basic test', function() {
    this.timeout(60000);
    before(() => {
         if (browser === null) {
            browser = new webdriver.Builder()
                .forBrowser('chrome')
                .build();
        }
    });

    after(() => {
         browser.quit();
        browser = null;
    });

    beforeEach(function() {
    });

    afterEach(function() {
    });

    // it('Google Test', function(done) {
    //     const camera = helper.makeCamera2(browser, path.join(".", "latest_test"), helper.underscore('Google Test'));
    //     var counter = 0;
    //     browser.get("http://www.google.com")
    //         .then(camera(++counter))
    //         .then(() => {
    //             done()
    //         });;
    // });

    it('eVo Warmup Test', function(done) {
        // const camera = helper.makeCamera(path.join(".", "latest_test"), helper.underscore('eVo Warmup Test'));
        const camera2 = helper.makeCamera(
              browser
              , path.join(".", "latest_test")
              , path.join(".", "baseline_test")
              , helper.underscore('eVo Warmup Test')
              , resembelanceTolerance
        );

        var counter = 0;
        browser.get("http://localhost:3000")
            .then( ()=>(camera2(++counter)) )
            .then( () => ( browser.findElement(By.linkText("reactPlayground")).click()) )

        browser.wait(function () {
            return browser.isElementPresent(By.className("commentItem"));
       }, 40000);

        browser.findElement(By.className("commentItem"))
            .then( (comment)=>(camera2(++counter)) )
            .then((screenCheck) => {
                assert.isTrue( !screenCheck.baselineExist || resembelanceTolerance < screenCheck.resemblence, "Screen not same");
            })
            .then(() => {
                // No need to wait, so can do direct as all elements here are
                // already rendered
                var elem = browser.findElement(By.id('input_name'));
                elem.sendKeys('codechewing');
                var elem = browser.findElement(By.id('input_message'));
                elem.sendKeys('some comments that i type');
                var elem = browser.findElement(By.id('cmdPost'));
                elem.click();
            })
            .then(()=>(camera2(++counter)))
            .then(() => (browser.findElement(By.className("commentList"))))
            .then((commentList) => {
                commentList.getText().then((text) => {});
             })
            .then(() => {
                done()
            })
            .catch((e)=>{
                console.error(e);
                done();
            })
            ;
    });

    it("Test 3", ()=>{

    })
});
