var fs = require("fs");
var path = require("path");
var mkdirp = require('mkdirp');
var resemble = require('node-resemble-js');
var assert = require('chai').assert;

module.exports = {};

module.exports.makeCamera = function(browser, screenshotPath, baselinePath, photoName, saveDiffTolerance) {

     resemble.outputSettings({
          errorColor: {
               red: 155,
               green: 100,
               blue: 155
          },
          errorType: 'movement',
          transparency: 0.6,
          largeImageThreshold: 0
     });
    return function(counter) {
        const paddedCounter = ("0000" + counter).slice(-4);
        var name = name || `${photoName}_${paddedCounter}.png`;
        return new Promise((resolve, reject) => {
            browser.takeScreenshot().then((data) => {
                try {
                    mkdirp.sync(screenshotPath);
                    fs.writeFileSync(path.join(screenshotPath, name), data, 'base64');
                    // stats = fs.lstatSync(path.join(baselinePath, name));
                    if (!fileExist(path.join(baselinePath, name))) {
                      resolve({
                          baselineExist: false,
                          resemblence: 0
                      });

                    } else {
                      //TODO: Check for accuracy and save an inaccurate file here!!!
                      // Checking for baseline screen vs current screen
                      resemble(path.join(baselinePath, name))
                          .compareTo(path.join(screenshotPath, name))
                          .onComplete(function(data) {
                              var resemblePercent = 100.0 - data.misMatchPercentage;

                              if(saveDiffTolerance>resemblePercent) {
                                data.getDiffImage().pack().pipe(fs.createWriteStream(path.join(screenshotPath, "DIFF_" + name)));

                              }
                              resolve({
                                  baselineExist: true,
                                  resemblence: resemblePercent
                              });
                          });
                    }

                } catch (e) {
                    console.log("Exception occured")
                    reject(e);
                }
            });
        });
    }
}

var fileExist = module.exports.fileExist = function(fullfilename) {
  try {
    stats = fs.lstatSync(fullfilename);
    if (stats.isFile()) return true;
    return false;
  } catch (e) {
    return false;
  }


}


module.exports.makeCamera2 = function(browser, screenshotPath, photoName) {

    return function(counter) {
        const paddedCounter = ("0000" + counter).slice(-4);
        var name = name || `${photoName}_${paddedCounter}.png`;
        browser.takeScreenshot().then((data) => {
            try {
                mkdirp.sync(screenshotPath);
                fs.writeFileSync(path.join(screenshotPath, name), data, 'base64');
            } catch (e) {
                console.error(e);
            }
        });
    }
}

//
// module.exports.makeCamera = function(screenshotPath, photoName) {
//     return function(data, counter) {
//         var name = name || `${photoName}_${counter}.png`;
//         try {
//             mkdirp.sync(screenshotPath);
//             fs.writeFileSync(path.join(screenshotPath, name), data, 'base64');
//
//         } catch (e) {
//             console.error(e);
//         }
//     }
// }

module.exports.underscore = (words) => {
    return (words || "").replace(".", " ").trim().replace(/ +/g, "_").toLowerCase();
}
