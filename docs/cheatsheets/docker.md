# Docker Cheatsheet

## Basic Images Command
Listing images

     $ docker images

Running images

     $ docker run -t -i ubuntu:14.04 /bin/bash

Run latest image

     $ docker run -t -i ubuntu /bin/bash
     $ docker run -t -i ubuntu:latest /bin/bash

Getting images

     $ docker pull centos

OR

     $ docker run -t -i centos /bin/bash

Finding images

     $ docker search sinatra

Tagging images

     $ docker tag 5db5f8471261 ouruser/sinatra:devel

Viewing images

2 types of images. User images vs base images.
Example of base images: ubuntu
Example of user images: training/ubuntu

## Creating images
### Method 1 - pull and update
You can pull an image, update and commit.

     $ docker run -t -i training/sinatry /bin/bash
     $ gem install json
     $ exit
     $ docker commit -m "Added json gem" -a "cjloong"  0b26160e5a8 cjloong/sinatra:v2

Syntax for commit
     $ docker commit -m <commit message> -a <username> <container id> <repository>





### Method 2 - Using Docker Files
We create a docker image using scripts

     $ mkdir sinatra
     $ cd sinatra
     $ vi Dockerfile

In vi session, type in the content of the file.

     # This is a comment
     FROM ubuntu:14.04
     MAINTAINER Kate Smith <ksmith@example.com>
     RUN apt-get update && apt-get install -y ruby ruby-dev
     RUN gem install sinatra

Save the file and build the container

     $ docker build -t cjloong/sinatra:v2 .

### Running the container
     $ docker run -d -P --name web training/webapp python app.py
     $ docker ps -l
     $ docker ps -a
     $ docker inspect web
     $ docker stop web
     $ docker rm web
     $ docker rmi cjloong/web # removing image

 ### Network

     # listing networks
     $ docker network ls

     # Running a container which by default uses bridge network
     $ docker run -itd --name=networktest ubuntu


     $ docker network inspect bridge

     # disconnect container networktest from bridge network
     $ docker network disconnect bridge networktest  

     # Creating a bridge network
     $ docker network create -d bridge my_bridge_network
     $ docker network inspect my_bridge_network   #inspect the network

     # Add containers to a network
     $ docker run -d --network=my-bridge-network --name db training/postgress
     $ network inspect my_bridge_network
     $ docker exec -i db ifconfig  # looking at ip address
     $ docker network connect my_bridge_network web   # Connecting the bridge to container

NOTE: you can connect as many network to a container. It can be a means of
communication between containers.

## Storage
2 types:
### Data volumes
     - specially designated directory within one or more containers
     - init when container created
     - can be shared and reused among containers
     - changes made directly
     - changes to data volume when update image
     - data persisted when container deleted

Cheatsheet

     # mount host /webapp into container. You need to inspect on where it was
     # mounted to
     $ docker run -d -P --name web -v /webapp training/webapp python app.py

     # Mounting host:/src/webapp to container /opt/webapp
     # in windows /c is c drive
     $ docker run -d -P --name web -v /src/webapp:/opt/webapp training/webapp python app.py

     # Mounting read only 'ro' option
     # NOTE: YOu cannot mount a host directory from Dockerfile because it
     # should be portable
     $ docker run -d -P --name web -v /src/webapp:/opt/webapp:ro training/webapp python app.py

     # Official way is to mount volume when container runs. There is no
     # way to attach a container halfway. To add volumes, we have to stop
     # and rerun. If we do not want to loose, we stop, commit and run with
     # new mounts

### Data volume containers
There are volumes shared between different containers. NOTE: Apps must be able
to handle shared storage, if not corruptions will occur.


     # Creating a data volume container
     $ docker create -v /dbdata --name dbstore training/postgres /bin/true

     # Running with the volume attached
     $ docker run -d --volumes-from dbstore --name db1 training/postgress
     $ docker run -d --volumes-from dbstore --name db2 training/postgress

     # Can chain it to another container
     $ docker run -d --volumes-from db1 --name db3 training/postgress

     # When remove containers that have volumes mounted, volumes will not be deleted. To delete
     $ docker rm -v <against the last container that reference the volume>

     # Dangling volumes can be located
     $ docker volume ls -f dangling=true
     $ docker volume rm <volume name>

 ### Backup/restore/migrate data volumes

     # To backup data volume container dbstore
     # Here, we mount host path to container/backup
     # Then run a backup of dbstore to the container /backup path (which is current dir $(pwd))
     $ docker run --rm --volumes-from dbstore -v $(pwd):/backup ubuntu tar cvf /backup/backup.tar /dbdata

     # To restore
     $ docker run -v /dbdata --name dbstore2 ubuntu /bin/bash
     $ docker run --rm --volumes-from dbstore2 -v $(pwd):/backup ubuntu bash -c "cd /dbdata && tar xvf /backup/backup.tar --strip 1"

     # Anonymous volume (volumes that are removed when container stops)
     # Example below, /foo is anonymous and /bar is not
     $ docker run --rm -v /foo -v awesome:/bar busybox top


# Docker Machine
To install more packages, use TinyCore. tce or tce-load

     # Create machines
     $ docker-machine create --driver virtualbox machine1

     # Listing all machines
     $ docker-machine ls

     # View machine env details
     $ docker-machine env default
     $ docker-machine env machine1

     # Switch machines
     $ eval "$(docker-machine env default)"
     $ eval "$(docker-machine env machine1)"


     # Stop / start current machines
     $ docker-machine stop
     $ docker-machine start
     $ docker-machine stop default
     $ docker-machine start default

     # View ip
     $ docker-machine ip

### Other machine commands
- `docker-machine config`
- `docker-machine env`
- `docker-machine inspect`
- `docker-machine ip`
- `docker-machine kill`
- `docker-machine provision`
- `docker-machine regenerate-certs`
- `docker-machine restart`
- `docker-machine ssh`
- `docker-machine start`
- `docker-machine status`
- `docker-machine stop`
- `docker-machine upgrade`
- `docker-machine url`
