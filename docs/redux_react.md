# Overview
This is a document of the study of react redux framework.

## Overall Architecture

## Imutability
- a piece of data we don't want to change
- we create a new one

Mutable example
```
var a = {
     name: "Will"
}

a.name="Bob" // Mutated
```

Example of immutable
```
// In jQuery - $.extend
var a = {name: "Will"}

Object.assign({}, a, {
     name: "Bob"
});
```

```
var a = [0,1,2];
b = a.concat(3);

// a is untouched
```

concat, filter, map, reduce is always doing new stuffs and is the
primary tool to do Imutability

## Redux Store
- It is a store of application states over time.
- Initilized with reducer function - ie. createStore(reducer)
