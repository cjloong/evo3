# Overview
This is the high level overview of eVo architecture.

## Components Overview
### NLP
1. Design 
    - Feed it text and it will push out identified entities, actions, relationships, screens.
    - Feed it architecture, it will create documentation.
    - Markdown language support
    
### Parsers
1. Parsers:-
    - Entity parser
        - This would become a command line for editing entity relationship
        - English like language
        - Link to NLP for initial feed


## Building Components
