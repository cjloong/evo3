# Docker Notes
## From Docker Introduction
Container vs virtual machines
- in dark ages, one application on one physical server
     - Slow deployment times, huge cost, wasted resource,
       difficult to scale, difficult to migrate, vendor
       lock in
     - VM helps but its big
          - each vm need cpu, storage, ram and entire OS
          - wasted
     - Container base virtualization
          - uses kernel on host to run multiple guest instances
          - each instance is called a Container
          - each container have their own
               - root
               - processes
               - memory
               - devices
               - network ports
          - Its not a VM. Containers are :-
               - lightweight
               - no need to install guest OS
               - less CPU, RAM, storage required
               - more containers per mahcine than VM
               - Greater protability

# Commands
Run a Docker
```
$ docker run hello-world
```

Note, I do not have Windows Pro, so I am using docker toolbox. So, the downside is I cannot use cmd to run docker commands.

To run docker, double click Docker Quickstart Terminal shortcut
which launch the command line:
```
"C:\Program Files\Git\bin\bash.exe" --login -i "C:\Program Files\Docker Toolbox\start.sh"
```

3 good command is:
docker ps, docker info, docker version

To run a ubuntu container
```
docker run -it ubuntu bash
```
'-t' - psedo tty
'-i' - interactive

Running ngix
```
docker run -d -p 80:80 --name webserver nginx
```
'-d' - run detach mode
'-p' - publish value (port to host port)

To try out ngix, point webserver to http://localhost after running detached

For docker-toolblx, we need to know the vm info.
```
$ docker-machine ls
NAME      ACTIVE   DRIVER       STATE     URL                         SWARM   DOCKER    ERRORS
default   *        virtualbox   Running   tcp://192.168.99.100:2376           v1.12.0
```
From the above, we know the vm is running 192.168.99.100
So, when we do a http://192.168.99.100 on browser, we get
the nginx hello world screen.

To see what images have been downloaded
```
docker images
```
## Searching for images
https://hub.docker.com/?utm_source=getting_started_guide&utm_medium=embedded_MacOSX&utm_campaign=find_whalesay

Then can run (read instructions first)

```
docker run whalesay cowsay boo
```

The above command downloads the whalesay image (if not already)
downloaded. Then issue a command line cowsay boo to the machine.

## Build own image
Create a directory and edit Dockerfile
```
FROM docker/whalesay:latest
RUN apt-get -y update && apt-get install -y fortunes
CMD /usr/games/fortune -a | cowsay
```

Result:
```
$ pwd
/c/Users/chekjen/git/evo3/dockers/mydockerbuild

$ ls
Dockerfile

$ cat Dockerfile
FROM docker/whalesay:latest
RUN apt-get -y update && apt-get install -y fortunes
CMD /usr/games/fortune -a | cowsay
$
```

Build the new docker image
```
$ docker build -t docker-whale .

```

## Docker repository
We need to signup and have our own repository. Then we can
push images to/from the repository.

First, tag it.
```
 docker tag 7d9495d03763 cjloong/docker-whale:latest
```
NOTE: Remember to key in correct tag. To see, docker images


Login and push
```
$ docker login --username=cjloong
$ docker push cjloong/docker-whale

```

Removing images
```
$ docker rmi cjloong/docker-whale
```

Pull it back
```
$ docker pull cjloong/docker-whale
```


# Writing hello world container
Run a container
```
$ docker run ubuntu /bin/echo hello
```
Run in interactive mode
```
$docker run -t -i ubuntu /bin/bash
```

When done
```
$ exit
```

Run daemon mode
```
$ docker run -d ubuntu /bin/sh -c "while true; do echo hello world; sleep 1; done"
```

Stop the container
```
$ docker stop <name>
```

NOTE about the name
```
$ docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS               NAMES
f0472ba5b9b6        ubuntu              "/bin/sh -c 'while tr"   4 seconds ago       Up 12 seconds                           sad_bhabha

```
When we run a daemon, docker assign a name. In this instance, we see the name as
sad_bhabha

## Running a webapp (Python prebuilt test)
```
$ docker run -d -P training/webapp python app.py
```
'-d' - run in background
'-P' - map required network ports inside container to host
'-P' is a shortcut for -p 500, means map container port 500 to ephemeral port


after passing -P, the PORTS column is filled up

```
$ docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS                     NAMES
6cc640d27959        training/webapp     "python app.py"     2 minutes ago       Up 2 minutes        0.0.0.0:32768->5000/tcp   berserk_heyrovsky
```
In this example, outside world uses ephemeral port number to get in.
ie. http://192.168.99.100:32768 to get to webserver.

To see IP address of docker machine
```
$ docker-machine ls
NAME      ACTIVE   DRIVER       STATE     URL                         SWARM   DOCKER    ERRORS
default   *        virtualbox   Running   tcp://192.168.99.100:2376           v1.12.0

chekjen@ChekJenAlltrix MINGW64 ~
$ docker-machine ip default
192.168.99.100

chekjen@ChekJenAlltrix MINGW64 ~
$
```

To see the port of a particular component, we could use command
```
chekjen@ChekJenAlltrix MINGW64 ~
$ docker port berserk_heyrovsky
5000/tcp -> 0.0.0.0:32768

chekjen@ChekJenAlltrix MINGW64 ~
```

To see logs, we use the following command
```
$ docker logs <name>
$ docker logs -f <name>
```

To look at processes in container
```
$ docker top berserk_heyrovsky
PID                 USER                COMMAND
10269               root                python app.py

```

Inspect container
```
$ docker inspect berserk_heyrovsky
[
    {
        "Id": "6cc640d27959d3f92d819644cb2a48fe68881243713c5e18503730579b68688a",
        "Created": "2016-07-30T15:45:44.301148834Z",
        "Path": "python",
        "Args": [
            "app.py"
        ],
        "State": {
            "Status": "running",
            ...
```

To stop container, do
```
$ docker stop <name>
```

Remove container
```
chekjen@ChekJenAlltrix MINGW64 ~
$ docker rm berserk_heyrovsky
berserk_heyrovsky

chekjen@ChekJenAlltrix MINGW64 ~
```

After removing, when you do docker ps -a , it will not be there.

## Docker Images

Tools to visualize image :-
- https://imagelayers.io/
- https://github.com/justone/dockviz
