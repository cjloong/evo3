Terminology
Fully vs partially observable
Deterministic vs Stochastic
Discrete vs Continuous
    - finite choices (types) vs infinite choices (real numbers)
Benign vs Adversarial
    - 