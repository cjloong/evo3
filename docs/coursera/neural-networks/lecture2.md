Feed forward
- normal ones like in Andrew Ng course
- deep nn if 1 or more layer of hidden unigs

Recurrent
- recently a lot of progress
- cyclic - can remember info for a long time (directed cycles)
	- cycles can have infinite loops, so harder to implement/train
- more biologically realistic


Symmetrically connected network
- weights same in both directions
- easier to analyze then recurrent


Perceptrons
- gen 1 nn
- still in use for millions of features

z = b + SUMi xiwi

y = 1 if z>=0, 0 otherwise
