# Knowledge

## Angular2
### Angular cli

#### App
To generate new app

    $ ng new <name> --prefix <prefix>
    
#### Components
A component in angular encapsulates the stylesheet, html template and code. The ng cli tool
generates this files as well as a test file.

To generate component

    $ ng generate component <name> --inline-template --inline-style
    $ ng g c <name> -is -it



#### Templates
Ref and Element
Example:

    <input #MyRef type=input>
    <button (click)="onClick(MyRef)">
    <button (click)="onClick2($event, MyRef)">
    
The above example will take value of input and pass it to onClick function when click event
occur. Second button will take DOM event using keyword $event and pass to the function.


#### Material
To install angular material

    $ npm install -save @angular/material

        
## Node js
To update npm

    $ npm install npm -g

To clean cache
    
    $ npm cache clean
    
Updating node to latest version

    $ npm install -g n
    
