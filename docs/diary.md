# Overview
This document is a log of activities, thoughts and high level planning.

# Goals
- Refactor out usable components from eVo project. Namely we need to refactor out the following:
    - matrix
    - machine learning libraries
    - constraint programming
    - expert system
- Create UI layer for all refactored libraries.
    - editor layer
    - shell layer
    - visualization layer
- Setup docker and project site to display products. Need to opensource some projects.
    
# Current
- Development of expert system
    - Working on matching algorithm, Rete algorithm to be more specific.
        - Created on/off for nodes in rete network. Idea is as follows:-
            - Link working memory set/unset actions to rete network
            - on chage of working memory, rete network will turn on/of states
              for all nodes affected.
            - Will also mark the latest conflict set
            - DESIGN:
                - We need a working memory listener
                - On change of working memory (Add, Change, Delete) we trigger
                  rete network synchronization
                - Rete network will always have the latest set of conflict sets
                
- Angular2 exploration and using it to develop ui layer for expert system.
    - create hello world
    - incorporation of material design.
    - startup project
    

# Done
## 2016-11-18
